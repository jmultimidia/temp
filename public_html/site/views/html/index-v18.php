
<!doctype html>
<html class="no-js" lang="zxx"> 
	
<head>
	<title>Uaifome</title>
		
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="author" content="Anderson Barbosa Cavalcanti">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">


		<link rel="stylesheet" href="css/owl.theme.css">
		<link rel="stylesheet" href="css/owl.transitions.css">
		<link rel="stylesheet" href="css/owl.carousel.css">
		<!-- Owl Stylesheet -->
		<link rel="stylesheet" href="css/magnific-popup.css">
		<!-- Magnific PopUP Stylesheet -->
		<link rel="stylesheet" href="css/isotope.css">
		<!-- Isotope Stylesheet -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<!-- <link rel="stylesheet" href="css/bootstrap.css" /> -->
		<!-- Bootstrap Stylesheet -->
		<link rel="stylesheet" href="css/et-lant-font.css" />
		<!-- Et-Lant Font Stylesheet -->
		<link rel="stylesheet" href="css/3dslider.css" />
		<!-- 3D Slider Stylesheet -->
		<link rel="stylesheet" href="css/animate.css" />
		<!-- Animate Stylesheet -->
		<link rel="stylesheet" href="css/material-design-iconic-font.css" />
		<!-- Iconic Font Stylesheet -->

		<link rel="stylesheet" href="style.css">
		<!-- Main Style.css Stylesheet -->

		<link rel="stylesheet" href="css/responsive.css">
		<!-- Resposive.css Stylesheet -->

		<style type="text/css">
			
			.img_1 {width: 100px;height: 100px;border-radius: 100%;background-color: #fff;box-shadow: 0 18px 32px rgba(0,0,0,.08);margin: 0 auto;text-align: center;line-height: 100px;display: block;border-radius: 50%; overflow: hidden;}
	
			@media screen and (min-width: 500px) {

				#cont_add_estabelecimentos .form-group {width: 50% !important; float: left !important;}
				#cont_add_estabelecimentos .form-group:nth-child(1) {width: 100% !important;}

				.cont_2 {padding-right: 1rem;}
				.cont_1 {padding-left: 1rem;}
			}
		</style>
	</head>
	<body class="apps-craft-v18">

	<div id="load_all" style="display: none; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.5); z-index: 9999;">
        <img style="position: absolute; top: 50%; left: 50%; margin-top: -35px; margin-left: -35px; width: 70px; height: 70px;" src="img/load_2.gif">
    </div>
	
	<!-- Apps Craft Body Start -->
	<div id="preloader"></div>

	<!-- Apps Craft Main Menu -->
	<header style="position: fixed; top: 0; left: 0; width: 100%; z-index: 100; transition: 0.3s; padding: 1.5rem 1rem; background: transparent;">
    <div class="container">
      <div class="row">
        <div class="col-xs-12" style="display: flex; justify-content: space-between; align-items: center;">
          <div class="apps-craft-logo">
            <a href="#apps-craft-home">
              <img src="img/sticky-logo.png" style="height: 30px;" alt="uaifome Logo">
            </a>
          </div>
			
			<button class="btn" id="btn_open_cont_add_estabelecimentos" style="background: transparent; border: solid 1px #fff; border-radius: 30px; color: #fff; outline: none; font-weight: normal;">Cadastre sua empresa</button>
          <p style="margin-bottom: 0; color: #fff;">Uaifome</p>
        </div>
      </div>
    </div>
  </header> <!-- .apps-craft-main-menu END -->

	<!-- Apps Craft Welcome Section -->
	<section class="apps-craft-welcome-section apps-craft-welcome-section-v5 apps-craft-welcome-section-v18" id="apps-craft-home">
		<div class="apps-craft-18-banner-bg-wrap">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<div class="apps-craft-position-rel">
			<div id="apps_craft_animation">
				<div class="layer" data-depth="0.20">
					<div class="appscraft-screen-container">
					</div>
				</div>
			</div>
		</div>

		<div class="apps-craft-position-rel-v2">
			<div id="apps_craft_animation-2">
				<div class="layer" data-depth="0.20"><div class="background"><img src="img/version-18-bg.png" alt=""></div></div>
			</div>
		</div>
		<div class="apps-craft-welcome-container">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-6 col-md-pull-6 col-sm-6">
						<div class="apps-craft-welcome-tbl">
							<div class="apps-craft-welcome-tbl-c">
								<div class="apps-craft-welcome-content text-left">
									<h1>Obtenha seu App <span>Uaifome</span> grátis <br> agora </h1>

									<div class="apps-craft-download-store-btn-group">
										<a href="https://play.google.com/store/apps/details?id=org.uaiFome.uaiFome" target="_blanck" class="apps-craft-btn play-store-btn"><img src="img/google-play-logo.png" alt=""></a>
										<a href="https://itunes.apple.com/us/app/uaifome-delivery/id1439797763?l=pt&ls=1&mt=8" target="_blanck" class="apps-craft-btn app-store-btn"><img src="img/app-store-logo.png" alt=""></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <!-- .apps-craft-welcome-section END -->

	<!-- Apps Crfat Why Chose Us Section -->
	<section class="apps-craft-why-chose-us-section section-padding" id="apps-craft-chose-us" data-0="background-position:0px 1000px;" data-100000="background-position:0px -50000px;">
		<div class="container">
			<div class="apps-craft-section-heading">
				<h2 >PORQUE USAR O APLICATIVO UAIFOME</h2>
			</div> <!-- .apps-craft-section-heading END -->
			<div class="row content-margin-top">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="apps-craft-why-choose-us-container clear-both">
						<div class="apps-craft-why-choose-us-container-inner clear-both">
							<div class="apps-craft-why-chose-single clear-both">
								<div class="apps-craft-why-chose-ico">
									<span class="icon-lightbulb apps-craft-round">
										<span class="apps-craft-dash-border"></span>
									</span>
								</div> <!-- .apps-craft-why-chose-ico END -->

								<div class="apps-craft-why-chose-txt">
									<h3>Facil de usar</h3>
									<p>Com poucos cliques você escolhe o melhor estabelecimento e faz seu pedido</p>
								</div> <!-- .apps-craft-why-chose-txt END -->
							</div> <!-- .apps-craft-why-chose-single END -->
							<div class="apps-craft-why-chose-single clear-both">
								<div class="apps-craft-why-chose-ico">
									<span class="icon-wine apps-craft-round">
										<span class="apps-craft-dash-border"></span>
									</span>
								</div> <!-- .apps-craft-why-chose-ico END -->

								<div class="apps-craft-why-chose-txt">
									<h3>Ótima experiência</h3>
									<p>O app Uaifome tem uma interface super agradável de usar</p>
								</div> <!-- .apps-craft-why-chose-txt END -->
							</div> <!-- .apps-craft-why-chose-single END -->
							<div class="apps-craft-why-chose-single clear-both">
								<div class="apps-craft-why-chose-ico">
									<span class="icon-trophy apps-craft-round"></span>
								</div> <!-- .apps-craft-why-chose-ico END -->

								<div class="apps-craft-why-chose-txt">
									<h3>Monte seu pedido</h3>
									<p>No app Uaifome vc pode adicionar extras aos pedidos ou comprar pizzas com dois sabores</p>
								</div> <!-- .apps-craft-why-chose-txt END -->
							</div> <!-- .apps-craft-why-chose-single END -->
						</div> <!-- .apps-craft-why-choose-us-container-inner END -->
					</div> <!-- .apps-craft-why-choose-us-container END -->
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<figure class="apps-craft-why-chose-img" itemscope itemtype="http://schema.org/ImageGallery">
						<img src="img/choose-us-screenshort-1.png" class="wow fadeInRight" data-wow-delay=".8s" alt="Apps Craft Why Chose Image Screenshort">
						<img src="img/choose-us-screenshort-2.png" class="wow fadeInRight" data-wow-delay=".4s"  alt="Apps Craft Why Chose Image Screenshort">
					</figure> <!-- .apps-craft-why-chose-img END -->
				</div>
			</div>
		</div>
	</section> <!-- .apps-craft-why-chose-us-section END -->

	<!-- Apps Craft Screen Short Slider Section -->
	<section class="apps-carft-screen-short-ssection section-padding" id="apps-carft-screen-short">
		<div class="container">
			<div class="apps-craft-section-heading">
				<h2>Algumas telas</h2>
			</div> <!-- .apps-craft-section-heading END -->

			<div class="content-margin-top">
				<div class="apps-carft-screen-short-content">
					<div class="app-screen-mobile-image"></div>
					<div class="apps-craft-screenshort">
						<div id="carousel">
							<figure><img src="img/screenshort-slider-img-1.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-2.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-3.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-4.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-5.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-1.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-2.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-3.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-4.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-5.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-1.png" alt=""></figure>
							<figure><img src="img/screenshort-slider-img-2.png" alt=""></figure>
						</div>
					</div>
					<div id="options">
						<p id="navigation">
							<button id="previous" data-increment="-1" class="zmdi zmdi-long-arrow-left zmdi-hc-fw"></button>
							<button id="next" data-increment="1" class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></button>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Apps Craft Team -->
	<section class="apps-craft-team-section" id="apps-craft-team">
		<div class="team-parallax-bg section-padding" data-0="background-position:10000px 50%;" data-100000="background-position:50000px 0px;">
			<div class="container">
				<div class="apps-craft-section-heading">
					<h2>Categorias</h2>
				</div> <!-- .apps-craft-section-heading END -->
				<div class="row content-margin-top">
					<div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/3b5c869c147729c606419f7738522627.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Pizza média</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/3c58beb12b1177dcec3e64e8ac95e135.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Pizza grande</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/d4b3cb84ba56f9ad01d8a3a2533728fa.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Pizza pequena</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/d01bfcf645e0cd130ef3c24742e971a2.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Crepes</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/0cad88e8c4a6c0652720bc835d8ef691.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Bebidas</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/008d376d9f97eabbe0117875dd1cd3ae.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Espetinhos</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/75ff00417e77a74ec343d049db74faae.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Sanduíches</h2>
		              </div>
		            </div>
		                      <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
		              <figure class="apps-craft-team-img">
		                <div class="img_1">
		                  <img src="https://uaifome.com.br/assets/img/categorias/53d1a98c1428592f9d1871ccb1513692.jpg" style="width: 100%; height: 100%; object-fit: cover;" alt="Imagens principais categorias">
		                </div>
		              </figure>

		              <div class="apps-craft-team-bio">
		                <h2>Porções</h2>
		              </div>
		            </div>

				</div>
			</div>
		</div>
	</section> <!-- .apps-craft-team-section END -->


	<!-- Apps Craft Now Available On -->
	<div class="apps-craft-now-available-section" id="apps-craft-available" style="background-image: url(img/now-available-on-round-bg.png);">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="apps-craft-tbl">
						<div class="apps-craft-tbl-c">
							<div class="apps-craft-now-available-content wow fadeIn">
								<h3>Disponível para</h3>
								<p>Procurar cardápios para pedir comida virou coisa do passado. Com o Uaifome, você faz o pedido rapidamente em qualquer um dos restaurantes cadastrados que atenda à sua região. Em poucos cliques, você escolhe entre os restaurantes e os pratos que quiser e pode ficar despreocupado enquanto espera sua comida chegar onde quer que você esteja. É rápido, é seguro e é grátis.</p>

								<div class="apps-craft-download-store-btn-group">
									<a href="https://play.google.com/store/apps/details?id=org.uaiFome.uaiFome" target="_blanck" class="apps-craft-btn play-store-btn"><img src="img/google-play-logo.png" alt=""></a>
									<a href="https://itunes.apple.com/us/app/uaifome-delivery/id1439797763?l=pt&ls=1&mt=8" target="_blanck" class="apps-craft-btn app-store-btn"><img src="img/app-store-logo.png" alt=""></a>
								</div> <!-- .apps-craft-download-store-btn-group END -->
							</div> <!-- .apps-craft-now-available-content END -->
						</div>
					</div>
					<figure class="apps-craft-app-secreenshort" data-bottom="transform:translateY(180px) translateX(-50%);" data-top="transform:translateY(100px) translateX(-50%);" itemscope itemtype="http://schema.org/ImageGallery">
						<img src="img/screenshort-now-available.png" alt="Apps Craft App Screenshort">
					</figure> <!-- .apps-craft-app-secreenshort END -->
				</div>
			</div>
		</div>
	</div> <!-- .apps-craft-now-available-section END -->

	<!-- Apps Craft Footer Section -->
	<footer class="apps-craft-footer-section" id="apps-craft-footer">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="w-100 d-flex justify-content-between align-items-center">
						<p>&copy; Todos os direitos reservados UaiFome</p>
					</div>
				</div>
			</div>
		</div>
	</footer> <!-- .apps-craft-footer-section END -->

	<!-- add estabelecimentos -->
		<div class="modal" id="cont_add_estabelecimentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalVerticalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <form method="POST" enctype="multipart/form-data" action="<?= BASE?>estabelecimentos/add">

		      	<div class="modal-header">

			      	<h5>Cadastrar empresa</h5>
			    </div>

		      	<div class="modal-body">
	
					<div class="form-group">
						<p style="margin-bottom: 0;">Ícone do estabelecimento (200px por 200px):</p>
						<input type="file" name="img" class="form-control" accept="image/png,image/jpg,imagem/jpeg">
					</div>

					<div class="form-group cont_2">
						<p style="margin-bottom: 0px;">Nome do estabelecimento: <span style="color: red;">*</span></p>
						<input type="text" name="nome" class="form-control" required="required">
					</div>

					<div class="form-group cont_1">
						<p style="margin-bottom: 0px;">Telefone: <span style="color: red;">*</span></p>
						<input type="text" name="telefone" class="form-control" placeholder="DDD e número" required="required">
					</div>

					<div class="form-group cont_2">
						<p style="margin-bottom: 0px;">Email de login: <span style="color: red;">*</span></p>
						<input type="email" name="email" class="form-control" required="required">
					</div>

					<div class="form-group cont_1">
						<p style="margin-bottom: 0px;">Email de contato: <span style="color: red;">*</span></p>
						<input type="email" name="email_contato" class="form-control" required="required">
					</div>

					<div class="form-group cont_2">
						<p style="margin-bottom: 0px;">Estado: <span style="color: red;">*</span></p>
						<select name="estado" class="form-control">
	                        <option>AC</option><option>AL</option><option>AP</option><option>AM</option><option>BA</option><option>CE</option><option>DF</option><option>ES</option><option>GO</option><option>MA</option><option>MT</option><option>MS</option><option>MG</option><option>PA</option><option>PB</option><option>PR</option><option>PE</option><option>PI</option><option>RJ</option><option>RN</option><option>RS</option><option>RO</option><option>RR</option><option>SC</option><option>SP</option><option>SE</option><option>TO</option>
	                    </select>
					</div>

					<div class="form-group cont_1">
						<p style="margin-bottom: 0;">Cidade: <span style="color: red;">*</span></p>
						<select name="cidade" class="form-control">
	                       
	                    </select>
					</div>

					<div class="form-group cont_2">
						<p style="margin-bottom: 0;">Bairro: <span style="color: red;">*</span></p>
						<input type="text" name="bairro" class="form-control" required="required">
					</div>

					<div class="form-group cont_1">
						<p style="margin-bottom: 0;">Endereço: <span style="color: red;">*</span></p>
						<input type="text" name="endereco" class="form-control" required="required">
					</div>

					<div class="form-group cont_2">
						<p style="margin-bottom: 0;">Senha: <span style="color: red;">*</span></p>
						<input type="password" name="senha" class="form-control" required="required">
					</div>

					<div class="form-group cont_1">
						<p style="margin-bottom: 0;">Repetir senha: <span style="color: red;">*</span></p>
						<input type="password" name="senha_2" class="form-control" required="required">
					</div>

					<div style="clear: both;"></div>

					<div class="alert alert-warning" style="margin-bottom: 0; display: none;">teste</div>
			      </div>
			      <div class="modal-footer" style="border-top: solid 1px #ccc;">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
			        <button type="button" class="btn btn-success" id="btn_cadastrar_estabelecimento">Cadastrar</button>
			      </div>
			  </form>
		    </div>
		  </div>
		</div>
	<!-- fim add estabelecimentos -->

	<div id="cont_cadastro_success" style="display: none; position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: 100; background: rgba(0, 0, 0, 0.5);">

		<div style="width: 100%; height: 100%; display: flex; justify-content: center; align-items: center;">
			<div style="width: 90%; max-width: 300px; padding: 1rem; border-radius: 5px; background: #fff;">

				<button class="close btn_close">
					<span aria-hidden="true">&times;</span>
				</button>

				<img style="width: 100px; display: block; margin: auto;" src="img/emoticon-square-smiling-face-with-closed-eyes.png">

				<h5 style="text-align: center;">Cadastro realizado com sucesso.</h5>

				<p style="text-align: center;">Para acessar seu painel clique em "Login"</p>

				<button class="btn btn-block" id="btn_login_estabelecimento" style="color: #fff; background: #791313;">Login</button>
			</div>
		</div>
	</div>

	<!-- js File Start -->

	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<!-- jquery-3.1.1.min.js -->
	<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>
	<!--jquery.ajaxchimp.min.js  -->
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<!--jquery.easing.1.3.js  -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- bootstrap.min.js -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- owl.carousel.min.js -->
	<script src="js/isotope.pkgd.min.js"></script>
	<!-- isotope.pkgd.min.js -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<!-- jquery.magnific-popup.min.js -->
	<script src="js/skrollr.min.js"></script>
	<!-- skrollr.min.js -->
	<script src="js/utils.js"></script>
	<!-- utils.js -->
	<script src="js/jquery.parallax.js"></script>
	<!-- jquery.parallax.js -->
	<script src="js/wow.js"></script>
	<!-- wow.js -->

	<script src="js/main.js"></script>
	<!-- main.js -->
	<script type="text/javascript">
      $(function() {

        if($(document).scrollTop() > 50) {

          $('header').css({"background":'rgba(255, 255, 255, 0.5)'}).find('p').css({"color":"#000"});
        } else {
          
          $('header').css({"background":'transparent'}).find('p').css({"color":"#fff"});
        }
        
        $(document).on('scroll', function() {

          if($(document).scrollTop() > 50) {

            $('header').css({"background":'rgba(255, 255, 255, 0.5)'}).find('p').css({"color":"#000"});
          } else {
            
            $('header').css({"background":'transparent'}).find('p').css({"color":"#fff"});
          }
        })
      })
    </script>

    <script type="text/javascript" src="js/estabelecimento.js"></script>
</body>

</html>