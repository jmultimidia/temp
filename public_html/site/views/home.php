<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133926425-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133926425-1');
</script>
-->
    <title>Guia & Delivery - Baixe nosso App!</title>

    <meta charset="UTF-8" />
    <meta name="robots" content="index, follow">
    <meta name="description"
        content="Disponível para Android e iOS. É Guia, é Delivery, é Completo! Encontre e peça comida online nos principais restaurantes de Matinhos.">
    <meta name="keywords"
        content="guia e delivery, guia & delivery, aplicativo guia delivery, guia, delivery, caiobá, aplicativo de comida, app de comida, app de comida em matinhos, app de comida em caiobá, guia de matinhos, guia comercial,pedir comida onlinel, ifood em matinhos, delivery much em matinhos" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Anderson Barbosa Cavalcanti">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">


    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.theme.css">
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.transitions.css">
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/owl.carousel.css">
    <!-- Owl Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/magnific-popup.css">
    <!-- Magnific PopUP Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/isotope.css">
    <!-- Isotope Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/bootstrap.css" /> -->
    <!-- Bootstrap Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/et-lant-font.css" />
    <!-- Et-Lant Font Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/3dslider.css" />
    <!-- 3D Slider Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/animate.css" />
    <!-- Animate Stylesheet -->
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/material-design-iconic-font.css" />
    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/swiper.min.css">
    <!-- Iconic Font Stylesheet -->

    <link rel="stylesheet" href="<?= BASE ?>assets/css/style.css">
    <!-- Main Style.css Stylesheet -->

    <link rel="stylesheet" href="<?= BASE ?>assets/css/plugins/responsive.css">
    <!-- Resposive.css Stylesheet -->

    <style type="text/css">
    .img_1 {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        background-color: #fff;
        box-shadow: 0 18px 32px rgba(0, 0, 0, .08);
        margin: 0 auto;
        text-align: center;
        line-height: 100px;
        display: block;
        border-radius: 50%;
        overflow: hidden;
    }

    @media screen and (min-width: 500px) {

        #cont_add_estabelecimentos .form-group {
            width: 50% !important;
            float: left !important;
        }

        #cont_add_estabelecimentos .form-group:nth-child(1) {
            width: 100% !important;
        }

        .cont_2 {
            padding-right: 1rem;
        }

        .cont_1 {
            padding-left: 1rem;
        }
    }

    @media screen and (min-width: 990px) {

        .opt_1:nth-child(4) h3 {
            height: 36px;
        }
    }

    @media screen and (min-width: 1180px) {

        .opt_1:nth-child(2) h3 {
            padding-top: 30px;
            height: 92px;
        }

        .opt_1:nth-child(4) h3 {
            height: 70px;
        }
    }
    </style>
</head>

<body class="apps-craft-v18">

    <!-- Apps Craft Body Start 
  <div id="preloader"></div>
-->
    <!-- Apps Craft Main Menu -->
    <header
        style="position: fixed; top: 0; left: 0; width: 100%; z-index: 100; transition: 0.3s; padding: 1.5rem 1rem; background: transparent;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12" style="display: flex; justify-content: space-between; align-items: center;">
                    <div class="apps-craft-logo">
                        <a href="#apps-craft-home">
                            <img src="<?= BASE ?>assets/img/sticky-logo.png" style="height: 30px;"
                                alt="Guia & Delivery Logo">
                        </a>
                    </div>
                    <p style="margin-bottom: 0; color: #fff;">Guia & Delivery</p>
                </div>
            </div>
        </div>
    </header> <!-- .apps-craft-main-menu END -->

    <!-- Apps Craft Welcome Section -->
    <section class="apps-craft-welcome-section apps-craft-welcome-section-v3"
        style="background-image: url(img/welcome-version-2.jpg);" id="apps-craft-home">
        <div class="apps-craft-welcome-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="apps-craft-welcome-tbl">
                            <div class="apps-craft-welcome-tbl-c">
                                <div class="apps-craft-welcome-content text-center">
                                    <h1>Faça o download Gratuitamente do aplicativo <span>Guia & Delivery</span> </h1>
                                    <p>O APP Guia & Delivery foi feito pensando em você.</p>

                                    <div class="apps-craft-download-store-btn-group">
                                        <a href="https://play.google.com/store/apps/details?id=beta.ged2018.guiaedelivery"
                                            target="_blanck" class="apps-craft-btn play-store-btn"><img
                                                src="<?= BASE ?>assets/img/google-play-logo.png" alt=""></a>
                                        <a href="https://itunes.apple.com/us/app/guia-e-delivery-matinhos-pr/id1449202632?l=pt&ls=1&mt=8"
                                            target="_blanck" class="apps-craft-btn app-store-btn link_apple"><img
                                                src="<?= BASE ?>assets/img/app-store-logo.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="apps-craft-welcome-screenshort-v3 hidden-xs" itemscope
                    itemtype="http://schema.org/ImageGallery">
                    <figure itemprop="associatedMedia" class="text-right" itemscope
                        itemtype="http://schema.org/ImageObject">
                        <img src="<?= BASE ?>assets/img/welcome-screenshort-left.png"
                            alt="Apps Craft Welcome Left Screenshort" class="wow fadeIn" data-wow-delay=".2s">
                    </figure>

                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <img src="<?= BASE ?>assets/img/welcome-screenshort-center.png"
                            alt="Apps Craft Welcome Center Screenshort" class="wow fadeIn" data-wow-delay=".4s">
                    </figure>

                    <figure itemprop="associatedMedia" class="text-left" itemscope
                        itemtype="http://schema.org/ImageObject">
                        <img src="<?= BASE ?>assets/img/welcome-screenshort-right.png"
                            alt="Apps Craft Welcome Right Screenshort" class="wow fadeIn" data-wow-delay=".6s">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="apps-craft-feature-section section-padding" id="apps-craft-feature">
        <div class="container">
            <div class="apps-craft-section-heading">
                <h2>CARACTERÍSTICAS</h2>
            </div> <!-- .apps-craft-section-heading END -->
            <div class="row content-margin-top">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="apps-craft-feature-img text-center" itemscope itemtype="http://schema.org/ImageGallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                            <img src="<?= BASE ?>assets/img/feature-app-screenshort.png" alt="Apps Craft Feature Image">

                            <span class="apps-craft-feature-ico icon-1x" data-bottom="transform:translateX(-103px)"
                                data-top="transform:translateX(-63px);">
                                <img src="<?= BASE ?>assets/img/icon/Icon-1.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-2x" data-bottom="transform:translateX(-171px)"
                                data-top="transform:translateX(-131px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-2.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-3x" data-bottom="transform:translateX(-153px)"
                                data-top="transform:translateX(-113px);">
                                <img src="<?= BASE ?>assets/img/icon/12.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-4x" data-bottom="transform:translateX(-176px)"
                                data-top="transform:translateX(-136px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-5.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-5x" data-bottom="transform:translateX(-97px)"
                                data-top="transform:translateX(-55px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-7.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-6x" data-bottom="transform:translateY(-130px)"
                                data-top="transform:translateY(-90px);">
                                <img src="<?= BASE ?>assets/img/icon/15.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-7x" data-bottom="transform:translateY(-134px)"
                                data-top="transform:translateY(-94px);">
                                <img src="<?= BASE ?>assets/img/icon/icon-3.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-8x" data-bottom="transform:translateY(-160px)"
                                data-top="transform:translateY(-120px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON--9.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-9x" data-bottom="transform:translateX(-110px)"
                                data-top="transform:translateX(-70px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-11.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-10x" data-bottom="transform:translateY(26px)"
                                data-top="transform:translateY(26px);">
                                <img src="<?= BASE ?>assets/img/icon/13.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-11x" data-bottom="transform:translateX(74px)"
                                data-top="transform:translateX(34px);">
                                <img src="<?= BASE ?>assets/img/icon/ICon4.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-12x" data-bottom="transform:translateX(160px)"
                                data-top="transform:translateX(120px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-10.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-13x" data-bottom="transform:translateX(150px)"
                                data-top="transform:translateX(110px);">
                                <img src="<?= BASE ?>assets/img/icon/ICON-6.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-14x" data-bottom="transform:translateX(80px)"
                                data-top="transform:translateX(40px);">
                                <img src="<?= BASE ?>assets/img/icon/15.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-15x" data-bottom="transform:translateX(120px)"
                                data-top="transform:translateX(80px);">
                                <img src="<?= BASE ?>assets/img/icon/13.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->

                            <span class="apps-craft-feature-ico icon-16x" data-bottom="transform:translateX(-140px)"
                                data-top="transform:translateX(-100px);">
                                <img src="<?= BASE ?>assets/img/icon/18.png" alt="Small Icon">
                            </span> <!-- .apps-craft-feature-ico END -->
                        </figure>
                    </div> <!-- .apps-craft-feature-img END -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="apps-craft-feature-container clear-both">
                        <div class="apps-craft-single-feature opt_1">
                            <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="200ms">
                                <i class="zmdi zmdi-cutlery zmdi-hc-fw"></i>
                                <h3>Delivery pra quem quer reunir os amigos em casa</h3>
                                <p>Peça comida online nos melhores restaurantes diretamente pelo app, de forma prática e
                                    sem ligações telefônicas</p>
                            </div> <!-- .apps-craft-feature-content END -->
                        </div> <!-- .apps-craft-single-feature END -->
                        <div class="apps-craft-single-feature opt_1">
                            <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="400ms">
                                <i class="icon-wine apps-craft-round"></i>
                                <h3>É guia, é delivery, é completo!</h3>
                                <p>Pensamos em cada detalhe. Através do app você pode mandar um whatsapp, ligar,
                                    consultar o cardápio e ainda pedir online</p>
                            </div> <!-- .apps-craft-feature-content END -->
                        </div> <!-- .apps-craft-single-feature END -->
                        <div class="apps-craft-single-feature opt_1">
                            <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="600ms">
                                <i class="zmdi zmdi-wifi-alt zmdi-hc-fw"></i>
                                <h3>Um guia pra quem ama comer fora</h3>
                                <p>Encontre Restaurantes, Pizzarias e Lanchonetes. Descubra se tem Wifi, Fraldário,
                                    Espaço Kids e muito mais sobre o estabelecimento</p>
                            </div> <!-- .apps-craft-feature-content END -->
                        </div> <!-- .apps-craft-single-feature END -->
                        <div class="apps-craft-single-feature opt_1">
                            <div class="apps-craft-feature-content wow fadeIn" data-wow-delay="800ms">
                                <i class="icon-trophy apps-craft-round"></i>
                                <h3>Ótima Experiência</h3>
                                <p>O aplicativo Guia & Delivery foi feito pensando em você, por tanto, o aplicativo Guia
                                    & Delivery tem uma interface super agradável e facil de usar</p>
                            </div> <!-- .apps-craft-feature-content END -->
                        </div> <!-- .apps-craft-single-feature END -->
                    </div> <!-- .apps-craft-feature-container END -->
                </div>
            </div>
        </div>
    </section>

    <div class="apps-craft-video-section" id="video-section" style="background-image: url(img/video-bg.jpg);">
        <div class="apps-craft-video-section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="apps-craft-video-content">
                            <h4>Video Demo</h4>
                            <a href="<?= BASE ?>assets/img/10000000_287150911988161_2446577600880715754_n.mp4"
                                class="apps-craft-popup-video"><i
                                    class="zmdi zmdi-play-circle-outline zmdi-hc-fw"></i></a>
                        </div> <!-- .apps-craft-video-content END -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="apps-carft-screen-short-ssection section-padding" id="apps-carft-screen-short">
        <div class="container">
            <div class="apps-craft-section-heading">
                <h2>Algumas telas</h2>
            </div> <!-- .apps-craft-section-heading END -->

            <div class="row content-margin-top">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 img-full">
                    <!-- Swiper -->
                    <div
                        class="swiper-container two swiper-container-horizontal swiper-container-3d swiper-container-coverflow">
                        <div class="swiper-wrapper"
                            style="transform: translate3d(-1482.5px, 0px, 0px); transition-duration: 0ms;">
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                data-swiper-slide-index="0"
                                style="transform: translate3d(750px, 0px, -1125px) rotateX(0deg) rotateY(0deg); z-index: -7; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                data-swiper-slide-index="1"
                                style="transform: translate3d(600px, 0px, -900px) rotateX(0deg) rotateY(0deg); z-index: -5; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2"
                                style="transform: translate3d(450px, 0px, -675px) rotateX(0deg) rotateY(0deg); z-index: -4; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3"
                                style="transform: translate3d(300px, 0px, -450px) rotateX(0deg) rotateY(0deg); z-index: -2; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-prev"
                                data-swiper-slide-index="4"
                                style="transform: translate3d(150px, 0px, -225px) rotateX(0deg) rotateY(0deg); z-index: -1; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                                style="transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg); z-index: 1; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1"
                                style="transform: translate3d(-150px, 0px, -225px) rotateX(0deg) rotateY(0deg); z-index: 0; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="2"
                                style="transform: translate3d(-300px, 0px, -450px) rotateX(0deg) rotateY(0deg); z-index: -2; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide" data-swiper-slide-index="3"
                                style="transform: translate3d(-450px, 0px, -675px) rotateX(0deg) rotateY(0deg); z-index: -3; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="4"
                                style="transform: translate3d(-600px, 0px, -900px) rotateX(0deg) rotateY(0deg); z-index: -5; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                data-swiper-slide-index="0"
                                style="transform: translate3d(-750px, 0px, -1125px) rotateX(0deg) rotateY(0deg); z-index: -6; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-1.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                data-swiper-slide-index="1"
                                style="transform: translate3d(-900px, 0px, -1350px) rotateX(0deg) rotateY(0deg); z-index: -8; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-2.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2"
                                style="transform: translate3d(-1050px, 0px, -1575px) rotateX(0deg) rotateY(0deg); z-index: -9; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-3.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3"
                                style="transform: translate3d(-1200px, 0px, -1800px) rotateX(0deg) rotateY(0deg); z-index: -11; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-4.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="4"
                                style="transform: translate3d(-1350px, 0px, -2025px) rotateX(0deg) rotateY(0deg); z-index: -12; transition-duration: 0ms;">
                                <div class="slider-image">
                                    <img src="<?= BASE ?>assets/img/screenshort-slider-img-5.png" alt=""
                                        class="img-responsive">
                                </div>
                            </div>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span
                                class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span
                                class="swiper-pagination-bullet"></span><span
                                class="swiper-pagination-bullet"></span><span
                                class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet"></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!-- Apps Craft Team -->
    <section class="apps-craft-team-section" id="apps-craft-team">
        <div class="team-parallax-bg section-padding" data-0="background-position:10000px 50%;"
            data-100000="background-position:50000px 0px;">
            <div class="container">
                <div class="apps-craft-section-heading">
                    <h2>Categorias</h2>
                </div> <!-- .apps-craft-section-heading END -->
                <div class="row content-margin-top">
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/8_1535294245.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Restaurante</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/4_1535057519.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Pizza</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/5_1536374281.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Lanches</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/7_1535294155.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Comida oriental</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/3_1536372515.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Marmitex</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/9_1536276655.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Confeitaria e Doceria</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/10_1536371567.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Sorvete</h2>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 apps-craft-team" style="margin-bottom: 1rem;">
                        <figure class="apps-craft-team-img">
                            <div class="img_1"
                                style="background: #2379BD; border: 5px solid #fafafa; display: flex; justify-content: center; align-items: center;">
                                <img src="<?= BASE ?>assets/img/categorias/6_1535070233.png"
                                    style="width: 65%; height: 65%; object-fit: cover;"
                                    alt="Imagens principais categorias">
                            </div>
                        </figure>

                        <div class="apps-craft-team-bio">
                            <h2>Churrasco</h2>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section> <!-- .apps-craft-team-section END -->

    <!-- Apps Craft Now Available On -->
    <div class="apps-craft-now-available-section" id="apps-craft-available"
        style="background-image: url(<?= BASE ?>assets/img/now-available-on-round-bg.png);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="apps-craft-tbl">
                        <div class="apps-craft-tbl-c">
                            <div class="apps-craft-now-available-content wow fadeIn">
                                <h3>Disponível para</h3>
                                <p>O aplicativo Guia & Delivery surgiu da necessidade de reunir em um só lugar
                                    informações completas de restaurantes, lanchonetes, pizzarias e outros segmentos, de
                                    forma prática e rápida. O nosso grande diferencial é listar todos os
                                    estabelecimentos de comida gratuitamente. Assim, o usuário tem a oportunidade de
                                    conhecer todos os restaurantes da cidade, descobrir seus diferenciais, acessar o
                                    cardápio e a localização.</p>

                                <div class="apps-craft-download-store-btn-group">
                                    <a href="https://play.google.com/store/apps/details?id=beta.ged2018.guiaedelivery"
                                        target="_blanck" class="apps-craft-btn play-store-btn"><img
                                            src="<?= BASE ?>assets/img/google-play-logo.png" alt=""></a>
                                    <a href="https://itunes.apple.com/us/app/guia-e-delivery-matinhos-pr/id1449202632?l=pt&ls=1&mt=8"
                                        target="_blanck" class="apps-craft-btn app-store-btn link_apple"><img
                                            src="<?= BASE ?>assets/img/app-store-logo.png" alt=""></a>
                                </div> <!-- .apps-craft-download-store-btn-group END -->
                            </div> <!-- .apps-craft-now-available-content END -->
                        </div>
                    </div>
                    <figure class="apps-craft-app-secreenshort"
                        data-bottom="transform:translateY(180px) translateX(-50%);"
                        data-top="transform:translateY(100px) translateX(-50%);" itemscope
                        itemtype="http://schema.org/ImageGallery">
                        <img src="<?= BASE ?>assets/img/screenshort-now-available.png" alt="Apps Craft App Screenshort">
                    </figure> <!-- .apps-craft-app-secreenshort END -->
                </div>
            </div>
        </div>
    </div> <!-- .apps-craft-now-available-section END -->

    <!-- Apps Craft Footer Section -->
    <footer class="apps-craft-footer-section" id="apps-craft-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="apps-craft-subscribe-content">
                        <div class="wow fadeInUp" data-wow-delay=".3s">
                            <h2><span>&copy;</span> Todos os direitos reservados Guia & Delivery</h2>
                        </div>
                    </div> <!-- .apps-craft-subscribe-content END -->

                    <figure class="apps-craft-footer-logo text-center">
                        <a href="#">
                            <img src="<?= BASE ?>assets/img/icon.png" alt="Apps Craft Footer Logo" class="wow fadeInUp"
                                data-wow-delay=".7s">
                        </a>
                    </figure> <!-- .apps-craft-footer-logo END -->
                </div>
            </div>
        </div>
    </footer> <!-- .apps-craft-footer-section END -->

    <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery-3.1.1.min.js"></script>
    <!-- jquery-3.1.1.min.js -->
    <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery.ajaxchimp.min.js"></script>
    <!--jquery.ajaxchimp.min.js  -->
    <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/jquery.easing.1.3.js"></script>
    <!--jquery.easing.1.3.js  -->
    <script type="text/javascript" src="<?= BASE ?>assets/js/plugins/bootstrap.min.js"></script>
    <!-- bootstrap.min.js -->
    <script src="<?= BASE ?>assets/js/plugins/owl.carousel.min.js"></script>
    <!-- owl.carousel.min.js -->
    <script src="<?= BASE ?>assets/js/plugins/isotope.pkgd.min.js"></script>
    <!-- isotope.pkgd.min.js -->
    <script src="<?= BASE ?>assets/js/plugins/jquery.magnific-popup.min.js"></script>
    <!-- jquery.magnific-popup.min.js -->
    <script src="<?= BASE ?>assets/js/plugins/skrollr.min.js"></script>
    <!-- skrollr.min.js -->
    <script src="<?= BASE ?>assets/js/plugins/utils.js"></script>
    <!-- utils.js -->
    <script src="<?= BASE ?>assets/js/plugins/jquery.parallax.js"></script>
    <!-- jquery.parallax.js -->
    <script src="<?= BASE ?>assets/js/plugins/wow.js"></script>
    <!-- wow.js -->
    <script src="<?= BASE ?>assets/js/plugins/swiper.min.js"></script>
    <script src="<?= BASE ?>assets/js/plugins/main.js"></script>
    <!-- main.js -->
    <script type="text/javascript">
    $(function() {

        if ($(document).scrollTop() > 50) {

            $('header').css({
                "background": 'rgba(255, 255, 255, 0.5)'
            }).find('p').css({
                "color": "#000"
            });
            $('#btn_open_cont_add_estabelecimentos').css({
                "color": '#000',
                'border': "solid 1px #000"
            });
        } else {

            $('header').css({
                "background": 'transparent'
            }).find('p').css({
                "color": "#fff"
            });
            $('#btn_open_cont_add_estabelecimentos').css({
                "color": '#fff',
                'border': "solid 1px #fff"
            });
        }

        $(document).on('scroll', function() {

            if ($(document).scrollTop() > 50) {

                $('header').css({
                    "background": 'rgba(255, 255, 255, 0.5)'
                }).find('p').css({
                    "color": "#000"
                });
                $('#btn_open_cont_add_estabelecimentos').css({
                    "color": '#000',
                    'border': "solid 1px #000"
                });
            } else {

                $('header').css({
                    "background": 'transparent'
                }).find('p').css({
                    "color": "#fff"
                });
                $('#btn_open_cont_add_estabelecimentos').css({
                    "color": '#fff',
                    'border': "solid 1px #fff"
                });
            }
        })
    })
    </script>
</body>

</html>