<?php
error_reporting(0);
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'mvc_painel' . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);
define( "DS", DIRECTORY_SEPARATOR );
date_default_timezone_set("America/Fortaleza");
setlocale(LC_ALL, 'pt_BR');

require ROOT . 'vendor/autoload.php';
require ROOT . 'application/libs/Helper.php';
require ROOT . 'application/libs/Upload.php';
require APP . 'config/config.php';

$url = \AgendaLabs\Libs\Helper::splitUrl();

$tempodevida = 2678400; // 1 ano de vida
session_set_cookie_params($tempodevida);
session_cache_expire(605400);
@session_cache_limiter('nocache');
session_start();
setcookie(session_name(), session_id(), time() + $tempodevida, '/');

use AgendaLabs\Core\Route;

$Route = new Route($url);

// SITE
$Route->get('appwebview', 'SiteController@index');
$Route->get('appwebviewtest', 'SiteController@indexTest');
$Route->get('cidades', 'SiteController@cidade', 'id_cidade');
$Route->get('segmentos', 'SiteController@segmentos', 'id_segmento');
$Route->post('pesquisa', 'SiteController@pesquisa');
$Route->get('categorias', 'SiteController@categorias');
$Route->get('estabelecimentos', 'SiteController@estabelecimentos', 'id_segmento');
$Route->get('estabelecimento', 'SiteController@estabelecimento', 'id_loja');
$Route->get('item', 'SiteController@item', 'id');
$Route->get('api', 'SiteController@api', 'funcao');
$Route->get('promocoes', 'SiteController@promocoes');
$Route->get('servicos', 'SiteController@servicos');
$Route->get('eventos', 'SiteController@eventos');
$Route->get('noticias', 'SiteController@noticias');
$Route->get('radios', 'SiteController@radios');
$Route->get('ver-noticia', 'SiteController@noticia', 'id');
$Route->get('ver-evento', 'SiteController@evento', 'id');
$Route->get('ver-radio', 'SiteController@radio', 'id');
$Route->get('destaques', 'SiteController@destaques');
$Route->get('deliverys', 'SiteController@deliverys');
$Route->get('lojas', 'SiteController@lojas');
$Route->get('acidade', 'SiteController@acidade');
$Route->get('emergencia', 'SiteController@emergencia');
$Route->get('cardapios', 'SiteController@cardapios', 'id_loja');
$Route->get('favoritos', 'SiteController@favoritos');
$Route->get('addfavoritos', 'SiteController@addFavoritos', 'id_loja');
$Route->post('favoritar', 'SiteController@favoritar');
$Route->get('removefavoritos', 'SiteController@removeFavoritos', 'id_loja');
$Route->view('anuncie', 'app/home/anuncie');
$Route->view('faleconosco', 'app/home/faleconosco');
$Route->get('filtros', 'SiteController@filtros');
$Route->post('filtrar', 'SiteController@filtrar');
$Route->get('listarfiltros', 'SiteController@listarfiltros');
$Route->get('temp', 'SiteController@temp');

$Route->get('checkout', 'SiteController@checkout');
$Route->get('checkoutentrega', 'SiteController@checkoutentrega');
$Route->post('finalizarpedido', 'SiteController@finalizarpedido');
$Route->get('meuspedidos', 'SiteController@meuspedidos');
$Route->get('recebido', 'PedidoController@recebido', 'id');
$Route->post('avaliacao', 'SiteController@avaliacao');
$Route->post('cliente', 'ClienteController@update');

// ADMIN
$Route->view('login', 'admin/auth/login');
$Route->post('login', 'AuthController@login');
$Route->get('logout', 'AuthController@logout');

$Route->view('forgot', 'admin/auth/forgot');
$Route->post('forgot', 'AuthController@forgot');
$Route->get('remember', 'AuthController@remember', 'session');
$Route->post('newpassword', 'AuthController@newpassword');

// TODO: ############
$Route->group2('ajax', function () {
    \AgendaLabs\Libs\Helper::ajax($_POST['controller'], $_POST['action'], $_POST['param']);
    exit();
});

if (@$_SESSION['acesso'] == 'Administrador') {
}

if (@$_SESSION['acesso'] == 'Administrador' || @$_SESSION['acesso'] == 'Empresa'):
    $Route->group('system', function ($Route) {
        if (@$_SESSION['acesso'] == 'Administrador') {
            $Route->group('loja', function ($Route) {
                $Route->crud('loja');
            });
            $Route->group('usuario', function ($Route) {
                $Route->crud('user');
            });
            $Route->group('tamanho', function ($Route) {
                $Route->crud('tamanho');
            });
        }

        $Route->group('account', function ($Route) {
            $Route->get('', 'AuthController@account');
            $Route->post('save', 'AuthController@update');
        });

        $Route->group('meu-estabelecimento', function ($Route) {
            $Route->get('', 'EstabelecimentoController@viewEdit');
            $Route->get('remover', 'EstabelecimentoController@delete', 'id');
            $Route->post('salvar', 'EstabelecimentoController@update');
        });

        $Route->group('usuario', function ($Route) {
            $Route->crud('user');
        });
        $Route->group('tamanho', function ($Route) {
            $Route->crud('tamanho');
        });

        $Route->get('', 'HomeController@admin');

        $Route->group('categoria', function ($Route) {
            $Route->crud('categoria');
        });
        $Route->group('subcategoria', function ($Route) {
            $Route->crud('subcategoria');
        });
        $Route->group('produto', function ($Route) {
            $Route->crud('produto');
        });

        $Route->group('produto_valor', function ($Route) {
            $Route->post('cadastrar', 'ProdutoValorController@create');
            $Route->get('remover', 'ProdutoValorController@delete');
        });

        $Route->group('estabelecimento_parte', function ($Route) {
            $Route->post('salvar', 'EstabelecimentoParteController@createOrUpdate');
        });

        $Route->group('loja_parte', function ($Route) {
            $Route->post('salvar', 'LojaParteController@createOrUpdate');
        });

        $Route->group('adicional', function ($Route) {
            $Route->crud('adicional');
        });

        $Route->group('pedido', function ($Route) {
            $Route->get('', 'PedidoController@index');
            $Route->post('', 'PedidoController@index');
            $Route->get('painel', 'PedidoController@painel');
            $Route->get('situacao', 'PedidoController@situacao', 'id,situacao');
            $Route->get('frame', 'PedidoController@pedidoframe');
        });

        $Route->group('caixa', function ($Route) {
            $Route->crud('caixa');
        });

        $Route->get('venda', 'PedidoController@venda');
        $Route->post('venda', 'PedidoController@create');
        $Route->get('cupom', 'PedidoController@cupom', 'id_pedido');

        $Route->get('lancar', 'PedidoController@venda', 'acao,id');
        $Route->get('mesa', 'PedidoController@mesa');
        $Route->post('mesa', 'PedidoController@mesaCreate');

        $Route->group2('cozinha', function ($Route) {
            $Route->get('pedidos', 'CozinhaController@index');
            $Route->get('pronto/{id}', 'CozinhaController@pronto');
        });

        $Route->group('reportar', function ($Route) {
            $Route->crud('reportar');
        });

        $Route->group('cidade', function ($Route) {
            $Route->crud('cidade');
        });

        $Route->group('formapagamento', function ($Route) {
            $Route->crud('formapagamento');
        });

        $Route->group('facilidade', function ($Route) {
            $Route->crud('facilidade');
        });

        $Route->group('segmento', function ($Route) {
            $Route->crud('segmento');
        });

        $Route->group('cardapio', function ($Route) {
            $Route->crud('cardapio');
        });

        $Route->group('promocao', function ($Route) {
            $Route->crud('promocao');
        });

        $Route->group('radio', function ($Route) {
            $Route->crud('radio');
        });

        $Route->group('destaque', function ($Route) {
            $Route->crud('destaque');
        });

        $Route->group('evento', function ($Route) {
            $Route->crud('evento');
        });

        $Route->group('noticia', function ($Route) {
            $Route->crud('noticia');
        });

        $Route->group('servico', function ($Route) {
            $Route->crud('servico');
        });

    });
else:
    header('location: ' . URL_PUBLIC . '/segmentos');
endif;
