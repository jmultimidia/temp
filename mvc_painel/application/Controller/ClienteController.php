<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Cliente;
use AgendaLabs\Model\Site;

class ClienteController
{

    private $table    = 'cliente';
    private $baseView = 'admin/cliente';
    private $urlIndex = 'cliente';

    public function update()
    {        
        $cli      = new Cliente();
        $site     = new Site();
        $cliente  = $site->findCliente(@$_POST['id_celular']);
        $data = $cliente;

        if (!$cliente['id_celular']):
            $cli->create('cliente', ['id_celular' => $_POST['id_celular'], 'id_onesignal' => $_POST['id_onesignal']]);
            $data = $site->findCliente(@$_POST['id_celular']);
            if (!$data['id_onesignal']):
                $cli->save('cliente', ['id' => $data['id'], 'id_celular' => $data['id_celular'], 'id_onesignal' => $_POST['id_onesignal']]);            
                $data = $site->findCliente($data['id_celular']);
            endif;
        endif;                
        echo json_encode($data);        
    }

}