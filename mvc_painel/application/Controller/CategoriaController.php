<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Categoria;

class CategoriaController
{

    private $table    = 'categoria';
    private $baseView = 'admin/categoria';
    private $urlIndex = 'categoria';

    public function index()
    {
        $model    = new Categoria();
        $response = $model->allCategorias();
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        $model             = new Categoria();
        $response['lojas'] = $model->all('loja', 'nome DESC');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function viewEdit($param)
    {
        $model             = new Categoria();
        $response          = $model->find($this->table, $param['id']);
        $response['lojas'] = $model->all('loja', 'nome DESC');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model = new Categoria();
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }

        $id = $model->create($this->table, $_POST, ['id', 'image']);
        if ($id) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model = new Categoria();
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }

        if ($model->save($this->table, $_POST, ['image'])) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit/' . $_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = new Categoria();
        $model->delete($this->table, 'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function ordem($param)
    {
        $model   = new Categoria();
        $id_loja = '';
        if (@$_SESSION['acesso'] == 'Empresa') {
            $id_loja = $_SESSION['id_loja'];
        }

        $model = new Categoria();
        $model->defineOrder($this->table, $param['id'], $param['ordem'], $id_loja);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
