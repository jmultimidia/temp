<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Evento;
use AgendaLabs\Model\LojaParte;
use AgendaLabs\Model\Noticia;
use AgendaLabs\Model\Produto;
use AgendaLabs\Model\Radio;
use AgendaLabs\Model\Site;
use AgendaLabs\Model\User;

class SiteController
{
    private $baseView = 'app';

//    function __construct($url)
    //    {
    //        if(!$_SESSION['id_cidade'] || $_SESSION['id_celular']) {
    //            header('location: '. URL_PUBLIC . '/appwebview');
    //        }
    //    }

    public function index()
    {

        $response['session'] = print_r($_SESSION['id_celular'], true);

        $model                  = new Site();
        $_SESSION['id_celular'] = @$_GET['info'];
        $cliente                = $model->findCliente($_SESSION['id_celular']);

        $_SESSION['id_user'] = $cliente['id'];
        if (@$cliente['id_cidade']) {
            $_SESSION['id_cidade'] = $cliente['id_cidade'];
            $favoritos             = $model->all('cliente_favoritos', 'id', ['id_celular'], [@$_SESSION['id_celular']]);
            foreach ($favoritos as $item) {
                $_SESSION['favoritos'][$item['id_loja']] = $item['id_loja'];
            }
            header('location: ' . URL_PUBLIC . '/segmentos');
            exit();
        } else { /* elseif (@$_GET['info']) {
        $_SESSION['id_user'] = $model->create('cliente', ['id_celular' => @$_GET['info']]);
        }  */
            $response['cidades'] = $model->all('cidade', 'cidade', 'status', 1);

            Helper::view($this->baseView . '/home/cidades', $response);
        }
    }

    public function indexTest()
    {
        $response['session'] = print_r($_SESSION['id_celular'], true);

        $model                  = new Site();
        $_SESSION['id_celular'] = @$_GET['info'];
        $cliente                = $model->findCliente($_SESSION['id_celular']);

        $_SESSION['id_user'] = $cliente['id'];
        if (@$cliente['id_cidade']) {
            $_SESSION['id_cidade'] = $cliente['id_cidade'];
            $favoritos             = $model->all('cliente_favoritos', 'id', ['id_celular'], [@$_SESSION['id_celular']]);
            foreach ($favoritos as $item) {
                $_SESSION['favoritos'][$item['id_loja']] = $item['id_loja'];
            }
            header('location: ' . URL_PUBLIC . '/segmentos');
            exit();
        } else { /* elseif (@$_GET['info']) {
        $_SESSION['id_user'] = $model->create('cliente', ['id_celular' => @$_GET['info']]);
        }  */
            $response['cidades'] = $model->all('cidade', 'cidade');

            Helper::view($this->baseView . '/home/cidades', $response);
        }
    }

    public function cidade($param)
    {
        $_SESSION['id_cidade'] = $param['id_cidade'];
        //echo "cidade: $_SESSION[id_cidade] usuario: $_SESSION[id_user] celular: $_SESSION[id_celular]";
        $model = new Site();
        if ($_SESSION['id_user']) {
            $model->save('cliente', ['id' => $_SESSION['id_user'], 'id_cidade' => $param['id_cidade']]);
        }
        header('location: ' . URL_PUBLIC . '/segmentos');
    }

    public function segmentos()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);
        $model               = new Site();
        $expira_em           = 5; //DEFINE EM MINUTOS A EXPIRAÇÃO DO ACESSO DO USUARIO
        $sessao              = session_id();
        $tempo_on            = date('Y-m-d H:i:s');
        $tempo_fim           = date('Y-m-d H:i:s', mktime(date('H'), date('i') - $expira_em, date('s'), date('m'), date('d'), date('Y')));
        $model->deleteVisita('visitas_online', $tempo_fim);
        $visitante = $model->findVisita('visitas_online', $sessao);
        if ($visitante):
            $id = $model->saveVisita('visitas_online', ['sessao' => $sessao, 'tempo' => $tempo_on, 'ip' => $_SERVER['REMOTE_ADDR']]);
        else:
            $model->create('visitas_online', ['sessao' => $sessao, 'tempo' => $tempo_on, 'ip' => $_SERVER['REMOTE_ADDR']]);
            $visitas = $model->find('configuracao', 1);
            $model->save('configuracao', ['id' => 1, 'visitas' => $visitas['visitas'] + 1]);
        endif;

        $response['segmentos'] = $model->all('segmento', 'ordem', 'destaque', 1);
        $response['eventos']   = $model->slideEventos();
        $response['promocoes'] = $model->slidePromocoes();
        $response['destaques'] = $model->slideDestaques();
        $response['noticias']  = $model->slideNoticias();
        $response['radios']    = $model->slideRadios();
        Helper::view($this->baseView . '/home/segmentos', $response);
    }

    public function categorias()
    {
        $model                 = new Site();
        $response['segmentos'] = $model->all('segmento', 'ordem', 'destaque', 1);
        Helper::view($this->baseView . '/home/categorias', $response);
    }

    public function filtros()
    {
        $model                   = new Site();
        $response['segmentos']   = $model->all('segmento', 'ordem', 'destaque', 1);
        $response['pagamentos']  = $model->all('formapagamento', 'formapagamento');
        $response['facilidades'] = $model->all('facilidade', 'facilidade');
        $response['bairros']     = $model->all('bairro', 'bairro', 'id_cidade', $_SESSION['id_cidade']);
        Helper::view($this->baseView . '/home/filtro', $response);
    }

    public function filtrar()
    {
        $_SESSION['filtros']['id_segmento']       = $_POST['id_segmento'];
        $_SESSION['filtros']['id_facilidade']     = $_POST['id_facilidade'];
        $_SESSION['filtros']['id_formapagamento'] = $_POST['id_formapagamento'];
        $_SESSION['filtros']['pedidoonline']      = $_POST['pedidoonline'];
        $_SESSION['filtros']['delivery']          = $_POST['delivery'];
        $_SESSION['filtros']['bairro']            = $_POST['bairro'];        
        header('location: ' . URL_PUBLIC . '/listarfiltros');
    }

    public function listarfiltros()
    {       
        $model                     = new Site();
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->filtrar($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/filtrado', $response);
    }

    public function favoritos()
    {
        $response['session'] = print_r($_SESSION['id_celular'], true);

        $model                  = new Site();

        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->allFavoritos($_SESSION['id_celular'], $diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/favoritos', $response);
    }

    public function pesquisa()
    {
        $model                     = new Site();
        $response['pesquisa']      = $_POST['palavra-chave'];
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->allPesquisa($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/pesquisa', $response);
    }

    public function estabelecimentos($param)
    {
        $response['session'] = print_r($_SESSION['id_celular'], true);

        $model                     = new Site();
        $response['segmento']      = $model->find('segmento', $param['id_segmento']);
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->allLojas($param['id_segmento'], $diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/estabelecimentos', $response);
    }

    public function deliverys()
    {
        $model                     = new Site();
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->allDeliverys($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
                foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/deliverys', $response);
    }

    public function lojas()
    {
        $model                     = new Site();
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->Lojas($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/lojas', $response);
    }

    public function acidade()
    {

        $model                     = new Site();
        $response['segmentos']     = $model->all('segmento', 'segmento');
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->Acidade($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/acidade', $response);
    }

    public function servicos()
    {
        $model                     = new Site();
        $response['segmentos']     = $model->all('segmento', 'segmento');
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->Servico($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/servicos', $response);
    }

    public function emergencia()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);

        $model                     = new Site();
        $response['segmentos']     = $model->all('segmento', 'segmento');
        $diaDaSemana               = date('N');
        $diasDaSemana              = [1 => 'Segunda', 2 => 'Terça', 3 => 'Quarta', 4 => 'Quinta', 5 => 'Sexta', 6 => 'Sábado', 7 => 'Domingo'];
        $response['lojas']         = $model->Emergencia($diasDaSemana[$diaDaSemana]);
        $response['lojasDestaque'] = $response['demaisLojas'] = [];
        foreach ($response['lojas'] as $loja) {
            if ($loja['destaque'] && $loja['aberto']) {
                $response['lojasDestaque'][] = $loja;
            } elseif ($loja['aberto']) {
                $response['lojasAbertas'][] = $loja;
            } else {
                $response['demaisLojas'][] = $loja;
            }
        }
        Helper::view($this->baseView . '/home/emergencia', $response);
    }

    public function estabelecimento($param)
    {
        $model                = new Site();
        $response['cardapio'] = $model->all('cardapio', 'id DESC', 'id_loja', $param['id_loja']);
        $response['loja']     = $model->find('loja', $param['id_loja']);

        $expira_em   = 5; //DEFINE EM MINUTOS A EXPIRAÇÃO DO ACESSO DO USUARIO
        $sessao_loja = $response['loja']['id'] . session_id();
        $tempo_on    = date('Y-m-d H:i:s');
        $tempo_fim   = date('Y-m-d H:i:s', mktime(date('H'), date('i') - $expira_em, date('s'), date('m'), date('d'), date('Y')));
        $model->deleteVisita('visitas_online', $tempo_fim);
        $visitante = $model->findVisita('visitas_online', $sessao_loja);
        if ($visitante):
            $id = $model->saveVisita('visitas_online', ['sessao' => $sessao_loja, 'tempo' => $tempo_on, 'ip' => $_SERVER['REMOTE_ADDR']]);
        else:
            $model->create('visitas_online', ['sessao' => $sessao_loja, 'tempo' => $tempo_on, 'ip' => $_SERVER['REMOTE_ADDR']]);
            $visitas = $model->find('loja', $response['loja']['id']);
            $model->save('loja', ['id' => $response['loja']['id'], 'visitas' => $visitas['visitas'] + 1]);
        endif;

        if (($_SESSION['pedido']['id_loja']) != $response['loja']['id']) {
            unset($_SESSION['pedido']);
        }
        $diaDaSemana  = date('N');
        $diasDaSemana = [
            1 => 'Segunda',
            2 => 'Terça',
            3 => 'Quarta',
            4 => 'Quinta',
            5 => 'Sexta',
            6 => 'Sábado',
            7 => 'Domingo',
        ];
        $response['loja']['aberto'] = $model->aberto($param['id_loja'], $diasDaSemana[$diaDaSemana]);
        $response['horario']        = $model->all('loja_horario', 'id ASC', 'id_loja', $param['id_loja']);
        $response['formapagamento'] = $model->allFormaPagamento($param['id_loja']);
        $response['facilidade']     = $model->allFacilidade($param['id_loja']);

        $produtos             = $model->allProdutos($param['id_loja']);
        $response['produtos'] = $response['categorias'] = [];
        foreach ($produtos as $produto) {
            $response['categorias'][$produto['id_categoria']] = $produto['categoria'];
            $response['produtos'][$produto['id_categoria']][] = $produto;

            $id_adicional = $model->all('adicional_produto', 'id', 'id_produto', $produto['id'])['id_adicional'];
            if ($id_adicional) {
                foreach ([$id_adicional] as $id_adc) {
                    $response['produtos'][$produto['id']]['adicionais'] = $model->all('adicional', 'id', 'id', $id_adc);
                }
            }
        }

        Helper::view($this->baseView . '/home/estabelecimento', $response);
    }

    public function item($param)
    {
        $model    = new Produto();
        $response = $model->find('produto', $param['id']);
        $model->save('produto', ['id' => $response['id'], 'visitas' => $response['visitas'] + 1]);
        Helper::view($this->baseView . '/home/item', $response);
    }

    public function api($param)
    {
        $model = new Site();
        if ($param['funcao'] == "info"):
            $response['info'] = $model->find('configuracao', 1);
        endif;
        Helper::view($this->baseView . '/home/api', $response);
    }

    public function evento($param)
    {
        $model    = new Evento();
        $response = $model->find('evento', $param['id']);
        $model->save('evento', ['id' => $response['id'], 'visitas' => $response['visitas'] + 1]);
        Helper::view($this->baseView . '/home/evento', $response);
    }

    public function radio($param)
    {
        $model    = new Radio();
        $response = $model->find('radio', $param['id']);
        $model->save('radio', ['id' => $response['id'], 'visitas' => $response['visitas'] + 1]);
        Helper::view($this->baseView . '/home/radio', $response);
    }

    public function noticia($param)
    {
        $model    = new Noticia();
        $response = $model->find('noticia', $param['id']);
        $model->save('noticia', ['id' => $response['id'], 'visitas' => $response['visitas'] + 1]);
        Helper::view($this->baseView . '/home/noticia', $response);
    }

    public function promocoes()
    {
        $model    = new Site();
        $response = $model->allPromocoes();
        Helper::view($this->baseView . '/home/promocoes', $response);
    }

    public function destaques()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);

        $model    = new Site();
        $response = $model->allDestaques();
        Helper::view($this->baseView . '/home/destaques', $response);
    }

    public function eventos()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);

        $model    = new Site();
        $response = $model->allEventos();
        Helper::view($this->baseView . '/home/eventos', $response);
    }

    public function radios()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);

        $model    = new Site();
        $response = $model->allRadios();
        Helper::view($this->baseView . '/home/radios', $response);
    }

    public function noticias()
    {
        $response['session'] = print_r($_SESSION['id_celular'] ?? false, true);

        $model    = new Site();
        $response = $model->allNoticias();
        Helper::view($this->baseView . '/home/noticias', $response);
    }

    public function cardapios($param)
    {
        $model                = new Site();
        $response['cardapio'] = $model->all('cardapio', 'nome ASC', 'id_loja', $param['id_loja']);
        $response['loja']     = $model->find('loja', $param['id_loja']);
        Helper::view($this->baseView . '/home/cardapios', $response);
    }

    public function addFavoritos($param)
    {
        $model = new Site();
        if ($model->create('cliente_favoritos', ['id_celular' => $_SESSION['id_celular'], 'id_loja' => $param['id_loja']])) {
            $_SESSION['favoritos'][$param['id_loja']] = $param['id_loja'];
        }
        header('location: ' . URL_PUBLIC . '/favoritos');
    }
    
    public function favoritar($param)
    {        
        //$post_id = $_POST['post_id'];
        //$action = $_POST['action'];
        $action = $param['action'];
        $model = new Site();

        switch ($action) {
            case 'like':
                try {
                $model->create('cliente_favoritos', ['id_celular' => $_SESSION['id_celular'], 'id_loja' => $param['id_loja']]);
                $_SESSION['favoritos'][$param['id_loja']] = $param['id_loja'];                
            } catch (Exception $e) {
                return false;
            } 
               break;
            case 'unlike':
                try {
                $model->delete('cliente_favoritos', ['id_celular', 'id_loja'], [$_SESSION['id_celular'], $param['id_loja']]);
                unset($_SESSION['favoritos'][$param['id_loja']]);
            } catch (Exception $e) {
                return false;
            } 
               break;            
            default:
                break;
        }
        
    }

    public function removeFavoritos($param)
    {
        $model = new Site();
        $model->delete('cliente_favoritos', ['id_celular', 'id_loja'], [$_SESSION['id_celular'], $param['id_loja']]);
        unset($_SESSION['favoritos'][$param['id_loja']]);
        header('location: ' . URL_PUBLIC . '/favoritos');
    }

    public function form()
    {
        Helper::trataMail($_POST['param']);
    }

    public function meuspedidos()
    {
        $model               = new Site();
        $response['pedidos'] = $model->pedidosUser($_SESSION['id_user']);
        foreach ($response['pedidos'] as $key => $item) {
            $response['pedidos'][$key]['produtos']      = $model->produtoPedido($item['id']);
            $response['pedidos'][$key]['adc']           = $model->adcObs($item['id']);
            $response['pedidos'][$key]['tempo_entrega'] = $model->find('loja', $response['pedidos'][$key]['id_loja'])['tempo_entrega'];
        }
        Helper::view($this->baseView . '/home/meuspedidos', $response);
    }

    public function checkoutSave()
    {
        $params = [];
        parse_str($_POST['param'], $params);
        if (is_array($_SESSION['pedido'])) {
            if (is_array(@$params['pedido'])) {
                $_SESSION['pedido'] = array_merge($_SESSION['pedido'], $params['pedido']);
            } else {
                $_SESSION['pedido'] = array_merge($_SESSION['pedido'], $params);
            }
        } else {
            $_SESSION['pedido'] = $params['pedido'];
        }
    }

    public function checkout()
    {
        $model = new Site();

        $response['loja']           = $model->find('loja', $_SESSION['pedido']['id_loja']);
        $lojaPartes                 = new LojaParte();
        $response['loja']['partes'] = json_encode($lojaPartes->partesEIndicesPorTamanho($_SESSION['pedido']['id_loja']));

        $response['bairros'] = $model->all('loja_frete', 'id', 'id_loja', $response['loja']['id']);
        for ($i = 0; $i < count($response['bairros']); $i++) {
            $response['bairros'][$i]['bairro'] = $model->find('bairro', $response['bairros'][$i]['id_bairro'])['bairro'];
        }

        $response['formapagamento'] = $model->allFormaPagamento($response['loja']['id']);

        $response['produtos'] = [];
        $produtoModel         = new Produto;

        foreach ($_SESSION['pedido']['produto'] as $id => $qtdes) {
            foreach ($qtdes as $idTamanho => $qtde) {
                if ($qtde > 0) {
                    //print_r(['id' => $id, 'id_produto' => $idProduto, 'id_tamanho' => $idTamanho]);
                    //exit;
                    $produto = $produtoModel->getValue($id, $idTamanho);
                    if ($produto['varios_sabores']) {
                        $sabores            = $produtoModel->allSimilarProducts($_SESSION['pedido']['id_loja'], $produto['id_subcategoria'], $produto['tipo'], $produto['id_tamanho']) ?? [];
                        $produto['sabores'] = $sabores;
                    }

                    $response['produtos'][$id . '_' . $idTamanho]               = $produto;
                    $response['produtos'][$id . '_' . $idTamanho]['adicionais'] = $model->allAdicionais($id);
                    $response['produtos'][$id . '_' . $idTamanho]['qtde']       = $qtde;

                    $adcs = $model->all('adicional_produto', 'id', 'id_produto', $id);
                    foreach ($adcs as $adc) {
                        $ad = $model->all('adicional_grupo', 'id', 'id_grupo', $adc['id_adicional']);
                        foreach ($ad as $a) {
                            $response['grupos'][$a['id_adicional']] = $a;
                        }
                    }
                }
            }
        }

        $_SESSION['pedido']['id_loja'] = $response['loja']['id'];
//        $_SESSION['pedido']['produtos'] = $response['produtos'];

        /*
        echo "<pre>";
        print_r($response);
        exit;
        //*/

        if (COUNT($response['produtos']) > 0) {
            Helper::view($this->baseView . '/home/checkout', $response);
        } else {
            header('location: ' . URL_PUBLIC . '/estabelecimento/' . $response['loja']['id']);
        }
    }

    public function checkoutentrega()
    {
        $model                    = new Site();
        $response                 = $_SESSION['pedido'];
        $response['loja']         = $model->find('loja', $_SESSION['pedido']['id_loja']);
        $response['ultimoPedido'] = $model->ultimoPedido($_SESSION['id_user']);
        /*
        echo "<pre>";
        print_r($response);
        exit;
        //*/
        Helper::view($this->baseView . '/home/checkoutentrega', $response);
    }

    public function finalizarpedido()
    {
        if (isset($_SESSION['pedido']['produtos']) && COUNT($_SESSION['pedido']['produtos']) > 0) {
            $model            = new Site();
            $response['loja'] = $model->find('loja', $_SESSION['pedido']['id_loja']);

            $pedido['id_loja']       = $_SESSION['pedido']['id_loja'];
            $pedido['id_user']       = $_SESSION['id_user'];
            $pedido['tipo']          = $_SESSION['pedido']['tipo'];
            $pedido['id_pagamento']  = $_SESSION['pedido']['id_pagamento'];
            $pedido['nome']          = $_POST['nome'];
            $pedido['celular']       = $_POST['celular'];
            $pedido['endereco']      = $_POST['endereco'];
            $pedido['numero']        = $_POST['numero'];
            $pedido['complemento']   = $_POST['complemento'];
            $pedido['ptoreferencia'] = $_POST['ptoreferencia'];
            $pedido['bairro']        = ($_SESSION['pedido']['tipo'] == 'Entregar' ? $_POST['bairro'] : '');
            $pedido['id_cidade']     = $_SESSION['id_cidade'];
            $pedido['situacao']      = 'Pendente';
            $response['id_pedido']   = $model->create('pedido', $pedido);

            $preco = 0;
            foreach ($_SESSION['pedido']['produtos'] as $item) {
                $descricao[$item['id_produto']] = [];
                $precoAdicional                 = 0;
                $modelProduto                   = new Produto();
                $produto                        = $modelProduto->getValue($item['id_produto'], $item['id_tamanho'], $item['sabores'] ?? []);
                $sabores                        = '';
                if (!empty(trim($item['sabores']))) {
                    $saboresIds = trim($item['sabores']);
                    $value      = $modelProduto->getValueBySabores($saboresIds, $item['id_tamanho']);

                    $sabores = $modelProduto->getSabores($saboresIds, $item['id_tamanho']);
                    if ($response['loja']['preco_fracionario'] == 'maior') {
                        $produto['preco'] = $value['max'];
                    } else {
                        $produto['preco'] = $value['avg'];
                    }
                }

                if ($produto) {
                    if (!empty($item['adicional'])) {
                        $descricao[$item['id_produto']] = [];
                        foreach ($item['adicional'] as $itemAdicional) {
                            $adicional                        = $model->find('adicional', $itemAdicional);
                            $descricao[$item['id_produto']][] = $adicional['nome'] . ' (R$ ' . Helper::valor($adicional['preco']) . ')';
                            $preco += $adicional['preco'];
                        }
                    }

                    $produtoSalvar['id_loja']    = $_SESSION['pedido']['id_loja'];
                    $produtoSalvar['id_pedido']  = $response['id_pedido'];
                    $produtoSalvar['id_produto'] = $item['id_produto'];
                    $produtoSalvar['id_tamanho'] = $item['id_tamanho'];
                    $produtoSalvar['sabores']    = $sabores;
                    $produtoSalvar['qtde']       = $item['qtde'];
                    $produtoSalvar['descricao']  = implode(', ', $descricao[$item['id_produto']]);
                    $produtoSalvar['preco']      = $item['qtde'] * $produto['preco'];
                    $produtoSalvar['descricao'] .= ($item['obs'] ? '<br>Obs:' . $item['obs'] : '');
                    $preco += $produtoSalvar['preco'];
                    $model->create('pedido_produto', $produtoSalvar);
                }
            }

            $pedidoSalvar['id']           = $response['id_pedido'];
            $pedidoSalvar['valor_pedido'] = $preco + ($_SESSION['pedido']['valor_frete'] && @$_SESSION['pedido']['tipo'] == 'Entregar' ? $_SESSION['pedido']['valor_frete'] : 0);
            if ($_SESSION['pedido']['valor_troco']) {
                $pedidoSalvar['valor_pago'] = $preco;
            }
            if ($_SESSION['pedido']['valor_troco']) {
                $pedidoSalvar['valor_troco'] = $_SESSION['pedido']['valor_troco'] - $pedidoSalvar['valor_pedido'];
            }
            $pedidoSalvar['id_onesignal'] = $model->find('cliente', $_SESSION['id_user'])['id_onesignal'];
            $model->save('pedido', $pedidoSalvar);

            $user      = new User();
            $aparelhos = $user->all('user', 'id', 'id_loja', $_SESSION['pedido']['id_loja']);

            $ids = [];
            foreach ($aparelhos as $aparelho) {
                if ($aparelho['id_onesignal']) {
                    $ids[] = $aparelho['id_onesignal'];
                }
            }
            //print_r($ids);

            $content = array(
                "en" => "Você recebeu um novo pedido!",
            );

            $fields = array(
                'app_id'             => "9d680ccc-8c20-4e25-877a-a731e3e3ce7a",
                'include_player_ids' => $ids,
                'data'               => array("foo" => "bar"),
                'contents'           => $content,
            );

            $fields = json_encode($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $push = curl_exec($ch);
            curl_close($ch);

            $response['code'] = 'success';
            $response['msg']  = '<b>Pedido realizado com sucesso!</b><br><br>O estabelecimento deverá confirmar que o seu pedido está em produção.';
        } else {
            $response['code'] = 'error';
            $response['msg']  = 'Aconteceu um erro no seu pedido.<br>Por favor ligue direto no estabelecimento e informe esse erro.';
        }
        unset($_SESSION['pedido']);
        Helper::view($this->baseView . '/home/checkoutfinalizado', $response);
    }

    public function temp()
    {

        $response['session'] = print_r($_SESSION['id_celular'], true);

        Helper::view($this->baseView . '/home/temp', $response);
        
    }    

}