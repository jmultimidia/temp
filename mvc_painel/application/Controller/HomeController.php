<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Dashboard;

class HomeController
{

    public function admin()
    {
        $model               = new Dashboard();
        $response['pedidos'] = $model->relPedidos();
        $response['views']   = $model->views();
        Helper::view('admin/home/index', $response);
    }

    public function client()
    {
        Helper::view('admin/home/index');
    }

}
