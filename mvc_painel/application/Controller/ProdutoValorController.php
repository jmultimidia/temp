<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\ProdutoValor;
use AgendaLabs\Model\Produto;
use AgendaLabs\Libs\Helper;

class ProdutoValorController
{

    private $table = 'produto_valores';
    private $baseView = 'admin/produto';
    private $urlIndex = 'produto/editar/';

    public function create()
    {
	if (!isset($_POST['id_produto'], $_POST['id_tamanho'], $_POST['valor'])) {
	    return header('location: ' . URL_ADMIN . '/produto');
	}
	$produto = new Produto();
	if (!$produto->checkExistsById($_POST['id_produto'])) {
	    return header('location: ' . URL_ADMIN . '/produto');
	}
	$dados = [];
	$dados['id_produto'] = $_POST['id_produto'];
	$dados['id_tamanho'] = $_POST['id_tamanho'];
	$dados['valor'] = Helper::valor($_POST['valor'],1);
        $model = new ProdutoValor();
        $id = $model->create($this->table,$dados);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex.$_POST['id_produto']);
    }

    public function delete($params = [])
    {
	if (count($params) !== 2) {
	    return header('location: ' . URL_ADMIN . '/produto');
	}
	$produto = new Produto();
	$id_produto = current($params);
	$id = end($params);
	if (!$produto->checkExistsById($id_produto)) {
	    return header('location: ' . URL_ADMIN . '/produto');
	}
        $model = new ProdutoValor();
        $model->delete($this->table, ['id', 'id_produto'], [$id, $id_produto]);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex.$id_produto);
    }
}
