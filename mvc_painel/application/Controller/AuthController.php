<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Auth;

class AuthController
{

    public function login()
    {
        $login = new Auth();
        $user  = $login->login();

        //falha no login
        if (!$user) {
            header('location: ' . URL_PUBLIC . '/login?msg=1');
            exit();
        }

        if ($user['nome']) {
            $_SESSION['nome']    = $user['nome'];
            $_SESSION['id_user'] = $user['id'];
            $_SESSION['id_loja'] = $user['id_loja'];
            $_SESSION['acesso']  = $user['acesso'];
            $_SESSION['imagem']  = $user['imagem'];
        }

        if ($_POST['phone']) {
            $_SESSION['phone'] = $_POST['phone'];
            header('location: ' . URL_ADMIN . '/pedido/painel');
        } else {
            header('location: ' . URL_ADMIN);
        }
        exit();
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        header('location: ' . URL_PUBLIC . '/login');
        exit();
    }

    public function forgot()
    {
        $model   = new Auth();
        $account = $model->forgot();
        if ($account['id']) {
            $account['session'] = str_replace([' ', '.'], '', microtime());
            $model->save('user', $account);
            Helper::trataMail(['email' => $account['email'], 'tipo' => 'forgot', 'session' => $account['session']]);
            header('location: ' . URL_ADMIN);
        } else {
            $response['error'] = 'E-mail não encontrado';
            Helper::view('admin/auth/forgot', $response);
        }

    }

    public function remember($param)
    {
        $model    = new Auth();
        $response = $model->forgot($param);
        Helper::view('admin/auth/remember', $response);
    }

    public function newpassword()
    {
        $model      = new Auth();
        $save['id'] = $_POST['id'];
        $salt       = \AgendaLabs\Libs\Helper::salt();
        if ($_POST['senha']) {
            $save['senha'] = hash('sha512', $_POST['senha']);
            $save['salt']  = $salt;
        }

        if ($model->save('user', $save)) {
            header('location: ' . URL_ADMIN);
        } else {
            Helper::view('admin/auth/edit/');
        }
    }

    public function account()
    {
        $model    = new Auth();
        $response = $model->find('user', $_SESSION['id_user']);
        Helper::view('admin/auth/edit', $response);
    }

    public function meuestabelecimento()
    {
        $model    = new Loja();
        $response = $model->find('user', $_SESSION['id_user']);
        Helper::view('admin/loja/edit', $response);
    }

    public function update()
    {
        $salt                         = \AgendaLabs\Libs\Helper::salt();
        $model                        = new Auth();
        $id                           = $save['id']                           = $_SESSION['id_user'];
        $save['nome']                 = $_SESSION['nome']                 = $_POST['nome'];
        $save['telefone']             = $_POST['telefone'];
        $save['email']                = $_POST['email'];
        $save['id_onesignal']         = $_POST['id_onesignal'];
        $save['json_estabelecimento'] = $_POST['json_estabelecimento'];
        if ($_POST['senha']) {
            $save['senha'] = hash('sha512', $_POST['senha']);
            $save['salt']  = $salt;
        }

        if ($model->save('user', $save)) {
            $caminho     = 'files/user/';
            $nome_imagem = $id . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save('user', ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
                $_SESSION['imagem'] = $caminho . $nome_imagem . '.' . $formato;
            }
            header('location: ' . URL_ADMIN . '/account');
        } else {
            Helper::view('admin/estabelecimento/edit/');
        }
    }

    public function updateestabelecimento()
    {
        $salt                         = \AgendaLabs\Libs\Helper::salt();
        $model                        = new Auth();
        $id                           = $save['id']                           = $_SESSION['id_user'];
        $save['nome']                 = $_SESSION['nome']                 = $_POST['nome'];
        $save['telefone']             = $_POST['telefone'];
        $save['email']                = $_POST['email'];
        $save['id_onesignal']         = $_POST['id_onesignal'];
        $save['json_estabelecimento'] = $_POST['json_estabelecimento'];
        if ($_POST['senha']) {
            $save['senha'] = hash('sha512', $_POST['senha']);
            $save['salt']  = $salt;
        }

        if ($model->save('user', $save)) {
            $caminho     = 'files/user/';
            $nome_imagem = $id . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save('user', ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
                $_SESSION['imagem'] = $caminho . $nome_imagem . '.' . $formato;
            }
            header('location: ' . URL_ADMIN . '/meu-estabelecimento');
        } else {
            Helper::view('admin/estabelecimento/edit/');
        }
    }

    public function autorizacao($param)
    {
        $login          = new Auth();
        $_POST['login'] = $param['login'];
        $_POST['senha'] = $param['senha'];
        $user           = $login->login();
        echo $user['id'];
    }

}
