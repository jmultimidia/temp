<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\LojaParte;
use AgendaLabs\Libs\Helper;

class LojaParteController
{

    private $table = 'loja_partes';
    private $baseView = 'admin/loja';
    private $urlIndex = 'loja/editar/';

    public function createOrUpdate()
    {
        if (!isset($_POST['id_loja'], $_POST['id_tamanho'])) {
            return header('location: ' . URL_ADMIN . '/loja');
        }

        if (empty($_POST['partes']) || is_int($_POST['partes'])) {
            $_POST['partes'] = 1;
        }

        $this->deleteAll($_POST['id_loja'], $_POST['id_tamanho']);
        $model = new LojaParte();
	$params = [
            'id_loja' => $_POST['id_loja'],
            'id_tamanho' => $_POST['id_tamanho'],
            'partes' => $_POST['partes'],
        ];

        $model->create($this->table,$params);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex.$_POST['id_loja']);
    }

    private function deleteAll($idLoja, $idTamanho)
    {
        $model = new LojaParte();
        $model->delete($this->table, ['id_loja', 'id_tamanho'], [$idLoja, $idTamanho]);
    }
}
