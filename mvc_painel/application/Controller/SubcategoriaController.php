<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Subcategoria;
use AgendaLabs\Libs\Helper;

class SubcategoriaController
{

    private $table = 'subcategoria';
    private $baseView = 'admin/subcategoria';
    private $urlIndex = 'subcategoria';

    public function index()
    {
        $model = new Subcategoria();
        $response = $model->allSubcategorias();
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        $model = new Subcategoria();
        $response['lojas'] = $model->all('loja','nome DESC');
        Helper::view($this->baseView.'/edit',$response);
    }

    public function viewEdit($param)
    {
        $model = new Subcategoria();
        $response = $model->find($this->table,$param['id']);
        $response['lojas'] = $model->all('loja','nome DESC');
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = new Subcategoria();
        if(@$_SESSION['acesso'] == 'Empresa') $_POST['id_loja'] = $_SESSION['id_loja'];
        $id = $model->create($this->table,$_POST,['id','image']);
        if($id) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = new Subcategoria();
        if(@$_SESSION['acesso'] == 'Empresa') $_POST['id_loja'] = $_SESSION['id_loja'];
        if($model->save($this->table,$_POST,['image'])) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = new Subcategoria();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
