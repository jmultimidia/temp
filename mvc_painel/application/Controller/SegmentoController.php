<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Segmento;
use AgendaLabs\Libs\Helper;

class SegmentoController
{

    private $table = 'segmento';
    private $baseView = 'admin/segmento';
    private $urlIndex = 'segmento';

    public function index()
    {
        $model = New Segmento();       
        $response = $model->all($this->table,'ordem');
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        Helper::view($this->baseView.'/edit');
    }

    public function viewEdit($param)
    {
        $model = New Segmento();
        $response = $model->find($this->table,$param['id']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Segmento();
        $id = $model->create($this->table,$_POST,['id','image']);
        if($id) {
            $caminho = 'files/segmento/';
            $nome_imagem = $id.'_'.time();
            $formato = 'png';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = New Segmento();
        if($model->save($this->table,$_POST,['image'])) {
            $id = $_POST['id'];
            $caminho = 'files/segmento/';
            $nome_imagem = $id.'_'.time();
            $formato = 'png';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }
    
    public function delete($param)
    {
        $model = New Segmento();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function ordem($param)
    {
        $model = New Segmento();
        $model->defineOrder($this->table,$param['id'],$param['ordem']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}