<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Adicional;
use AgendaLabs\Libs\Helper;

class AdicionalController
{

    private $table = 'adicional';
    private $baseView = 'admin/adicional';
    private $urlIndex = 'adicional';

    public function index()
    {
        $model = New Adicional();
        $response = $model->allAdicionais();
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        $model = New Adicional();
        $response['lojas'] = $model->all('loja','nome DESC');
        $response['produtos'] = $model->all('produto','nome','id_loja',$_SESSION['id_loja']);
        $response['adicionais'] = $model->all('adicional','nome',['id_loja','tipo'],[$_SESSION['id_loja'],'Muitos']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function viewEdit($param)
    {
        $model = New Adicional();
        $response = $model->find($this->table,$param['id']);
        $response['lojas'] = $model->all('loja','nome DESC');
        $response['produtos'] = $model->all('produto','nome','id_loja',$_SESSION['id_loja']);
        $response['grupo'] = $model->all('adicional_grupo','id','id_grupo',$param['id']);
        $produtos = $model->all('adicional_produto','id','id_adicional',$param['id']);
        $response['produtosAdicionais'] = [];
        foreach ($produtos as $produto) {
            $response['produtosAdicionais'][] = $produto['id_produto'];
        }

        $response['adicionais'] = $model->all('adicional','nome',['id_loja','tipo'],[$_SESSION['id_loja'],'Muitos']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Adicional();
        if(!@$_POST['estoque']) $_POST['estoque'] = 0;
        if($_POST['preco']) {
            $_POST['preco'] = Helper::valor($_POST['preco'],1);
        } else {
            $_POST['preco'] = 0;
        }
        if(@$_SESSION['acesso'] == 'Empresa') $_POST['id_loja'] = $_SESSION['id_loja'];
        $id = $model->create($this->table,$_POST,['id','image','produtos','grupo']);
        if($id) {
            foreach ((array)@$_POST['produtos'] as $produto) {
                $model->create('adicional_produto',['id_produto'=>$produto,'id_adicional'=>$id]);
            }
            foreach ((array)@$_POST['grupo'] as $grupo) {
                $model->create('adicional_grupo',['id_adicional'=>$grupo,'id_grupo'=>$id]);
            }
            $caminho = 'files/adicional/';
            $nome_imagem = $id.'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = New Adicional();
        if(!@$_POST['estoque']) $_POST['estoque'] = 0;
        if($_POST['preco']) {
            $_POST['preco'] = Helper::valor($_POST['preco'],1);
        } else {
            $_POST['preco'] = 0;
        }
        if(@$_SESSION['acesso'] == 'Empresa') $_POST['id_loja'] = $_SESSION['id_loja'];
        if($model->save($this->table,$_POST,['image','produtos','grupo'])) {
            $model->delete('adicional_produto','id_adicional',$_POST['id']);
            $model->delete('adicional_grupo','id_grupo',$_POST['id']);
            foreach ((array)@$_POST['produtos'] as $produto) {
                $model->create('adicional_produto',['id_produto'=>$produto,'id_adicional'=>$_POST['id']]);
            }
            foreach ((array)@$_POST['grupo'] as $grupo) {
                $model->create('adicional_grupo',['id_adicional'=>$grupo,'id_grupo'=>$_POST['id']]);
            }
            $caminho = 'files/adicional/';
            $nome_imagem = $_POST['id'].'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$_POST['id'],'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = New Adicional();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
