<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Produto;
use AgendaLabs\Model\ProdutoValor;
use AgendaLabs\Model\Tamanho;
use AgendaLabs\Model\Tipo;

class ProdutoController
{
    private $table    = 'produto';
    private $baseView = 'admin/produto';
    private $urlIndex = 'produto';

    public function index()
    {
        $model    = new Produto();
        $response = $model->allProdutos();
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        $model                     = new Produto();
        $tipos                     = new Tipo();
        $tamanhos                  = new Tamanho();
        $response['tipos']         = $tipos->allTipos();
        $response['tamanhos']      = $tamanhos->allTamanhos();
        $response['lojas']         = $model->all('loja', 'nome ASC');
        $response['minha_loja']    = $model->find('loja', $_SESSION['id_loja']);
        $response['categorias']    = $model->all('categoria', 'nome ASC', 'id_loja', $_SESSION['id_loja']);
        $response['subcategorias'] = $model->all('subcategoria', 'nome ASC', 'id_loja', $_SESSION['id_loja']);
        Helper::view($this->baseView . '/edit', $response);
    }

    public function viewEdit($param)
    {
        $model    = new Produto();
        $response = $model->find($this->table, $param['id']);

        $tipos                       = new Tipo();
        $tamanhos                    = new Tamanho();
        $produtoValores              = new ProdutoValor();
        $response['tipos']           = $tipos->allTipos();
        $response['tamanhos']        = $tamanhos->allTamanhos();
        $response['produto_valores'] = $produtoValores->byProduto($param['id']);
        $response['minha_loja']      = $model->find('loja', $_SESSION['id_loja']);
        $id_loja                     = $response['id_loja'];

        $response['lojas'] = $model->all('loja', 'nome ASC');
        //$response['categorias'] = $model->all('categoria','nome ASC','id_loja',$_SESSION['id_loja']);
        $response['categorias']    = $model->all('categoria', 'nome ASC', 'id_loja', $id_loja);
        $response['subcategorias'] = $model->all('subcategoria', 'nome ASC', 'id_loja', $id_loja);
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model = new Produto();
        //$response['sou_loja'] = $model->getLoja();
        if (!$_POST['estoque']) {
            $_POST['estoque'] = 0;
        }
        if ($_POST['preco']) {
            $_POST['preco'] = Helper::valor($_POST['preco'], 1);
        } else {
            $_POST['preco'] = 0;
        }
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }
        if (empty($_POST['id_subcategoria'])) {
            unset($_POST['id_subcategoria']);
        }
        $id = $model->create($this->table, $_POST, ['id', 'image']);
        if ($id) {
            $caminho     = 'files/produto/';
            $nome_imagem = $id . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model                   = new Produto();
        $_POST['varios_sabores'] = $_POST['varios_sabores'] ?? 0;
        if (empty($_POST['id_subcategoria'])) {
            unset($_POST['id_subcategoria']);
        }

        if (empty($_POST['estoque'])) {
            $_POST['estoque'] = 0;
        }
        if ($_POST['preco']) {
            $_POST['preco'] = Helper::valor($_POST['preco'], 1);
        } else {
            $_POST['preco'] = 0;
        }
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }
        if ($model->save($this->table, $_POST, ['image'])) {
            $caminho     = 'files/produto/';
            $nome_imagem = $_POST['id'] . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save($this->table, ['id' => $_POST['id'], 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit/' . $_POST['id']);
        }

    }

    public function delete($param)
    {
        $model = new Produto();
        $model->delete($this->table, 'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function selectCategorias($param)
    {
        $model                  = new Produto();
        $return                 = '';
        $response['categorias'] = $model->all('categoria', 'nome ASC', 'id_loja', $param['id_loja']);
        foreach ($response['categorias'] as $item) {
            $return .= '<option value="' . $item['id'] . '">' . $item['nome'] . '</option>';
        }
        echo $return;
    }

    public function ordem($param)
    {
        $id_loja = '';
        if (@$_SESSION['acesso'] == 'Empresa') {
            $id_loja = $_SESSION['id_loja'];
        }

        $model = new Produto();
        $model->defineOrder($this->table, $param['id'], $param['ordem'], $id_loja);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    /* public function souLoja()
{
$model = new Produto();
$return = '';
$response['sou_loja'] = $model->getLoja();
$return = $response['sou_loja'];
echo $return;
} */
}
