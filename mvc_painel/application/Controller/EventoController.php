<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Evento;

class EventoController
{

    private $table    = 'evento';
    private $baseView = 'admin/evento';
    private $urlIndex = 'evento';

    public function index()
    {
        $model    = new Evento();
        $response = $model->all($this->table, 'ordem');
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        $model               = new Evento();
        $response['cidades'] = $model->all('cidade', 'cidade');
        $response['lojas']   = $model->all('loja', 'nome DESC');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function viewEdit($param)
    {
        $model               = new Evento();
        $response            = $model->find($this->table, $param['id']);
        $response['cidades'] = $model->all('cidade', 'cidade');
        $response['bairros'] = [];
        foreach ($response['cidades'] as $cidade) {

            $response['bairros'][$cidade['id']] = [];
            $bairros                            = $model->all('bairro', 'bairro', 'id_cidade', $cidade['id']);
            foreach ($bairros as $bairro) {

                $ja = $model->all('loja_frete', 'id', ['id_loja', 'id_bairro'], [$param['id'], $bairro['id']]);
                if (!$ja) {
                    $response['bairros'][$cidade['id']][] = $bairro['id'] . ':' . $bairro['bairro'];
                }
            }
        }
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model = new Evento();
        if (empty($_POST['data_inicio'])):
            $_POST['data_inicio'] = "0000-00-00";
        else:
            $_POST['data_inicio'] = Helper::data($_POST['data_inicio'], 1);
        endif;
        $_POST['data_fim'] = Helper::data($_POST['data_fim'], 1);
        if (empty($_POST['agendamento'])):
            $_POST['agendamento'] = "2019-01-01";
        else:
            $_POST['agendamento'] = Helper::data($_POST['agendamento'], 1);
        endif;
        if (empty($_POST['validade'])):
            $_POST['validade'] = "2999-12-31";
        else:
            $_POST['validade'] = Helper::data($_POST['validade'], 1);
        endif;

        $id = $model->create($this->table, $_POST, ['id', 'image']);
        if ($id) {
            $caminho          = 'files/evento/';
            $nome_imagem      = $id . '_' . time();
            $nome_imagem_capa = $id . '_capa_' . time();
            $formato          = 'jpg';
            if (Helper::upload($_FILES['imagem_capa'], $nome_imagem_capa, $caminho, $formato, 320, 320)) {
                $model->save($this->table, ['id' => $id, 'imagem_capa' => $caminho . $nome_imagem_capa . '.' . $formato]);
            }
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 800, 800)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model = new Evento();
        if (empty($_POST['data_inicio'])):
            $_POST['data_inicio'] = "0000-00-00";
        else:
            $_POST['data_inicio'] = Helper::data($_POST['data_inicio'], 1);
        endif;
        $_POST['data_fim'] = Helper::data($_POST['data_fim'], 1);
        if (empty($_POST['agendamento'])):
            $_POST['agendamento'] = "2019-01-01";
        else:
            $_POST['agendamento'] = Helper::data($_POST['agendamento'], 1);
        endif;
        if (empty($_POST['validade'])):
            $_POST['validade'] = "2999-12-31";
        else:
            $_POST['validade'] = Helper::data($_POST['validade'], 1);
        endif;
        if ($model->save($this->table, $_POST, ['image'])) {
            $id               = $_POST['id'];
            $caminho          = 'files/evento/';
            $nome_imagem      = $id . '_' . time();
            $nome_imagem_capa = $id . '_capa_' . time();
            $formato          = 'jpg';
            if (Helper::upload($_FILES['imagem_capa'], $nome_imagem_capa, $caminho, $formato, 320, 320)) {
                $model->save($this->table, ['id' => $id, 'imagem_capa' => $caminho . $nome_imagem_capa . '.' . $formato]);
            }
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 800, 800)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit/' . $_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = new Evento();
        $model->delete($this->table, 'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function ordem($param)
    {
        $model = new Evento();
        $model->defineOrder($this->table, $param['id'], $param['ordem']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}