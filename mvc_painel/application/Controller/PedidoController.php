<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Pedido;

class PedidoController
{

    private $table    = 'pedido';
    private $baseView = 'admin/pedido';
    private $urlIndex = 'pedido';

    public function index()
    {
        $model               = new Pedido();
        $response['lojas']   = $model->all('loja', 'nome ASC');
        $response['pedidos'] = $model->allPedidos();
        if (@$_POST['id_loja'] || $_SESSION['acesso'] == 'Empresa') {
            $response['loja'] = $model->find('loja', ($_POST['id_loja'] ?: $_SESSION['id_loja']));
        }

        Helper::view($this->baseView . '/index', $response);
    }

    public function pedidoframe()
    {
        $model                  = new Pedido();
        $response['lojas']      = $model->all('loja', 'nome ASC');
        $response['pedidos']    = $model->allPedidos(1);
        $response['showNotice'] = false;

        foreach ($response['pedidos'] as $key => $pedido) {
            $response['pedidos'][$key]['produtos'] = $model->produtoPedido($pedido['id']);
            $response['pedidos'][$key]['adc']      = $model->adiciObs($pedido['id'], $model->produtoPedido($pedido['id'])['id_produto']);
            if ($pedido['mostrar_aviso']) {
                $response['showNotice'] = true;
                $model->setShowNotice($pedido['id'], false);
            }
        }
        Helper::view($this->baseView . '/pedidosframe', $response);
    }

    public function painel()
    {
        $model               = new Pedido();
        $response['lojas']   = $model->all('loja', 'nome ASC');
        $response['pedidos'] = $model->allPedidos(1);
        foreach ($response['pedidos'] as $key => $pedido) {
            $response['pedidos'][$key]['produtos'] = $model->produtoPedido($pedido['id']);
        }
        Helper::view($this->baseView . '/pedidos', $response);
    }

    public function loadpainel()
    {
        $model = new Pedido();
//        $response['lojas'] = $model->all('loja','nome ASC');
        $response['pedidos'] = $model->allPedidos(1);
        foreach ($response['pedidos'] as $key => $pedido) {
            $response['pedidos'][$key]['produtos'] = $model->produtoPedido($pedido['id']);
        }
        echo json_encode($response);
    }

    public function situacao($param)
    {
        $model = new Pedido();
        $model->save('pedido', ['id' => $param['id'], 'situacao' => ucfirst($param['situacao'])]);

        $msg = '';

        switch ($param['situacao']) {
            case 'producao':
                $msg = 'Obrigado! Seu pedido está sendo produzido.';
                break;
            case 'cancelado':
                $msg = 'Seu pedido foi cancelado.';
                break;
            case 'saiu':
                $msg = 'Fique atento! Seu pedido saiu para entrega.';
                break;
            case 'entregue':
                $msg = 'Obrigado! Seu pedido foi entregue.';
                break;
            default:
                $msg = 'O status do seu pedido mudou.';
        }

        $content = array(
            "en" => $msg,
        );

        $fields = array(
            //'app_id' => "70eba91e-2ebb-480f-ba48-364799d001cb",
            'app_id'             => "9d680ccc-8c20-4e25-877a-a731e3e3ce7a",
            'include_player_ids' => array($_GET['push']),
            'data'               => array("numero" => $param['id']),
            'contents'           => $content,
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        header('location: ' . URL_ADMIN . '/pedido/frame');
    }

    public function recebido($param)
    {
        $situacao = "entregue";
        $model    = new Pedido();
        $model->save('pedido', ['id' => $param['id'], 'situacao' => ucfirst($situacao)]);

        $msg = 'Obrigado! Seu pedido foi entregue.';

        $content = array(
            "en" => $msg,
        );

        $fields = array(
            //'app_id' => "70eba91e-2ebb-480f-ba48-364799d001cb",
            'app_id'             => "9d680ccc-8c20-4e25-877a-a731e3e3ce7a",
            'include_player_ids' => array($_GET['push']),
            'data'               => array("numero" => $param['id']),
            'contents'           => $content,
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);
        header('location: ' . URL_PUBLIC . '/meuspedidos');
    }

}
