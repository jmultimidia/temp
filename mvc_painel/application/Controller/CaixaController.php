<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Caixa;
use AgendaLabs\Libs\Helper;

class CaixaController
{

    private $table = 'caixa';
    private $baseView = 'admin/caixa';
    private $urlIndex = 'caixa';

    public function index()
    {
        $model = New Caixa();
        $response['caixas'] = $model->allCaixa();
        $response['caixasAbertos'] = $this->caixaAberto();
        $response['usuarios'] = $model->all('user','nome','id_loja',$_SESSION['id_loja']);
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        Helper::view($this->baseView.'/edit');
    }

    public function viewEdit($param)
    {
        $model = New Caixa();
        $response = $model->find($this->table,$param['id']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Caixa();
        $cadastrar['id_loja'] = $_SESSION['id_loja'];
        $cadastrar['id_user'] = $_POST['id_user'];
        $cadastrar['data_abertura'] = date('Y-m-d H:i:s');
        $cadastrar['id_user_abertura'] = $_SESSION['id_user'];
        $cadastrar['valor_abertura'] = Helper::valor(($_POST['valor']?:0),1);
        $id = $model->create($this->table,$cadastrar);
        if($id) {
            sleep(1);
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        }
    }

    public function update()
    {
        $model = New Caixa();
        $caixa = $this->caixaAberto($_POST['id_caixa']);
        if(@$caixa[0]['id'] && $_POST['id_caixa']) {
            $salvar['id'] = $caixa[0]['id'];
            $salvar['data_fechamento'] = date('Y-m-d H:i:s');
            $salvar['id_user_fechamento'] = $_SESSION['id_user'];
            $salvar['valor_fechamento'] = Helper::valor(($_POST['valor']?:0),1);
            $valoresFechamento = $model->valoresFechamento($caixa[0]['data_abertura'],$caixa[0]['id_user']);
            $salvar['valor_pedidos'] = $valoresFechamento['pedidos'];
            $salvar['valor_descontos'] = $valoresFechamento['descontos'];
            $salvar['valor_total'] = $valoresFechamento['total'];
            if($model->save($this->table,$salvar)) {
                $extrato = $model->extrato($caixa[0]['data_abertura'],$salvar['data_fechamento'],$caixa[0]['id_user']);
                $extratoSalvar['extrato'] = '';
                foreach ($extrato as $item) {
                    $extratoSalvar['extrato'] .= $item['pagamento'] . ': R$ ' . Helper::valor($item['total']) . '<br>';
                }
                $extratoSalvar['id'] = $salvar['id'];
                $model->save($this->table,$extratoSalvar);
                header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
            } else {
                Helper::view($this->baseView.'/edit/'.$_POST['id']);
            }
        }
    }

    public function delete($param)
    {
        $model = New Caixa();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function caixaAberto($id = false)
    {
        $model = New Caixa();
        return $model->caixaAberto($id);
    }

}
