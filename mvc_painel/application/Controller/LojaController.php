<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Loja;
use AgendaLabs\Model\LojaParte;

class LojaController
{

    private $table    = 'loja';
    private $baseView = 'admin/loja';
    private $urlIndex = 'loja';

    public function index()
    {
        $model    = new Loja();
        $response = $model->all($this->table, 'id DESC');
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        $model                       = new Loja();
        $response['cidades']         = $model->all('cidade', 'cidade');
        $response['segmentos']       = $model->all('segmento', 'segmento');
        $response['formapagamentos'] = $model->all('formapagamento', 'formapagamento');
        $response['facilidades']     = $model->all('facilidade', 'facilidade');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function viewEdit($param)
    {
        $model     = new Loja();
        $lojaParte = new LojaParte();

        $response                    = $model->find($this->table, $param['id']);
        $response['cidades']         = $model->all('cidade', 'cidade');
        $response['segmentos']       = $model->all('segmento', 'segmento');
        $response['formapagamentos'] = $model->all('formapagamento', 'formapagamento');
        $response['facilidades']     = $model->all('facilidade', 'facilidade');
        $response['tamanhos']        = $lojaParte->partesPorTamanhoELoja($param['id']);

        $response['taxas'] = $model->all('loja_frete', 'id', 'id_loja', $param['id']);
        for ($i = 0; $i < count($response['taxas']); $i++) {

            $response['taxas'][$i]['cidade'] = $model->find('cidade', $response['taxas'][$i]['id_cidade'])['cidade'];
            $response['taxas'][$i]['bairro'] = $model->find('bairro', $response['taxas'][$i]['id_bairro'])['bairro'];
        }

        $response['bairros'] = [];
        foreach ($response['cidades'] as $cidade) {

            $response['bairros'][$cidade['id']] = [];
            $bairros                            = $model->all('bairro', 'bairro', 'id_cidade', $cidade['id']);
            foreach ($bairros as $bairro) {

                $ja = $model->all('loja_frete', 'id', ['id_loja', 'id_bairro'], [$param['id'], $bairro['id']]);
                if (!$ja) {
                    $response['bairros'][$cidade['id']][] = $bairro['id'] . ':' . $bairro['bairro'];
                }
            }
        }

        $formapagamento                 = $model->all('loja_formapagamento', 'id', 'id_loja', $param['id']);
        $response['ids_formapagamento'] = [];
        foreach ($formapagamento as $item) {
            $response['ids_formapagamento'][] = $item['id_formapagamento'];
        }
        $facilidade                 = $model->all('loja_facilidade', 'id', 'id_loja', $param['id']);
        $response['ids_facilidade'] = [];
        foreach ($facilidade as $item) {
            $response['ids_facilidade'][] = $item['id_facilidade'];
        }
        $segmento                 = $model->all('loja_segmento', 'id', 'id_loja', $param['id']);
        $response['ids_segmento'] = [];
        foreach ($segmento as $item) {
            $response['ids_segmento'][] = $item['id_segmento'];
        }
        $response['funcionamento'] = $model->all('loja_horario', 'id ASC', 'id_loja', $param['id']);
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model             = new Loja();
        $_POST['comissao'] = $_POST['comissao'] ?: 0;
        $id                = $model->create($this->table, $_POST, ['id', 'image', 'segmento', 'formapagamento', 'facilidade', 'funcionamento']);
        if ($id) {
            foreach ((array) @$_POST['formapagamento'] as $item) {
                $model->create('loja_formapagamento', ['id_loja' => $id, 'id_formapagamento' => $item]);
            }
            foreach ((array) @$_POST['facilidade'] as $item) {
                $model->create('loja_facilidade', ['id_loja' => $id, 'id_facilidade' => $item]);
            }
            foreach ((array) @$_POST['segmento'] as $item) {
                $model->create('loja_segmento', ['id_loja' => $id, 'id_segmento' => $item]);
            }
            $i = 0;
            foreach ((array) @$_POST['funcionamento']['dia'] as $item) {
                if ($_POST['funcionamento']['hora_inicio'][$i] && $_POST['funcionamento']['hora_fim'][$i]) {
                    $model->create('loja_horario', [
                        'id_loja'     => $id,
                        'dia'         => $_POST['funcionamento']['dia'][$i],
                        'hora_inicio' => $_POST['funcionamento']['hora_inicio'][$i],
                        'hora_fim'    => $_POST['funcionamento']['hora_fim'][$i],
                    ]);
                }
                $i++;
            }
            $caminho     = 'files/loja/';
            $nome_imagem = $id . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            $nome_imagem = $nome_imagem . '_capa';
            if (Helper::upload($_FILES['capa'], $nome_imagem, $caminho, $formato, 800, 800)) {
                $model->save($this->table, ['id' => $id, 'capa' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model = new Loja();
        if ($_GET['loja']) {

            $model->create('loja_frete', $_POST);
        } else {

            $_POST['comissao'] = $_POST['comissao'] ?: 0;

            if ($model->save($this->table, $_POST, ['image', 'segmento', 'formapagamento', 'facilidade', 'funcionamento'])) {

                $id = $_POST['id'];
                $model->delete('loja_formapagamento', 'id_loja', $id);
                foreach ((array) @$_POST['formapagamento'] as $item) {
                    $model->create('loja_formapagamento', ['id_loja' => $id, 'id_formapagamento' => $item]);
                }
                $model->delete('loja_facilidade', 'id_loja', $id);
                foreach ((array) @$_POST['facilidade'] as $item) {
                    $model->create('loja_facilidade', ['id_loja' => $id, 'id_facilidade' => $item]);
                }
                $model->delete('loja_segmento', 'id_loja', $id);
                foreach ((array) @$_POST['segmento'] as $item) {
                    $model->create('loja_segmento', ['id_loja' => $id, 'id_segmento' => $item]);
                }
                $model->delete('loja_horario', 'id_loja', $id);
                $i = 0;
                foreach ((array) @$_POST['funcionamento']['dia'] as $item) {
                    if ($_POST['funcionamento']['hora_inicio'][$i] && $_POST['funcionamento']['hora_fim'][$i]) {
                        $model->create('loja_horario', [
                            'id_loja'     => $id,
                            'dia'         => $_POST['funcionamento']['dia'][$i],
                            'hora_inicio' => $_POST['funcionamento']['hora_inicio'][$i],
                            'hora_fim'    => $_POST['funcionamento']['hora_fim'][$i],
                        ]);
                    }
                    $i++;
                }
                $caminho     = 'files/loja/';
                $nome_imagem = $_POST['id'] . '_' . time();
                $formato     = 'jpg';
                if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                    $model->save($this->table, ['id' => $_POST['id'], 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
                }
                $nome_imagem = $nome_imagem . '_capa';
                if (Helper::upload($_FILES['capa'], $nome_imagem, $caminho, $formato, 800, 800)) {
                    $model->save($this->table, ['id' => $id, 'capa' => $caminho . $nome_imagem . '.' . $formato]);
                }
                header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
            } else {
                Helper::view($this->baseView . '/edit/' . $_POST['id']);
            }
        }
    }

    public function ordem($param)
    {
        $id_loja = '';
        if (@$_SESSION['acesso'] == 'Empresa') {
            $id_loja = $_SESSION['id_loja'];
        }

        $model = new Loja();
        $model->defineOrder($this->table, $param['id'], $param['ordem'], $id_loja);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function delete($param)
    {
        $model = new Loja();

        if ($_GET['loja']) {
            $model->delete('loja_frete', 'id', $param['id']);
            //header('location: ' . URL_ADMIN . '/' . $this->urlIndex . '/editar/' . $_GET['loja']);
        } else {
            $model->delete($this->table, 'id', $param['id']);
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        }
    }

}
