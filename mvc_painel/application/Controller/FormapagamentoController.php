<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Formapagamento;
use AgendaLabs\Libs\Helper;

class FormapagamentoController
{

    private $table = 'formapagamento';
    private $baseView = 'admin/formapagamento';
    private $urlIndex = 'formapagamento';

    public function index()
    {
        $model = New Formapagamento();
        $response = $model->all($this->table,'id DESC');
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        Helper::view($this->baseView.'/edit');
    }

    public function viewEdit($param)
    {
        $model = New Formapagamento();
        $response = $model->find($this->table,$param['id']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Formapagamento();
        $id = $model->create($this->table,$_POST,['id','image']);
        if($id) {
            $caminho = 'files/formapagamento/';
            $nome_imagem = $id.'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = New Formapagamento();
        if($model->save($this->table,$_POST,['image'])) {
            $id = $_POST['id'];
            $caminho = 'files/formapagamento/';
            $nome_imagem = $id.'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,200,200)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = New Formapagamento();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
