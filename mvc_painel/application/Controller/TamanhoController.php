<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\Tamanho;

class TamanhoController
{

    private $table    = 'tamanhos';
    private $baseView = 'admin/tamanho';
    private $urlIndex = 'tamanho';

    public function index()
    {
        $model    = new Tamanho();
        $response = $model->all($this->table, 'id DESC');
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        Helper::view($this->baseView . '/edit');
    }

    public function viewEdit($param)
    {
        $model    = new Tamanho();
        $response = $model->find($this->table, $param['id']);
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model = new Tamanho();
        $id    = $model->create($this->table, $_POST);
        if ($id) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model = new Tamanho();

        if ($model->save($this->table, $_POST)) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit/' . $_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = new Tamanho();
        $model->delete($this->table, 'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);

    }

}
