<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Libs\Helper;
use AgendaLabs\Model\User;

class UserController
{

    private $table    = 'user';
    private $baseView = 'admin/user';
    private $urlIndex = 'usuario';

    public function index()
    {
        $model    = new User();
        $response = $model->allUsers($this->table, 'id DESC');
        Helper::view($this->baseView . '/index', $response);
    }

    public function viewNew()
    {
        $model             = new User();
        $response['lojas'] = $model->all('loja', 'nome DESC');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function viewEdit($param)
    {
        $model             = new User();
        $response          = $model->find($this->table, $param['id']);
        $response['lojas'] = $model->all('loja', 'nome DESC');
        Helper::view($this->baseView . '/edit', $response);
    }

    public function create()
    {
        $model = new User();
        $salt  = \AgendaLabs\Libs\Helper::salt();
        if ($_POST['senha']) {
            $_POST['senha'] = hash('sha512', $_POST['senha']);
            $_POST['salt']  = $salt;
        } else {
            unset($_POST['senha']);
        }

        $_POST['acesso'] = 'Empresa';
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }

        $id = $model->create($this->table, $_POST, ['id', 'image', 'permissoes']);
        if ($id) {
            $caminho     = 'files/user/';
            $nome_imagem = $id . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit', $_POST);
        }
    }

    public function update()
    {
        $model = new User();
        $salt  = \AgendaLabs\Libs\Helper::salt();
        if ($_POST['senha']):
            $_POST['senha'] = hash('sha512', $_POST['senha']);
            $_POST['salt']  = $salt;
        else:unset($_POST['senha']);
        endif;
        if (@$_SESSION['acesso'] == 'Empresa') {
            $_POST['id_loja'] = $_SESSION['id_loja'];
        }

        if ($model->save($this->table, $_POST, ['image', 'permissoes'])) {
            $id          = $_POST['id'];
            $caminho     = 'files/user/';
            $nome_imagem = $_POST['id'] . '_' . time();
            $formato     = 'jpg';
            if (Helper::upload($_FILES['imagem'], $nome_imagem, $caminho, $formato, 200, 200)) {
                $model->save($this->table, ['id' => $id, 'imagem' => $caminho . $nome_imagem . '.' . $formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView . '/edit/' . $_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = new User();
        $model->delete($this->table, 'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}