<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Cardapio;
use AgendaLabs\Libs\Helper;

class CardapioController
{

    private $table = 'cardapio';
    private $baseView = 'admin/cardapio';
    private $urlIndex = 'cardapio';

    public function index()
    {
        $model = New Cardapio();
        $response = $model->allCardapio();
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        $model = New Cardapio();
        $response['lojas'] = $model->all('loja','nome DESC');
        Helper::view($this->baseView.'/edit',$response);
    }

    public function viewEdit($param)
    {
        $model = New Cardapio();
        $response = $model->find($this->table,$param['id']);
        $response['lojas'] = $model->all('loja','nome DESC');
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Cardapio();
        $id = $model->create($this->table,$_POST,['id','image']);
        if($id) {
            $caminho = 'files/cardapio/';
            $nome_imagem = $id.'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,720,1280)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = New Cardapio();
        if($model->save($this->table,$_POST,['image'])) {
            $id = $_POST['id'];
            $caminho = 'files/cardapio/';
            $nome_imagem = $id.'_'.time();
            $formato = 'jpg';
            if(Helper::upload($_FILES['imagem'],$nome_imagem,$caminho,$formato,720,1280)) {
                $model->save($this->table,['id'=>$id,'imagem'=>$caminho.$nome_imagem.'.'.$formato]);
            }
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = New Cardapio();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
