<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Facilidade;
use AgendaLabs\Libs\Helper;

class FacilidadeController
{

    private $table = 'facilidade';
    private $baseView = 'admin/facilidade';
    private $urlIndex = 'facilidade';

    public function index()
    {
        $model = New Facilidade();
        $response = $model->all($this->table,'id DESC');
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        Helper::view($this->baseView.'/edit');
    }

    public function viewEdit($param)
    {
        $model = New Facilidade();
        $response = $model->find($this->table,$param['id']);
        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Facilidade();
        $id = $model->create($this->table,$_POST,['id','image']);
        if($id) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit',$_POST);
        }
    }

    public function update()
    {
        $model = New Facilidade();
        if($model->save($this->table,$_POST,['image'])) {
            header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
        } else {
            Helper::view($this->baseView.'/edit/'.$_POST['id']);
        }
    }

    public function delete($param)
    {
        $model = New Facilidade();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

}
