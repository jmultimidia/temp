<?php

namespace AgendaLabs\Controller;

use AgendaLabs\Model\Tipo;
use AgendaLabs\Libs\Helper;

class TipoController
{

    private $table = 'tipo';
    private $baseView = 'admin/tipo';
    private $urlIndex = 'tipo';

    public function index()
    {
        $model = New Tipo();
        $response = $model->allTipos();
        Helper::view($this->baseView.'/index',$response);
    }

    public function viewNew()
    {
        $model = New Tipo();
        Helper::view($this->baseView.'/edit',$response);
    }

    public function viewEdit($param)
    {
        $model = New Tipo();
        $response = $model->find($this->table,$param['id']);

        Helper::view($this->baseView.'/edit',$response);
    }

    public function create()
    {
        $model = New Tipo();
        $id = $model->create($this->table,$_POST);
	header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function update()
    {
        $model = New Tipo();
        $model->save($this->table,['id'=>$_POST['id']]);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }

    public function delete($param)
    {
        $model = New Tipo();
        $model->delete($this->table,'id', $param['id']);
        header('location: ' . URL_ADMIN . '/' . $this->urlIndex);
    }
}
