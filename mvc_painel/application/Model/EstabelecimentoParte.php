<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;

class EstabelecimentoParte extends Model
{
    public function partesPorTamanhoELoja($id)
    {
        $sql = "
        	SELECT t.*,  (
			SELECT partes FROM loja_partes lp
			WHERE lp.id_loja = :id
			AND lp.id_tamanho = t.id
			) as partes
			FROM tamanhos as t
        ";
        $query = $this->PDO()->prepare($sql);
        $query->bindParam(':id', $id);
        $query->execute();

        return $query->fetchAll();
    }

    public function partesEIndicesPorTamanho($id)
    {
        $partes = $this->partesPorTamanhoELoja($id);

        $tamanhos = [];
        foreach ($partes as $parte) {
            $tamanhos[$parte['id']] = $parte['partes'];
        }

        return $tamanhos;
    }
}
