<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class Pedido extends Model
{

    public function allPedidos($painel = false)
    {
        $where = '';
        if (@$_SESSION['acesso'] == 'Empresa' || (@$_POST['id_loja'] && @$_SESSION['acesso'] == 'Administrador')) {
            $where = " AND p.id_loja = '" . (@$_POST['id_loja'] && @$_SESSION['acesso'] == 'Administrador' ? $_POST['id_loja'] : $_SESSION['id_loja']) . "'";
        }

        if (@$_POST['data_inicio'] || @$_POST['data_fim']) {
            if ($_POST['data_inicio']) {
                $where .= " AND p.data_cadastro >= '" . Helper::data($_POST['data_inicio'], 1) . " 00:00:00'";
            }

            if ($_POST['data_fim']) {
                $where .= " AND p.data_cadastro <= '" . Helper::data($_POST['data_fim'], 1) . " 23:59:59'";
            }

        } else if (!$painel) {
            $date = date('Y-m-d');
            $where .= " AND p.data_cadastro BETWEEN '" . $date . " 00:00:00' AND '" . $date . " 23:59:59'";
        }

        if (@$_POST['situacao']) {
            $where .= " AND p.situacao = '" . $_POST['situacao'] . "'";
        }

        if (@$_POST['tipo']) {
            $where .= " AND p.tipo = '" . $_POST['tipo'] . "'";
        }

        if (@$_POST['tipo']) {
            $where .= " AND p.tipo = '" . $_POST['tipo'] . "'";
        }

        if (@$_POST['id_pagamento']) {
            $where .= " AND p.id_pagamento = '" . $_POST['id_pagamento'] . "'";
        }

        if ($painel) {
            $where .= " AND p.situacao IN ('Pendente','Producao','Saiu','Entregue')";
            $where .= " AND p.data_cadastro >= '" . date('Y-m-d H:i:s', strtotime(date('Y-m-d') . "-1 days")) . "'";
        }

        $sql = "
          SELECT p.*, l.nome loja, fp.formapagamento pagamento,
          GROUP_CONCAT(CONCAT(pp.qtde,'x ', pro.nome) SEPARATOR ', ') produtos
          FROM pedido p
          INNER JOIN loja l ON l.id = p.id_loja
          LEFT JOIN pedido_produto pp ON pp.id_pedido = p.id
          LEFT JOIN produto pro ON pro.id = pp.id_produto
          LEFT JOIN formapagamento fp ON fp.id = p.id_pagamento
          WHERE 1=1 $where
          GROUP BY p.id
          ORDER BY p.data_cadastro DESC
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function relPedidos($painel = false, $situacao = false, $tipo = false)
    {
        $where = '';
        $sql   = "
          SELECT p.*, l.nome loja, fp.formapagamento pagamento,
          GROUP_CONCAT(CONCAT(pp.qtde,'x ', pro.nome) SEPARATOR ', ') produtos
          FROM pedido p
          INNER JOIN loja l ON l.id = p.id_loja
          LEFT JOIN pedido_produto pp ON pp.id_pedido = p.id
          LEFT JOIN produto pro ON pro.id = pp.id_produto
          LEFT JOIN formapagamento fp ON fp.id = p.id_pagamento
          WHERE 1=1 $where
          GROUP BY p.id
          ORDER BY p.data_cadastro DESC
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allProdutos($id_pedido)
    {
        $sql = "
          SELECT pp.*, p.nome, p.imagem
          FROM pedido_produto pp
          INNER JOIN produto p ON p.id = pp.id_produto
          WHERE pp.id_pedido = '" . $id_pedido . "'
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function produtoPedido($id_pedido)
    {
        $sql = "
         SELECT pp.*, if (t.id, CONCAT(p.nome, ' - ', t.nome), p.nome) as nome
          FROM pedido_produto pp
          INNER JOIN produto p ON p.id = pp.id_produto
	  LEFT JOIN tamanhos t ON pp.id_tamanho = t.id
          WHERE id_pedido = '" . $id_pedido . "'
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function setShowNotice($orderId, $value)
    {
        $show = $value ? 1 : 0;
        $sql  = "
          UPDATE pedido SET mostrar_aviso = ${show}
          WHERE id = ${orderId}
        ";

        $query = $this->PDO()->prepare($sql);

        return $query->execute();
    }

    public function adiciObs($id_pedido, $id_produto)
    {
        $sql = "
          SELECT *
          FROM pedido_produto
          WHERE id_pedido = '" . $id_pedido . "'
          AND id_produto = '" . $id_produto . "'
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
