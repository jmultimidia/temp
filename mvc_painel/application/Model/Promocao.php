<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class Promocao extends Model
{

    public function allPromocao()
    {
        $where = '';
        if (@$_SESSION['acesso'] == 'Empresa') {
            $where = " AND c.id_loja = '" . $_SESSION['id_loja'] . "'";
        }
        $sql = "
          SELECT c.*, l.nome loja
          FROM promocao c 
          LEFT JOIN loja l ON l.id = c.id_loja
          WHERE 1=1 $where
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
