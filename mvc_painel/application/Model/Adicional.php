<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class Adicional extends Model
{

    public function allAdicionais()
    {
        $where = '';
        if(@$_SESSION['acesso'] == 'Empresa') {
            $where = " AND a.id_loja = '" . $_SESSION['id_loja'] . "'";
        }
        $sql = "
          SELECT a.*, l.nome loja 
          FROM adicional a 
          INNER JOIN loja l ON l.id = a.id_loja
          WHERE 1=1 $where
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
