<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;

class Tamanho extends Model
{
    public function allTamanhos()
    {
        $where = '';
        $sql   = "
          SELECT * FROM tamanhos
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
}
