<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;

class Auth extends Model
{

    public function login()
    {
        $PDO   = $this->PDO();
        $query = $PDO->prepare("
            SELECT u.nome, u.id, u.acesso, u.imagem, id_loja, salt
            FROM user u
            WHERE u.email = :login
            AND u.senha = :senha
            AND u.status = 1 LIMIT 1");
        $query->execute(
            [
                ':login' => $_POST['login'],
                ':senha' => hash('sha512', $_POST['senha']),
            ]
        );
        $result       = $query->fetch();
        $senha        = hash('sha512', $result['senha'] . $result['salt']);
        $senha_valida = hash('sha512', hash('sha512', $_POST['senha']) . $result['salt']);

        if ($senha_valida = $senha):
            return $result;
        else:
            return false;
        endif;
    }

    public function forgot($param = false)
    {
        if ($param['session']) {
            $where = "session = :session";
            $set   = [':session' => $param['session']];
        } else {
            $where = "email = :email";
            $set   = [':email' => $_POST['email']];
        }
        $PDO   = $this->PDO();
        $query = $PDO->prepare("SELECT id, email FROM user WHERE " . $where . " LIMIT 1");
        $query->execute($set);
        $result = $query->fetch();
        return $result;
    }

}
