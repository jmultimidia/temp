<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;

class Produto extends Model
{
    public function allProdutos()
    {
        $sql = "
          SELECT p.*, l.nome as loja, c.nome as categoria, sc.nome as subcategoria
          FROM produto p
          INNER JOIN loja l ON l.id = p.id_loja
          LEFT JOIN categoria c ON c.id = p.id_categoria
          LEFT JOIN subcategoria sc ON sc.id = p.id_subcategoria
        ";
        if (@$_SESSION['acesso'] === 'Empresa') {
            $sql .= " WHERE p.id_loja = " . $_SESSION['id_loja'];
        }

        $query = $this->PDO()->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function getSabores($sabores, $idTamanho = null)
    {
        // Garantir que não terá SQL Inject
        $items      = explode(",", $sabores);
        $newSabores = [];
        foreach ($items as $sabor) {
            $newSabores[] = (int) $sabor;
        }

        $parseSabores = implode(",", $newSabores);
        $sql          = "
        SELECT p.id, p.nome, p.descricao
        FROM produto p
        LEFT JOIN produto_valores as pv ON pv.id_produto = p.id
        LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
            WHERE p.id in (${parseSabores})";
        if ($idTamanho) {
            $sql .= " AND pv.id_tamanho = :idTamanho";
        }
        $query = $this->PDO()->prepare($sql);
        //$query->bindParam('sabores', $sabores);
        if ($idTamanho) {
            $query->bindParam('idTamanho', $idTamanho);
        }
        $query->execute();

        $result = $query->fetchAll();
        $nomes  = [];
        foreach ($result as $item) {

            $nomes[] = $item['nome'];
        }

        return implode(", ", $nomes);
    }

    public function getValueBySabores($sabores, $idTamanho = null)
    {
        // Garantir que não terá SQL Inject
        $items      = explode(",", $sabores);
        $newSabores = [];
        foreach ($items as $sabor) {
            $newSabores[] = (int) $sabor;
        }

        $parseSabores = implode(",", $newSabores);
        $sql          = "
        SELECT max(IF(pv.valor, pv.valor, p.preco)) as max, avg(IF(pv.valor, pv.valor, p.preco)) as avg
        FROM produto p
        LEFT JOIN produto_valores as pv ON pv.id_produto = p.id
        LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
        WHERE p.id IN (${parseSabores})";
        if ($idTamanho) {
            $sql .= " AND pv.id_tamanho = :idTamanho";
        }

        $query = $this->PDO()->prepare($sql);
        //$query->bindParam('sabores', $sabores);
        if ($idTamanho) {
            $query->bindParam('idTamanho', $idTamanho);
        }
        $query->execute();

        return $query->fetch();
    }

    public function getValue($idProduto, $idTamanho = null)
    {
        $sql = "
          SELECT p.*, IF(pv.valor, pv.valor, p.preco) as preco,
            if(t.id, concat(p.nome, ' - ', t.nome), p.nome) as nome,
    pv.id_tamanho
          FROM produto p
          LEFT JOIN produto_valores as pv ON pv.id_produto = p.id
          LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
        ";
        $sql .= " WHERE p.id = :idProduto";
        if ($idTamanho) {
            $sql .= " AND pv.id_tamanho = :idTamanho";
        }

        /*
        echo $sql;
        echo "<br>";
        echo $idProduto;
        echo "<br>";
        echo $idTamanho;
        exit;
        //*/

        $query = $this->PDO()->prepare($sql);
        $query->bindParam('idProduto', $idProduto);
        if ($idTamanho) {
            $query->bindParam('idTamanho', $idTamanho);
        }
        $query->execute();

        return $query->fetch();
    }

    public function checkExistsById($id)
    {
        $sql = "
          SELECT id
          FROM produto
            WHERE id = :id
        ";
        if (@$_SESSION['acesso'] === 'Empresa') {
            $sql .= " AND id_loja = " . $_SESSION['id_loja'];
        }
        $sql .= " LIMIT 1";
        $query = $this->PDO()->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();

        return $query->rowCount();
    }

    /* public function getLoja()
    {
    if (@$_SESSION['acesso'] === 'Empresa') {
    $sql = "SELECT id, loja
    FROM loja WHERE id = " . $_SESSION['id_loja'];
    }
    $query = $this->PDO()->prepare($sql);
    $query->execute();
    return $query->fetch();
    } */

    public function allSimilarProducts($idLoja, $idSubcategoria = null, $tipo = null, $idTamanho = null)
    {
        $sql = "
            SELECT
                p.id,
                p.nome,
                p.descricao,
                IF(pv.valor, pv.valor, p.preco) as preco,
                pv.id_tamanho
            FROM
                produto as p
            LEFT JOIN produto_valores as pv ON p.id = pv.id_produto
            LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
            WHERE p.id_loja = :idLoja
                AND p.parte_varios_sabores = 1
                AND p.status = 1
                AND p.id_subcategoria = :idSubcategoria
                AND pv.id_tamanho = :idTamanho
                AND p.tipo = :tipo
            ";

        $query = $this->PDO()->prepare($sql);
        $query->bindParam(':idLoja', $idLoja);
        $query->bindParam(':idSubcategoria', $idSubcategoria);
        $query->bindParam(':idTamanho', $idTamanho);
        $query->bindParam(':tipo', $tipo);

        $query->execute();

        return $query->fetchAll();
    }
}
