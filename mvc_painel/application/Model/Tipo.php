<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class Tipo extends Model
{

    public function allTipos()
    {
        $where = '';
        $sql = "
          SELECT * FROM tipos
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}