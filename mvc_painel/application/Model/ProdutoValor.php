<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class ProdutoValor extends Model
{
    public function byProduto($id)
    {
        $sql = "
          SELECT pv.*, t.nome, t.abre
          FROM produto_valores as pv
            INNER JOIN tamanhos as t ON pv.id_tamanho = t.id
            WHERE id_produto = :id
        ";
        $query = $this->PDO()->prepare($sql);
	    $query->bindParam(':id', $id);
        $query->execute();

	return $query->fetchAll();
    }
}

