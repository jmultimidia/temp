<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class User extends Model
{

    public function allUsers()
    {
        $where = '';
        if(@$_SESSION['acesso'] == 'Empresa') {
            $where = " AND u.id_loja = '" . $_SESSION['id_loja'] . "'";
        }
        $sql = "
          SELECT u.*, l.nome loja
          FROM user u 
          LEFT JOIN loja l ON l.id = u.id_loja
          WHERE 1=1 $where
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
