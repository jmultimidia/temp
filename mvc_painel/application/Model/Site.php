<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;

class Site extends Model
{
    public function findCliente($id_celular)
    {
        $sql = "
            SELECT *
            FROM cliente c
            WHERE c.id_celular = '" . $id_celular . "'
            ORDER BY id_cidade DESC
            LIMIT 1";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function allLojas($id_segmento, $diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND ls.id_segmento = '" . $id_segmento . "'
            GROUP BY l.id
            ORDER BY l.destaque DESC, l.ordem ASC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allDeliverys($diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND delivery='1'
            GROUP BY l.id
            ORDER BY l.destaque DESC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function Lojas($diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND type='1'
            GROUP BY l.id
            ORDER BY l.destaque DESC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function Acidade($diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto, lh.hora_inicio, lh.hora_fim
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND type='3'
            GROUP BY l.id
            ORDER BY l.destaque DESC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function Emergencia($diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND type='4'
            GROUP BY l.id
            ORDER BY l.destaque DESC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function Servico($diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND type='2'
            GROUP BY l.id
            ORDER BY l.destaque DESC, RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allAdicionais($id_produto)
    {
        $sql = "
            SELECT a.*
            FROM adicional_produto ap
            INNER JOIN adicional a ON a.id = ap.id_adicional
            WHERE ap.id_produto = '" . $id_produto . "'
            ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function filtrar($diaDaSemana)
    {
        $where = '';
        if ($_SESSION['filtros']['id_segmento']) {
            $where .= " AND ls.id_segmento = '" . $_SESSION['filtros']['id_segmento'] . "'";
        }
        if ($_SESSION['filtros']['bairro']) {
            $where .= " AND l.bairro = '" . $_SESSION['filtros']['bairro'] . "'";
        }
        if ($_SESSION['filtros']['id_facilidade']) {
            $where .= " AND lf.id_facilidade = '" . $_SESSION['filtros']['id_facilidade'] . "'";
        }
        if ($_SESSION['filtros']['id_formapagamento']) {
            $where .= " AND lfp.id_formapagamento = '" . $_SESSION['filtros']['id_formapagamento'] . "'";
        }
        if ($_SESSION['filtros']['pedidoonline']) {
            $where .= " AND l.online = 1";
        }
        if ($_SESSION['filtros']['delivery']) {
            $where .= " AND l.delivery = 1";
        }

        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN loja_segmento ls ON ls.id_loja = l.id
            LEFT JOIN loja_facilidade lf ON lf.id_loja = l.id
            LEFT JOIN loja_formapagamento lfp ON lfp.id_loja = l.id
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '06:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            $where
            GROUP BY l.id
            ORDER BY RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allPesquisa($diaDaSemana)
    {
        $busca_div       = explode(" ", $_POST['palavra-chave']); // SEPARA AS PALABRAS
        $resultado       = count($busca_div); // CONTA A QUANTIDADE DE PALAVRAS
        $busca_descricao = " l.descricao LIKE '%"; //INÍCIO DO LIKE
        $busca_nome      = " l.nome LIKE '%";
        $final           = "%' "; // FINAL DO LIKE

        for ($i = 0; $i < $resultado; $i++):
            // SE A PALAVRA FOR A PRIMEIRA INSERE OR ANTES
            if ($i != $resultado - 1):
                $select = "or" . "$busca_descricao" . "$busca_div[$i]" . "$final" . "or" . "$busca_nome" . "$busca_div[$i]" . "$final" . $select;
            else:
                $select = "(" . "$busca_descricao" . "$busca_div[$i]" . "$final" . "or" . "$busca_nome" . "$busca_div[$i]" . "$final" . $select . ")";
            endif;
        endfor;

        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND " . $select . "
            GROUP BY l.id
            ORDER BY RAND()";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allFavoritos($id_celular, $diaDaSemana)
    {
        $sql = "
            SELECT l.*, lh.id aberto
            FROM loja l
            INNER JOIN cliente_favoritos cf ON cf.id_celular = '" . $id_celular . "'
            LEFT JOIN loja_horario lh ON lh.id_loja = l.id AND lh.dia = '" . $diaDaSemana . "' AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND cf.id_loja = l.id
            GROUP BY l.id
            ORDER BY RAND()";

        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }   

    public function aberto($id_loja, $diaDaSemana)
    {
        $sql = "
            SELECT *
            FROM loja_horario lh
            WHERE lh.id_loja = '" . $id_loja . "'
            AND lh.dia = '" . $diaDaSemana . "'
            AND CURTIME() BETWEEN lh.hora_inicio AND IF(lh.hora_fim < '08:00:00', '23:59:00', lh.hora_fim)
            ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function allPromocoes()
    {
        $sql = "
            SELECT p.*, l.nome loja
            FROM promocao p
            INNER JOIN loja l ON l.id = p.id_loja
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN p.agendamento AND p.validade
            AND p.status='1'
            ORDER BY l.destaque DESC, p.data_inicio ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function slidePromocoes()
    {
        $sql = "
            SELECT p.*, l.nome loja
            FROM promocao p
            INNER JOIN loja l ON l.id = p.id_loja
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN p.agendamento AND p.validade
            AND p.destaque='1'
            AND p.status='1'
            ORDER BY l.destaque DESC, p.data_inicio ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function slideDestaques()
    {
        $sql = "
            SELECT d.*, l.nome loja
            FROM destaque d
            INNER JOIN loja l ON l.id = d.id_loja
            WHERE l.id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN d.agendamento AND d.validade
            AND d.destaque='1'
            AND d.status='1'
            ORDER BY l.destaque DESC, d.ordem ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function slideNoticias()
    {
        $sql = "
        SELECT id, id_cidade, titulo, resumo, agendamento, validade, imagem, created_at
        FROM noticia
        WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
        AND CURDATE() BETWEEN agendamento AND validade
        AND status='1'
        AND destaque='1'
        ORDER BY ordem ASC, agendamento ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function slideRadios()
    {
        $sql = "
        SELECT id, id_cidade, nome, imagem_capa
        FROM radio
        WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
        AND status='1'
        AND destaque='1'
        ORDER BY ordem ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allDestaques()
    {
        $sql = "
            SELECT d.*, l.nome loja
            FROM destaque d
            INNER JOIN loja l ON l.id = d.id_loja
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN d.agendamento AND d.validade
            AND d.status='1'
            ORDER BY l.destaque DESC, d.ordem ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allEventos()
    {
        $sql = "
            SELECT *
            FROM evento
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN agendamento AND validade
            AND status='1'
            ORDER BY ordem ASC, data_inicio ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allNoticias()
    {
        $sql = "
            SELECT *
            FROM noticia
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN agendamento AND validade
            AND status='1'
            ORDER BY ordem ASC, created_at DESC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allRadios()
    {
        $sql = "
            SELECT *
            FROM radio
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND status='1'
            ORDER BY ordem ASC, created_at DESC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function slideEventos()
    {
        $sql = "
            SELECT *
            FROM evento
            WHERE id_cidade = '" . $_SESSION['id_cidade'] . "'
            AND CURDATE() BETWEEN agendamento AND validade
            AND destaque='1'
            AND status='1'
            ORDER BY ordem ASC, data_inicio ASC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allProdutos($idLoja)
    {
        $sql = "
            SELECT t.id as id_tamanho, t.nome as tamanho, t.abre as tamanho_abre, IF(pv.valor, pv.valor, p.preco) as valor,
                p.*, c.ordem, c.nome categoria, if(t.id, CONCAT(p.nome, ' - ', t.nome), p.nome) as nome
		FROM produto p
            INNER JOIN categoria c ON c.id = p.id_categoria
            LEFT JOIN produto_valores as pv ON p.id = pv.id_produto
            LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
            WHERE p.id_loja = :idLoja
            ORDER BY c.ordem ASC, c.nome ASC, p.nome ASC, p.id ASC, t.nome";
        $query = $this->PDO()->prepare($sql);
        $query->bindParam(':idLoja', $idLoja);
        $query->execute();
        return $query->fetchAll();
    }

    public function allSabores($idLoja, $idProduto)
    {
        $sql = "
            SELECT t.id as id_tamanho, t.nome as tamanho, t.abre as tamanho_abre, IF(pv.valor, pv.valor, p.preco) as valor,
                p.*, c.nome categoria
		FROM produto p
            INNER JOIN categoria c ON c.id = p.id_categoria
            LEFT JOIN produto_valores as pv ON p.id = pv.id_produto
            LEFT JOIN tamanhos as t ON pv.id_tamanho = t.id
            WHERE p.id_loja = :idLoja
            AND p.id_loja = :idLoja
            ORDER BY c.nome ASC, p.nome ASC, p.id ASC, t.nome";
        $query = $this->PDO()->prepare($sql);
        $query->bindParam(':idLoja', $idLoja);
        $query->bindParam(':idProduto', $idProduto);
        $query->execute();
        return $query->fetchAll();
    }

    public function allFormaPagamento($id_loja)
    {
        $sql = "
            SELECT f.*
            FROM loja_formapagamento lf
            INNER JOIN formapagamento f ON f.id = lf.id_formapagamento
            WHERE lf.id_loja = '" . $id_loja . "'
            ORDER BY f.formapagamento";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function allFacilidade($id_loja)
    {
        $sql = "
            SELECT f.*
            FROM loja_facilidade lf
            INNER JOIN facilidade f ON f.id = lf.id_facilidade
            WHERE lf.id_loja = '" . $id_loja . "'
            ORDER BY f.facilidade";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function ultimoPedido($id_user)
    {
        $sql = "
            SELECT p.*
            FROM pedido p
            WHERE p.id_user = '" . $id_user . "'
            AND p.situacao <> 'Incompleto'
            ORDER BY p.id DESC
            LIMIT 1";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function pedidosUser($id_user)
    {
        $sql = "
            SELECT p.*, l.nome loja, fp.formapagamento pagamento
            FROM pedido p
            INNER JOIN loja l ON l.id = p.id_loja
            LEFT JOIN pedido_produto pp ON pp.id_pedido = p.id
            LEFT JOIN produto pro ON pro.id = pp.id_produto
            LEFT JOIN formapagamento fp ON fp.id = p.id_pagamento
            WHERE p.id_user = '" . $id_user . "'
            AND p.situacao <> 'Incompleto'
            GROUP BY p.id
            ORDER BY p.id DESC";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function produtoPedido($id_pedido)
    {
        $sql = "
	 SELECT pp.*, if (t.id, CONCAT(p.nome, ' - ', t.nome), p.nome) as nome
          FROM pedido_produto pp
          INNER JOIN produto p ON p.id = pp.id_produto
	  LEFT JOIN tamanhos t ON pp.id_tamanho = t.id
          WHERE id_pedido = '" . $id_pedido . "'
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function adcObs($id_pedido)
    {
        $sql = "
          SELECT *
          FROM pedido_produto
          WHERE id_pedido = '" . $id_pedido . "'
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
}
