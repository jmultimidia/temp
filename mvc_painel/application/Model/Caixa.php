<?php

namespace AgendaLabs\Model;

use AgendaLabs\Core\Model;
use AgendaLabs\Libs\Helper;

class Caixa extends Model
{
    public function allCaixa()
    {
        $sql = "
            SELECT c.*, uC.nome caixa, uA.nome responsavel_abertura, uF.nome responsavel_fechamento
            FROM caixa c
            LEFT JOIN user uC ON uC.id = c.id_user
            LEFT JOIN user uA ON uA.id = c.id_user_abertura
            LEFT JOIN user uF ON uF.id = c.id_user_fechamento
            WHERE c.id_loja = '" . $_SESSION['id_loja'] . "'
            ORDER BY c.id DESC
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function caixaAberto($id = false)
    {
        $where = "";
        if($id) $where = " AND c.id = '" . $id . "' LIMIT 1";
        $sql = "
            SELECT c.*, uC.nome caixa, uA.nome responsavel_abertura, uF.nome responsavel_fechamento
            FROM caixa c
            LEFT JOIN user uC ON uC.id = c.id_user
            LEFT JOIN user uA ON uA.id = c.id_user_abertura
            LEFT JOIN user uF ON uF.id = c.id_user_fechamento
            WHERE c.data_abertura < NOW() 
                AND c.data_fechamento IS NULL 
                AND c.id_loja = '" . $_SESSION['id_loja'] . "'
            $where
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function valoresFechamento($date,$id_ser)
    {
        $sql = "
            SELECT 
                SUM(valor_pedido) pedidos, 
                SUM(valor_desconto) descontos, 
                SUM(valor_total) total 
            FROM pedido 
            WHERE id_loja = '" . $_SESSION['id_loja'] . "' 
                AND id_user = '" . $id_ser . "'  
                AND status = 1
                AND data_cadastro BETWEEN '" . $date . "' AND NOW()
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetch();
    }

    public function extrato($dataAbertura,$dataFechamento,$id_ser)
    {
        $sql = "
            SELECT 
                pagamento, 
                SUM(valor_total)total 
            FROM pedido 
            WHERE id_loja = '" . $_SESSION['id_loja'] . "' 
                AND id_user = '" . $id_ser . "'
                AND status = 1 
                AND data_cadastro BETWEEN '" . $dataAbertura . "' AND '" . $dataFechamento . "' 
            GROUP BY pagamento 
            ORDER BY pagamento
        ";
        $query = $this->PDO()->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

}
