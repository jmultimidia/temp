<?php
namespace AgendaLabs\Libs;

use AgendaLabs\Libs\Upload;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class Helper
{

    public static function splitUrl()
    {
        if (!isset($_GET['url'])) {
            $_GET['url'] = $_SERVER['REQUEST_URI'];
        }
        if (isset($_GET['url'])) {
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }

    public static function view($view, $response = [])
    {
        if (!$view) {
            $view = 'error/index';
        }
        require APP . 'view/' . $view . '.php';
    }

    public static function json($json)
    {
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($json);
    }

    public static function ajax($nomecontroller, $action, $param)
    {
        $getController = '\AgendaLabs\Controller\\' . $nomecontroller . 'Controller';
        $controller    = new $getController();
        $controller->{$action}($param);
    }

    public static function upload($arquivo, $nome_arquivo, $caminho, $formato = false, $largura = false, $altura = false, $ratio = true)
    {
        $foo = new Upload($arquivo);
        if ($foo->uploaded) {
            $foo->file_overwrite     = true;
            $foo->file_new_name_body = $nome_arquivo;
            if ($largura) {
                $foo->image_convert = $formato;
            }
            if ($largura) {
                $foo->image_resize = true;
                $foo->image_ratio  = $ratio;
                $foo->image_x      = $largura;
                $foo->image_y      = $altura;
            }
            if (!$largura):
                $foo->image_resize = true;
                $foo->image_ratio  = $ratio;
                $foo->image_y      = $altura;
            endif;
            $foo->Process($caminho);
            if ($foo->processed) {
                $foo->Clean();
                return true;
            } else {
//                return $foo->error;
                //                echo $foo->error;
                //                exit();
                return false;
            }
        } else {
//            return $foo->error;
            //            echo $foo->error;
            //            exit();
            return false;
        }
    }

    public static function rearrange($arr)
    {
        foreach ($arr as $key => $all) {
            foreach ($all as $i => $val) {
                $new[$i][$key] = $val;
            }
        }
        return $new;
    }

    public static function iconFile($file)
    {
        $file = explode('.', $file);
        $ext  = end($file);
        switch ($ext) {
            case 'doc':
            case 'docx':
                $icon = 'fa fa-file-word-o';
                break;
            case 'xls':
            case 'xlsx':
            case 'csv':
                $icon = 'fa fa-file-excel-o';
                break;
            case 'ppt':
            case 'pptx':
                $icon = 'fa fa-file-powerpoint-o';
                break;
            case 'pdf':
                $icon = 'fa fa-file-pdf-o';
                break;
            case 'psd':
            case 'cdr':
            case 'ai':
            case 'bmp':
            case 'gif':
            case 'jpeg':
            case 'jpg':
            case 'png':
                $icon = 'fa fa-file-image-o';
                break;
            case 'zip':
            case 'rar':
            case '7z':
                $icon = 'fa fa-file-archive-o';
                break;
            case 'mp3':
            case 'wma':
            case 'aac':
            case 'ogg':
            case 'ac3':
            case 'wav':
                $icon = 'fa fa-file-audio-o';
                break;
            case 'mpeg':
            case 'mov':
            case 'avi':
            case 'rmvb':
            case 'mkv':
            case 'mxf':
            case 'pr':
                $icon = 'fa fa-file-movie-o';
                break;
            case 'txt':
                $icon = 'fa fa-file-text-o';
                break;
            case 'php':
            case 'html':
            case 'css':
            case 'js':
                $icon = 'fa fa-file-code-o';
                break;
            default:
                $icon = 'fa fa-file-o';
                break;
        }
        return $icon;
    }

    public static function dataHora($data, $gravar = false)
    {
        if ($gravar) {
            $data = str_replace('/', '-', $data);
            $data = date('Y-m-d H:i:s', strtotime($data));
        } else {
            $data = date('d/m/Y H:i', strtotime($data));
        }

        return $data;
    }

    public static function data($data, $gravar = false)
    {
        if ($data) {
            if ($gravar) {
                $data = str_replace('/', '-', $data);
                $data = date('Y-m-d', strtotime($data));
            } else {
                $data = date('d/m/Y', strtotime($data));
            }
        }
        return $data;
    }

    public static function hora($hora)
    {
        $hora = substr($hora, 0, -3);
        return $hora;
    }

    public static function timeago($date)
    {
        $timestamp = strtotime($date);

        $strTime  = array("segundo", "minuto", "hora", "dia", "mês", "ano");
        $strTimes = array("segundos", "minutos", "horas", "dias", "meses", "anos");
        $length   = array("60", "60", "24", "30", "12", "10");

        $currentTime = time();
        if ($currentTime >= $timestamp) {
            $diff = time() - $timestamp;
            for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
                $diff = $diff / $length[$i];
            }

            $diff = round($diff);
            if ($diff == 1) {
                return $diff . " " . $strTime[$i] . " atrás ";
            } else {
                return $diff . " " . $strTimes[$i] . " atrás ";
            }

        }
    }
    
    public static function isNew($date)
    {
        $timestamp = strtotime($date. ' + 7 days');

        $currentTime = time();
        if ($currentTime < $timestamp) {
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }
    
    public static function valor($valor, $gravar = false)
    {
        if ($gravar) {
            $valor = str_replace(',', '.', str_replace(['.', 'R$', ' '], '', ($valor ?: 0)));
        } else {
            $valor = number_format(($valor ?: 0), 2, ',', '.');
        }
        return $valor;
    }

    public static function cleanToUrl($valor)
    {
        return mb_strtolower(str_replace(" ", "+", preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(trim($valor)))));
    }

    public static function trataMail($param)
    {
        $email = 'contato@npop.com.br';
        switch ($param['tipo']) {
            case 'contato':
                $assunto  = 'APP - Fale com a gente';
                $mensagem = "Nome: " . $param['nome'] . "<br>Telefone: " . $param['telefone'] . "<br>Email: " . $param['email'] . "<br>Mensagem: " . $param['mensagem'];
                self::mail($assunto, $mensagem, [$email]);
                break;
            case 'anuncie':
                $assunto  = 'APP - Anuncie na lista';
                $mensagem = "Nome: " . $param['nome'] . "<br>Empresa: " . $param['empresa'] . "<br>Telefone: " . $param['telefone'] . "<br>Email: " . $param['email'] . "<br>Mensagem: " . $param['mensagem'];
                self::mail($assunto, $mensagem, [$email]);
                break;
        }
    }

    public static function mail($assunto, $msg, $email = [], $cc = false, $anexo = false)
    {
        $body = '<div style="width:600px; padding-top: 10px; text-align: center; border: 3px solid #ec891c; background-color: #ec891c">';
        $body .= '<img src="' . URL_PUBLIC . '/assets/img/logo-top.png">';
        $body .= '</div>';
        $body .= '<div style="width:540px; padding: 50px 30px; font-size: 14px; border: 3px solid #ec891c;">';
        $body .= $msg;
        $body .= '</div>';

        $mail = new PHPMailer(); // Passing `true` enables exceptions
        try {
            //Server settings
            //            $mail->SMTPDebug = 3;                       // Enable verbose debug output
            $mail->setLanguage = 'en';
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host        = MAIL_HOST; // Specify main and backup SMTP servers
            $mail->SMTPAuth    = MAIL_AUTH; // Enable SMTP authentication
            $mail->Username    = MAIL_USER; // SMTP username
            $mail->Password    = MAIL_PASS; // SMTP password
            $mail->Port        = MAIL_PORT; // TCP port to connect to
            $mail->SMTPSecure  = MAIL_SECURE; // Enable TLS encryption, `ssl` also accepted
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                    'allow_self_signed' => true,
                ),
            );
//            $mail->SMTPAutoTLS = true; // remover quando subir no servidor

            $mail->IsHTML(true); // Set email format to HTML
            $mail->CharSet = 'UTF-8';

            //Recipients
            $mail->setFrom($mail->Username, APP_TITLE);
//            $mail->From = MAIL_USER;
            $mail->addReplyTo($mail->Username, APP_TITLE);
            foreach ($email as $item) {
                $mail->addAddress($item); // Add a recipient
            }
            if ($cc) {
                $mail->addCC($cc);
            }

            //Attachments
            if ($anexo) {
                $mail->addAttachment($anexo);
            }
            // Optional name use ',newName.ext'

            //Content
            $mail->Subject = $assunto;
            $mail->Body    = $body;
//            $mail->AltBody = 'This mail contain html code.';

            $mail->Send();

            return true;
//            echo 'Message has been sent';
        } catch (Exception $e) {
            return false;
//            echo 'Message could not be sent.';
            //            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }
    }
    public static function limitarTexto($texto, $limite, $quebrar = true)
    {
        //corta as tags do texto para evitar corte errado
        $contador = strlen(strip_tags($texto));
        if ($contador <= $limite):
            //se o número do texto form menor ou igual o limite então retorna ele mesmo
            $newtext = $texto;
        else:
            if ($quebrar == true): //se for maior e $quebrar for true
                //corta o texto no limite indicado e retira o ultimo espaço branco
                $newtext = trim(mb_substr($texto, 0, $limite)) . "...";
            else:
                //localiza ultimo espaço antes de $limite
                $ultimo_espaço = strrpos(mb_substr($texto, 0, $limite), " ");
                //corta o $texto até a posição lozalizada
                $newtext = trim(mb_substr($texto, 0, $ultimo_espaço)) . "...";
            endif;
        endif;
        return $newtext;
    }
    public static function salt()
    {
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        return $random_salt;
    }
    public static function urlHttps($valor)
    {

        $result = str_replace("http://", "https://", $valor);

        return $result;
    }
    public static function url_exists($arquivo)
    {

        $ch = curl_init($arquivo);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ($code == 200); // verifica se recebe "status OK"
    }
    public static function deleteDir($path) {
        if(!empty($path) && is_dir($path) ){
            $dir  = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS); //upper dirs are not included,otherwise DISASTER HAPPENS :)
            $files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ($files as $f) {if (is_file($f)) {unlink($f);} else {$empty_dirs[] = $f;} } if (!empty($empty_dirs)) {foreach ($empty_dirs as $eachDir) {rmdir($eachDir);}} rmdir($path);
        }
    }

}
