<?php
use AgendaLabs\Libs\Helper;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="author" content="JMultimidia.com.br" />
    <base href="<?=URL_PUBLIC?>/" target="_self">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <title><?=APP_TITLE . $title?></title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?=URL_PUBLIC?>/assets/app/css/colors/yellow<?=MINIFY?>.css?20203281834">
    <link rel="stylesheet" type="text/css" href="<?=URL_PUBLIC?>/assets/app/fonts/css/all.min.css">
    <link type="text/css" rel="stylesheet" href="<?=URL_PUBLIC?>/assets/app/css/jmultimidia<?=MINIFY?>.css?202003281549" />
    <link type="text/css" rel="stylesheet" href="<?=URL_PUBLIC?>/assets/site/css/animate.min.css" />
    <?php
foreach ($css as $file):
    if (Helper::url_exists($file)):
        echo '<link href="' . $file . '" rel="stylesheet">';
    endif;
endforeach;
?>
</head>