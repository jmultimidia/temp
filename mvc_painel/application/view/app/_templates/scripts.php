<?php

use AgendaLabs\Libs\Helper;
?>
<script type="text/javascript" src="<?=URL_PUBLIC?>/assets/app/scripts/jquery<?=MINIFY?>.js"></script>
<script type="text/javascript" src="<?=URL_PUBLIC?>/assets/app/scripts/custom<?=MINIFY?>.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=URL_PUBLIC?>/assets/app/scripts/lazysizes.min.js"></script>
<script src="<?=URL_PUBLIC?>/assets/app/scripts/wow.min.js"></script>
<script>
wow = new WOW({
    animateClass: 'animated',
    offset: 100,
    mobile: true, // default
    live: true,
    callback: function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
    }
});
wow.init();
</script>
<?php
foreach ($script as $file):
    if (Helper::url_exists($file)):
        echo '<script src="' . $file . '"></script>';
    endif;
endforeach;
if ($page && $page == "home"): ?>
<script id="rendered-js">
    // external js: flickity.pkgd.js
    var $carousel = $('.carousel-estabelecimentos').flickity({
        imagesLoaded: true,
        percentPosition: false,
        freeScroll: true,
        contain: true,
        // disable previous & next buttons and dots
        prevNextButtons: false,
        pageDots: false,
        autoPlay: 7000,
        cellAlign: 'left'
    });
    var $carousel = $('.carousel-ofertas').flickity({
        imagesLoaded: true,
        percentPosition: false,
        freeScroll: true,
        contain: true,
        // disable previous & next buttons and dots
        prevNextButtons: false,
        pageDots: false,
        autoPlay: 6000,
        cellAlign: 'left'
    });
    var $carousel = $('.carousel-radios').flickity({
        imagesLoaded: true,
        percentPosition: false,
        freeScroll: true,
        contain: true,
        // disable previous & next buttons and dots
        prevNextButtons: false,
        pageDots: false,
        autoPlay: 7000,
        cellAlign: 'left'
    });
    var $carousel = $('.carousel-eventos').flickity({
        imagesLoaded: true,
        percentPosition: false,
        freeScroll: true,
        contain: true,
        // disable previous & next buttons and dots
        prevNextButtons: false,
        pageDots: false,
        autoPlay: 5000,
        cellAlign: 'left'
    });

    var $imgs = $carousel.find('.carousel-cell img');
    // get transform property
    var docStyle = document.documentElement.style;
    var transformProp = typeof docStyle.transform == 'string' ?
        'transform' : 'WebkitTransform';
    // get Flickity instance
    var flkty = $carousel.data('flickity');

    $carousel.on('scroll.flickity', function() {
        flkty.slides.forEach(function(slide, i) {
            var img = $imgs[i];
            var x = (slide.target + flkty.x) * -1 / 3;
            img.style[transformProp] = 'translateX(' + x + 'px)';
        });
    });
    </script>
    <script id="rendered-js">
    $(document).ready(function() {
        var owl = $("#own-delivery");
        owl.owlCarousel({
            itemsCustom: [
                [0, 4.5],
                [450, 4.5],
                [600, 6],
                [700, 8],
                [1000, 10],
                [1200, 10],
                [1400, 19],
                [1600, 20]
            ],
            pagination: false
        });
    });
    </script>
<?php endif;?>
<script>
function abrirMapa(latitude, longitude, nome) {
        var android_link = "geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude + "(" + nome + ")";
        var ios_link = "http://maps.apple.com/?q=" + latitude + "," + longitude + "&t=s";

        if ((navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPad") != -1) ||
            (navigator.platform.indexOf("iPod") != -1) ||
            (navigator.platform.indexOf("MacIntel") != -1)) {
            window.open(ios_link);
        } else
            window.open(android_link);
    }

    function abrirWhats(celular, texto) {
        var android_link = "whatsapp://send?phone=55" + celular + "&text=" + texto;
        var ios_link = "https://wa.me/55" + celular + "?text=" + texto;

        if ((navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPad") != -1) ||
            (navigator.platform.indexOf("iPod") != -1) ||
            (navigator.platform.indexOf("MacIntel") != -1))
            window.open(ios_link);
        else
            window.open(android_link);
    }
    //    $('body').css('background-color','#DBB183')
</script>
<script type="text/javascript">
$(document).ready(function () {
// if the user clicks on the like button ...
$('.favorite-btn').on('click', function (e) {
    var id_loja = $(this).data('id');
    $clicked_btn = $(this);
    if ($clicked_btn.hasClass('bg-night-dark')) {
        action = 'like';
    } else if ($clicked_btn.hasClass('bg-red-dark')) {
        action = 'unlike';
    }
    var param = {};
            param['action'] = action;
            param['id_loja'] = id_loja;
    $.ajax({
        url: '<?=URL_PUBLIC?>/ajax',
            method: "post",
            dataType: "text",
            data: { controller: "Site", action: "favoritar", param: param },
            success: function (data) {
            if (action == "like") {
                $clicked_btn.removeClass('bg-night-dark');
                $clicked_btn.addClass('bg-red-dark'); 
                <?php if($page == "estabelecimento"):?>
                var txtFav = $(".favTxt").html();
                txtFav = txtFav.replace("Favoritar","Remover favorito");                
                $(".favTxt").html(txtFav);
                <?php endif;?>
                } else if (action == "unlike") {
                $clicked_btn.removeClass('bg-red-dark');
                $clicked_btn.addClass('bg-night-dark');   
                <?php if($page == "estabelecimento"):?>             
                var txtFav = $(".favTxt").html();
                txtFav = txtFav.replace("Remover favorito", "Favoritar");
                $(".favTxt").html(txtFav);
                <?php endif;
                if ($page == "favoritos"):
                    ?>
                    location.reload();
                    //window.location.reload();                    
                    <?php endif;
                ?>
                }
            },
        });
    });

});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-60539429-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-60539429-2');
</script>
