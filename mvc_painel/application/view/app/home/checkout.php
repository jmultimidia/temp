<?php

use AgendaLabs\Libs\Helper;

$title = 'Checkout';
$css   = [  
  URL_PUBLIC.'/assets/app/css/checkout'.MINIFY.'.css?201912081205', 
];
$script = [   
    URL_PUBLIC.'/assets/admin/js/plugins/parsley/parsley.min.js',
    URL_PUBLIC.'/assets/admin/js/plugins/parsley/i18n/pt-br.js', 
    URL_PUBLIC.'/assets/admin/js/plugins/maskedinput/jquery.maskedinput.min.js', 
    URL_PUBLIC.'/assets/app/scripts/jquery.inputmask.money.js', 
];
$page = "checkout";
require APP . 'view/app/_templates/initFile.php';
?>
<body class="theme-light" data-highlight="blue2">
<div id="page-transitions">
<!--header-->
<div class="page-hider"></div>

<!--End header-->
    <!--Page Content-->
    <section class="ui-content animated fadeIn faster">
<div class="page-content pages_maincontent header-clear-large">
<?php
include APP . 'view/app/home/modules/search.php';
?>
<div class="heading-style pb-0 mb-0">
                <h2 class="heading-title">Confirme seu pedido</h2>
                <em class="opacity-60">Antes de finalizar confira todos os itens</em>
                <div class="mt-4">
                    <i class="fas fa-clipboard-check font-30 color-blue-dark"></i>
                </div>      
</div>
<div class="container">
<div class="aviso-add mb-3">
                Clique em <i class="fas fa-plus-circle"></i> para acrescentar opções ou retirar ingredientes.
                </div>
                </div>
                <form method="post" action="<?=URL_PUBLIC?>/naodeviaenviar" data-parsley-focus="none">
<div class="container p-0 bg-light">
    <div class="accordion p-0" id="accordionExample">
        <?php
$total = 0;
$i     = 0;
foreach ($response['produtos'] as $produto) {
if (!$produto['varios_sabores']) {
$total += $produto['preco'] * $produto['qtde'];
}
if ($produto['adicionais'] || $produto['varios_sabores']) {
for ($n = 1; $n <= $produto['qtde']; $n++) {
echo '<input type="hidden"" name="produtos[' . $i . '][id_produto]" value="' . $produto['id'] . '">';
echo '<input type="hidden"" name="produtos[' . $i . '][id_tamanho]" value="' . $produto['id_tamanho'] . '">';
echo '<input type="hidden"" name="produtos[' . $i . '][qtde]" value="1">';?>
<script>
    $( document ).ready(function() {
        var $div<?=$i?> = $("#collapse<?=$i?>");
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.attributeName === "class") {
                    var attributeValue = $(mutation.target).prop(mutation.attributeName);
                    //console.log("Class attribute changed to:", attributeValue);
                buttonToggle(<?=$i?>);
                    }
                });
        });
            observer.observe($div<?=$i?>[0], {
            attributes: true
        });
    });
</script>
        <div class="card appVue">
            <div class="card-header p-0" id="heading<?=$i?>">
                <h3 class="mb-0">
                    <a class="btn btn-default btn-block text-left collapsed" data-toggle="collapse"
                        data-target="#collapse<?=$i?>" aria-expanded="false"
                        aria-controls="collapse<?=$i?>"
                        id="collapseLink<?=$i?>">
                        <div class="row">
                            <div class="col col-8">
                                <h2 style="font-weight: normal;"><i class="fas fa-plus-circle"></i>
                                    <?=$produto['nome']?></h2>
                                    <?php if($produto['varios_sabores']) { ?>  
                                                    <input type="text" style="display: none" id="produtos<?= $i ?>tamanho_escolhido" name="produtos<?= $i ?>tamanho_escolhido" value="" v-bind:value="sizechoosen" required data-parsley-required-message="Escolha pelo menos 1 sabor" data-parsley-errors-messages-disabled>    
                                                    <span class="parsley-required" style="color: #b32721" v-show="sizechoosen == ''" >Escolha pelo menos 1 sabor</span>
                                                    <span style="color: #b32721">{{ sizesavailable(<?= $produto['id_tamanho'] ; ?>) }}</span>
                                                <?php } ?>
                                <?=($produto['varios_sabores']) ? '<br />* selecione os sabores' : '';?>
                            </div>
                            <div class="col col-4 text-right">
                                <?php
if ($produto['varios_sabores']) {
?>
                                <br />
                                <h2>{{ priceLabelShow }}<span v-show="false" class="valorProduto"
                                        data-valor="<?=Helper::valor($produto['preco'])?>"
                                        data-sabores="1"><?=Helper::valor($produto['preco'])?></span>
                                </h2>
                                <?php
} else {
?>
                                <h2>R$ <span class="valorProduto">
                                        <?=Helper::valor($produto['preco'])?></span>
                                </h2>
                                <?php
}?>
                            </div>
                        </div>
                    </a>
                </h3>
            </div>
            <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>"
                data-parent="#accordionExample">
                <div class="card-body">
                    <?php
if ($produto['varios_sabores']) {
?>
                    <ul class="list-group m-t">
                        <li class="list-group-item">
                            Selecione os sabores
                        </li>
                        <?php
foreach ($produto['sabores'] as $sabor) {
    ?>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-12">
                                    <input
                                        :disabled="isDisable('<?=$sabor['id'];?>', '<?=$sabor['id_tamanho'];?>')"
                                        type="checkbox" v-model="sabores" name="sabores[]"
                                        value="<?=$sabor['id'];?>-<?=$sabor['preco'];?>-*$*-<?=$sabor['nome'];?>" />
                                    <textarea name="produtos[<?=$i;?>][sabores]" v-show="false">
                                        {{ inputSabores }}
                                    </textarea>
                                    <label><?=$sabor['nome'];?></label>
                                    <p><?=$sabor['descricao'];?></p>
                                </div>
                            </div>
                        </li>
                        <?php
}?>
                    </ul>
                    <br />
                    <?php
}?>
                    <ul class="list-group m-t">
                        <?php
$prod_adc = $produto['adicionais'];
$grupo    = [];
foreach ($prod_adc as $item) {
$grupo[$item['id']]        = $response['grupos'][$item['id']];
$adc[$item['id']]          = [];
$adc[$item['id']]['id']    = $item['id'];
$adc[$item['id']]['preco'] = $item['preco'];
$adc[$item['id']]['nome']  = $item['nome'];
if ($item['tipo'] == 'Um') {
    $adc[$item['id']]['tipo'] = 'label';
} else {
    $adc[$item['id']]['tipo'] = 'checkbox';
    foreach ($grupo[$item['id']] as $gr) {
        $adc[$item['id']]['tipo'] = 'radio';
        $adc[$item['id']]['pai']  = $grupo[$item['id']]['id_grupo'];
    }
}
}
//Checagem opcionais para só exibir se houver
if(count($prod_adc) > 0){
echo '<li class="list-group-item">Opcionais</li>';

foreach ($prod_adc as $item) {
if ($adc[$item['id']]['tipo'] == 'checkbox') {
    ?>
                        <li class="list-group-item">
                        <input type="checkbox" id="box-<?=$item['id']?>" valor="<?=$adc[$item['id']]['preco'];?>"
                                    v-on:change="setAdicional($event)"
                                    name="<?='produtos[' . $i . '][adicional][]'?>"
                                    value="<?=$item['id']?>">
                            <label for="box-<?=$item['id']?>" class="nomeAdicional"><span
                                    class="float-right"><?=$adc[$item['id']]['preco'] > 0 ? 'R$ ' : ''?><?=$adc[$item['id']]['preco'] > 0 ? Helper::valor($adc[$item['id']]['preco']) : ''?></span><?=$adc[$item['id']]['nome']?></label>                                              
                        </li>
                        <?php
        }
    }
    }
?>

                    </ul>
                    <br />
                    <ul class="list-group m-t">
                        <li class="list-group-item">
                            Observações
                        </li>
                        <li class="list-group-item">
                            <textarea name="<?='produtos[' . $i . '][obs]'?>" class="retira_ing"
                                placeholder="Digite sua solicitação"></textarea>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <?php
$i++;
}
} else {
echo '<input type="hidden"" name="produtos[' . $i . '][id_produto]" value="' . $produto['id'] . '">';
echo '<input type="hidden"" name="produtos[' . $i . '][id_tamanho]" value="' . $produto['id_tamanho'] . '">';
echo '<input type="hidden"" name="produtos[' . $i . '][qtde]" value="' . $produto['qtde'] . '">';?>
        <div class="card appVue">
            <div class="card-header p-0" id="heading<?=$i?>">
                <h3 class="mb-0">
                    <a class="btn btn-default btn-block text-left collapsed" data-toggle="collapse"
                        data-target="#collapse<?=$i?>" aria-expanded="false"
                        aria-controls="collapse<?=$i?>">
                        <div class="row">
                            <div class="col col-8">
                                <h2 style="font-weight: normal;"><?php if ($produto['tipo_item'] == 'Comida') {
echo '<i class="fas fa-plus-circle"></i> ';
}?><?=$produto['nome']?></h2>
                                <?=($produto['varios_sabores']) ? '<br />* selecione os sabores' : '';?>
                            </div>
                            <div class="col col-3 text-right">
                                <?php
if ($produto['varios_sabores']) {
?>
                                <br />
                                <h2 v-if="showPrice">R$ <span
                                        class="valorProduto">{{ priceLabel }}</span></h2>
                                <?php
} else {
?>
                                <h2><?=$produto['qtde'];?>x R$ <span class="valorProduto">
                                        <?=Helper::valor($produto['preco'])?></span>
                                </h2>
                                <?php
}?>
                            </div>
                        </div>
                    </a>
                </h3>
            </div>
            <?php if ($produto['tipo_item'] == 'Comida') {
?>
            <div id="collapse<?=$i?>" class="collapse" aria-labelledby="heading<?=$i?>"
                data-parent="#accordionExample">
                <div class="card-body">
                    <?php
if ($produto['varios_sabores']) {
?>
                    <ul class="list-group m-t">
                        <li class="list-group-item">
                            Selecione os sabores
                        </li>
                        <?php
foreach ($produto['sabores'] as $sabor) {
    ?>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-12">
                                    <input
                                        :disabled="isDisable('<?=$sabor['id'];?>', '<?=$sabor['id_tamanho'];?>')"
                                        type="checkbox" v-model="sabores" name="sabores[]"
                                        value="<?=$sabor['id'];?>-<?=$sabor['preco'];?>-*$*-<?=$sabor['nome'];?>" />

                                    <textarea name="produtos[<?=$i;?>][sabores]" v-show="false">
                                    {{ inputSabores }}
                                </textarea>
                                    <label><?=$sabor['nome'];?></label>
                                    <p><?=$sabor['descricao'];?></p>
                                </div>
                            </div>
                        </li>
                        <?php
}?>
                    </ul>
                    <br />
                    <?php
}?>
                    <ul class="list-group m-t">
                        <li class="list-group-item">
                            Observações
                        </li>
                        <li class="list-group-item">
                            <textarea name="<?='produtos[' . $i . '][obs]'?>" class="retira_ing"
                                placeholder="Digite sua solicitação"></textarea>
                        </li>
                    </ul>
                </div>
            </div>
            <?php
}?>

        </div>
        <?php
}
$i++;
}
?>

    </div>

    <div class="clearfix"></div>
</div>

<div class="container pl-3 pr-3 bg-light">
    <?php
if ($response['loja']['taxa_entrega']) {
?>
    <div class="row pt-2 pb-2" id="taxaEntrega"
        style="border-bottom: 1px solid rgba(0,0,0,.125); display: none;">
        <div class="col-8">
            <h2 style="font-weight: normal;">Taxa de entrega</h2>
        </div>
        <div class="col-4 text-right">
            <h2 style="margin-right: -3px;">R$ <span
                    class="taxaEntrega"><?=Helper::valor($response['loja']['taxa_entrega'])?></span>
            </h2>
        </div>
    </div>
    <?php
}
?>
    <div class="row pt-3" style="">
        <div class="col-8">
            <h2 class="font-16 uppercase ultrabold">TOTAL</h2>
        </div>
        <div class="col-4 text-right">
            <h2 class="ultrabold font-16">R$ <span class="valorTotal"><?=Helper::valor($total)?></span></h2>
        </div>
    </div>
    <div class="row">
            <div class="col-12" id="valor_pedido">
                <span class="parsley-required" style="color: #b32721; display: none;" id="erro_pedido_minimo">Valor de Pedido Mínimo é R$ <?= Helper::valor($response['loja']['pedido_minimo']) ?></span>
                <span class="parsley-required" style="color: #b32721; display: none;" id="erro_pedido_maximo">Valor de Pedido Máximo é R$ <?= Helper::valor($response['loja']['pedido_maximo']) ?></span>
            </div>
        </div>
</div>

<div class="container pl-3 pr-3 pt-3 bg-light">
    <div class="row">
        <div class="col-12">
            <div class="form-group select-box select-box-1">
                <select name="tipo" id="tipo" class="form-control" required>
                    <option value="">Quer que entregue?</option>
                    <option value="Entregar">Sim, por favor</option>
                    <?php if($response['loja']['retirar_balcao'] == 1): ?>                   
                    <option value="Retirar Balcão">Não, vou buscar</option>
                    <?php endif ?>
                </select>
            </div>
        </div>
        <div class="col-12" id="bairros" style="display: none;">
            <div class="form-group">
                <select id="valor_frete" name="valor_frete" class="form-control">
                    <option value="">Em que bairro você está?</option>
                    <?php
foreach ($response['bairros'] as $item) {
?>
                    <option value="<?=$item['valor_frete']?>"><?=$item['bairro']?></option>
                    <?php
}
?>
                </select>
                <input type="hidden" id="nome_bairro" name="bairro" value="" />
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <select name="id_pagamento" id="formapagamento" class="form-control" required>
                    <option value="">Como quer pagar?</option>
                    <?php
foreach ($response['formapagamento'] as $item) {
?>
                    <option value="<?=$item['id']?>"><?=$item['formapagamento']?></option>
                    <?php
}
?>
                </select>
                <script>
                $('#formapagamento').val('<?=($_SESSION['pedido']['id_pagamento'] ?: '')?>')
                </script>
            </div>
        </div>
        <div class="col-12" id="troco" style="display: none;">
                <div class="form-group">
                <label class="field-title color-theme">Precisa de troco para?</label>
                    <input name="valor_troco" id="valor_troco" inputmode="numeric" type="text" class="form-control valor" autocomplete="off">
                    <span class="parsley-required" style="color: #b32721; display: none;" id="erro_troco">Valor deve ser maior que R$ <span class="valorTotalTroco">0,00</span></span>
                </div>
            </div>

    </div>
</div>

<div class="container pb-3 bg-light">
    <input type="hidden" name="id_loja" value="<?=$_SESSION['pedido']['id_loja']?>">
    <button type="submit" class="btn bg-color-blue-dark button-full button-rounded button-sm uppercase ultrabold btn-block text-center" id="avancar"><i class="fas fa-chevron-circle-right"></i> Avançar</button>
</div>

</form>

<div id="voltar"
style="position: fixed; top: 0px; left: 0px; width: 100%; height:100%; background-color: rgba(0,0,0,0.7);display: none">
<div
    style="position: absolute; top: 35%; left: 0px; width: 100%; padding: 20px; background-color: #ec891c; color: #fff; text-align: center;">
    <strong>Voltar irá cancelar seu pedido.</strong><br>
    Você irá para a tela do estabelecimento.
    <br><br>
    Certeza que deseja voltar?<br><br>
    <a href="<?=URL_PUBLIC?>/estabelecimento/<?=$response['loja']['id']?>"
        class="btn btn-danger mr-3">VOLTAR</a>
    <a id="cancelarVoltar" class="btn btn-warning">CANCELAR</a>
</div>
</div>

</div>
     </section>       
</div>
<!--End PageContent-->
<?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script>
    $("form").submit(function(e) {
        validaPedidoMinMax();
        $.ajax({
            type: "POST",
            url: '<?=URL_PUBLIC?>/ajax',
            dataType: 'text',
            data: {
                controller: 'Site',
                action: 'checkoutSave',
                param: $('form').serialize()
            },
            cache: false,
            success: function(data) {
                if ($('.parsley-required').is(':visible')) {

                } else {
                    window.location.href = '<?=URL_PUBLIC?>/checkoutentrega';
                }
            },
        });
        e.preventDefault();
        return false;
    });

    $('#cancelarVoltar').click(function() {
        $('#voltar').hide('fast');
    })

    //    history.pushState(null, null, location.href);
    //    window.onpopstate = function () {
    //        $('#voltar').show('fast');
    //        history.go(1);
    //    };

    $('#tipo').change(function() {
        if ($(this).val() == 'Entregar') {
            $('#bairros').show('fast');
            $('#valor_frete').prop('required', true);
        } else {
            $("#valor_frete").prop('selectedIndex', 0).change();
            $('#bairros').hide('fast');
            $('#taxaEntrega').hide('fast');
            $('#valor_frete').prop('required', false);
        }
    }).change()

    var taxaAntiga = 0;
    var valorTotal = 0;
    function setValorTotal(valorTotal){
        //console.log('setValorTotal',valorTotal);
        var valorTotalFormatado = valorTotal.toFixed(2).replace('.',',');
        //console.log('setValorTotal formatado R$',valorTotalFormatado);
        $('.valorTotal').text(valorTotalFormatado);
        $('.valorTotalTroco').text(valorTotalFormatado);
    }

    $('#valor_frete').change(function() {

        $('#nome_bairro').val($(this).find('option:selected').text());
        var valorTotal = parseFloat($('.valorTotal').text().replace(',', '.'));
        if ($(this).val() != '') {
            var taxaEntrega = parseFloat($(this).val());
            $('.taxaEntrega').text(taxaEntrega.toFixed(2).replace('.', ','));
            $('#taxaEntrega').show('fast');
            valorTotal = ((valorTotal - taxaAntiga) + taxaEntrega);
            taxaAntiga = parseFloat($(this).val());
        } else {
            taxaAntiga = 0;
            var taxaEntrega = parseFloat($('.taxaEntrega').text().replace(',', '.'));
            if ($('#taxaEntrega').is(':visible')) valorTotal = (valorTotal - taxaEntrega);
            $('#taxaEntrega').hide('fast');
        }
        setValorTotal(valorTotal);
        //$('.valorTotal').text(valorTotal.toFixed(2).replace('.', ','));
    }).change();

    $('#formapagamento').change(function() {
        if ($(this).val() == 13) {
            $('#troco').show('fast');
        } else {
            $('#troco').hide('fast');
        }
    }).change()

    //Validar troco
    $('#valor_troco').change(function () {
        var valor_troco = $(this).val();
        validaTroco(valor_troco);        
    }).change();

    function validaTroco(valor){
        if (valor == 0){
            valor = '';
        }
        if(valor != ''){
            valor_troco = parseFloat(valor.replace(',','.'));
            $('#erro_troco').css('display', 'none');
            if(valor_troco > valorTotal){
                $("#avancar").attr("disabled", false);
                $('#erro_troco').css('display', 'none');
            }else{
                $("#avancar").attr("disabled", true);
                $('#erro_troco').css('display', 'inline');
            }
        } 
    }

    function validaPedidoMinMax(){
        if(valorTotal < <?= $response['loja']['pedido_minimo'] ?>)
            $('#erro_pedido_minimo').css('display', 'inline');
        else
            $('#erro_pedido_minimo').css('display', 'none');

        if(valorTotal > <?= $response['loja']['pedido_maximo'] ?>)
            $('#erro_pedido_maximo').css('display', 'inline');
        else
            $('#erro_pedido_maximo').css('display', 'none');
    }

    $('form').parsley();

    $('.inputAdicional').change(function() {
        var idItem = $(this).data('id');
        var valorProduto = parseFloat($('#'+idItem+' .valorProduto').text().replace(',','.'));
        var valorAdicional = parseFloat($(this).siblings('.valorAdicional').text().replace('R$ ','').replace(',','.'));
        valorTotal = parseFloat($('.valorTotal').text().replace(',','.'));
        if ( $(this).get(0).type == 'checkbox' ) {
              if($(this).is(':checked')) {
                  var valorProdutoTotal = (valorProduto+valorAdicional);
                  valorTotal = (valorTotal+valorAdicional);
                } else if($('#'+idItem+' .valorProduto').data('valor') != valorProduto) {
                    if ($('#'+idItem+' .valorProduto').data('sabores') != '1') {
                        var valorProdutoTotal = (valorProduto-valorAdicional);
                        valorTotal = (valorTotal-valorAdicional);
                    } else {
                        var valorProdutoTotal = (valorProduto-valorAdicional);
                        valorTotal = (valorTotal+0);
                    }
              }
        } else {
              if($(this).is(':checked')) {
                  var valorProdutoTotal = (valorProduto+valorAdicional);
                  valorTotal = (valorTotal+valorAdicional);
              } else if($('#'+idItem+' .valorProduto').data('valor') != valorProduto) {
                    if ($('#'+idItem+' .valorProduto').data('sabores') != '1') {
                        var valorProdutoTotal = (valorProduto-valorAdicional);
                        valorTotal = (valorTotal-valorAdicional);
                    } else {
                        var valorProdutoTotal = (valorProduto-valorAdicional);
                        valorTotal = (valorTotal+0);
                    }
              }
        }
        setTimeout(()=>{$('form').parsley().validate()}, 200);
        $('#'+idItem+' .valorProduto').text(valorProdutoTotal.toFixed(2).replace('.',','));
        setValorTotal(valorTotal);
    }).change()

    $(".appVue").each(function(index, el) {
        var app = new Vue({
            el,
            data: {
                sabores: [],
                partes: <?=$response['loja']['partes'];?>
            },
            computed: {
                inputSabores() {
                    return this.sabores.map(function(item) {
                        var saborId = item.split('-')

                        return saborId[0]
                    }).join(',')
                },
                showPrice() {
                    return _.size(this.sabores) > 0
                },
                priceLabelShow() {
                    var price = this.priceLabel
                    if (price == '0') {
                        return 'R$ ?'
                    }

                    return 'R$ ' + price.replace(".", ",")
                },
                priceLabel() {
                    var valores = this.sabores.map(function(item) {
                        var valor = item.split('-')

                        return valor[1]
                    })

                    if (_.size(valores) === 0) {
                        return 0
                    }

                    var price = _.max(valores)

                    return price
                }
            },
            methods: {
                setAdicional(event) {
                    var valor = $(event.target).attr('valor')
                    var total = parseFloat($('.valorTotal').text().replace(',', '.'));
                    var soma = $(event.target).is(':checked') ? total + parseFloat(valor) : total -
                        parseFloat(valor)

                    $('.valorTotal').text(soma.toFixed(2).replace('.', ','));
                },
                isDisable(id, tamanho) {
                    try {
                        var partes = parseInt(this.partes[tamanho])
                    } catch (e) {
                        var partes = 0
                    }
                    return (this.sabores.length === partes && this.sabores.filter(function(item) {
                        var sabores = item.split('-')

                        return sabores[0] == id
                    }).length === 0)
                }
            },
            watch: {
                priceLabel(current, last) {
                    var total = parseFloat($('.valorTotal').text().replace(',', '.'));
                    var soma = total - parseFloat(last || 0) + parseFloat(current || 0)

                    $('.valorTotal').text(soma.toFixed(2).replace('.', ','));
                }
            }
        })
    })
    $(document).ready(function(){
                $(valor_troco).inputmask("numeric", {
                    groupSeparator: ".",
                    radixPoint: ",",
                    placeholder: "",
                    autoGroup: true,
                    integerDigits: 16,
                    digits: 2,
                    digitsOptional: false,
                    clearMaskOnLostFocus: false
                });
            });
</script>
</body>
</html>
