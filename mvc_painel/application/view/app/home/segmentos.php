<?php
$title = '';
$css   = [];
$script = [];
$page = "home";
require APP . 'view/app/_templates/initFile.php';
?>
<style>
#preloader {
    background-color: var(--default);
}
</style>

<body class="theme-light" data-highlight="blue2">
    <?php require APP . 'view/app/_templates/preloader.php';?>
    <div id="page-transitions">
        <!--header-->
        <div class="page-hider"></div>
        <!--End header-->
        <!--Page Content-->
        <div class="page-content header-clear animated fadeIn faster">
            <a href="./estabelecimento/333">Clique aqui para visualizar o mercantil teste</a>
        </div>
        <!--End PageContent-->
        <?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
</body>

</html>