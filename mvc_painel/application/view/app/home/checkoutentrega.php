<?php

use AgendaLabs\Libs\Helper;

$title = 'Checkout';
$css   = [  
    URL_PUBLIC.'/assets/app/css/checkout'.MINIFY.'.css', 
];
$script = [   
    URL_PUBLIC.'/assets/admin/js/plugins/parsley/parsley.min.js',
    URL_PUBLIC.'/assets/admin/js/plugins/parsley/i18n/pt-br.js', 
    URL_PUBLIC.'/assets/app/scripts/jquery.maskedinput.android.js', 
];
$page = "checkout";
require APP . 'view/app/_templates/initFile.php';
?>
<body class="theme-light" data-highlight="blue2">
<div id="page-transitions">
<!--header-->
<div class="page-hider"></div>

<!--End header-->
    <!--Page Content-->
    <section class="ui-content animated fadeIn faster">
<div class="page-content pages_maincontent header-clear-large">
<?php
include APP . 'view/app/home/modules/search.php';
?>
<div class="heading-style pb-0 mb-0">
                <h2 class="heading-title"><?= ($_SESSION['pedido']['tipo'] == 'Retirar Balcão' ? 'Retirar no local' : 'Entrega') ?></h2>
                <em class="opacity-60">Antes de concluir, confirme os dados abaixo</em>
                <div class="mt-4">
                    <i class="fas fa-map-marker-alt font-30 color-blue-dark"></i>
                </div>      
</div>
<div class="container">
<div class="aviso-add mb-3">
<?php
    if($_SESSION['pedido']['tipo'] == 'Retirar Balcão') {
        ?>
    <div class="row mt-3 text-center" id="formBox">
        <div class="col-12">
            <?php
            if($response['loja']['tempo_entrega']) {
                echo '<h5 class="mb-3 text-center">Seu pedido ficará pronto às <span class="text-danger">' . date('H:i',strtotime('+' . $response['loja']['tempo_entrega'] . ' minutes',strtotime(date('H:i')))) . '</span></h5>';
            }
            ?>
            <?php
            if($response['loja']['endereco']) {
                ?>
                <div class="mb-3 text-center">
                    <h5><?= $response['loja']['endereco'] . ($response['loja']['numero'] ? ', '.$response['loja']['numero'] : '')  ?></h5>
                    <h5 class="m-b-md"><?= $response['loja']['complemento'] . ($response['loja']['complemento'] && $response['loja']['bairro'] ? ', ' : '') . $response['loja']['bairro']  ?></h5>
                </div>
                <?php
            } else {
                ?>
                <h5 class="mb-3 text-center">Este estabelecimento não possui loja física ou o seu endereço ainda não foi cadastrado.</h5>
                <?php
            }
            ?>
        </div>
    </div>
        <?php
    }
        if ( $_SESSION['pedido']['bairro'] != $response['ultimoPedido']['bairro'] ) {
          $response['ultimoPedido'] = [];
        }
    ?>
</div>
</div>
        
        <div class="container bg-light">
        <form method="post" action="<?= URL_PUBLIC ?>/finalizarpedido">
        <div class="row mt-3" id="formBox">
            <div class="col-12">
                <div class="form-group">
                    <input name="nome" type="text" class="form-control" placeholder="Nome e Sobrenome" required value="<?= $response['ultimoPedido']['nome'] ?? null ?>">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <input id="telefone" inputmode="numeric" name="celular" type="text" class="form-control" placeholder="Celular" required value="<?= $response['ultimoPedido']['celular'] ?? null ?>">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <input name="endereco" type="text" class="form-control" placeholder="Endereço" required value="<?= $response['ultimoPedido']['endereco'] ?? null ?>">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <input name="numero" inputmode="numeric" type="text" class="form-control" placeholder="Nº" required value="<?= $response['ultimoPedido']['numero'] ?? null ?>">
                </div>
            </div>
            <div class="col-8">
                <div class="form-group">
                    <input name="complemento" type="text" class="form-control" placeholder="Complemento" value="<?= $response['ultimoPedido']['complemento'] ?? null ?>">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <input name="ptoreferencia" type="text" class="form-control" placeholder="Ponto de Referência" required value="<?= $response['ultimoPedido']['ptoreferencia'] ?? null ?>">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <input name="bairro" type="text" class="form-control desabilitado" placeholder="Bairro" required value="<?= $_SESSION['pedido']['bairro'] ?? null ?>">
                </div>
            </div>
            <div class="col col-12">
                <div class="form-group">
                </div>
            </div>
            <div class="container pb-3 bg-light">
    <input type="hidden" name="id_loja" value="<?=$_SESSION['pedido']['id_loja']?>">
    <button type="submit" class="btn bg-color-blue-dark button-full button-rounded button-sm uppercase ultrabold btn-block text-center" id="avancar"><i class="fas fa-check"></i> Confirmar Pedido</button>
</div>
        </div>       

</form>
        </div>

<div id="voltar"
style="position: fixed; top: 0px; left: 0px; width: 100%; height:100%; background-color: rgba(0,0,0,0.7);display: none">
<div
    style="position: absolute; top: 35%; left: 0px; width: 100%; padding: 20px; background-color: #ec891c; color: #fff; text-align: center;">
    <strong>Voltar irá cancelar seu pedido.</strong><br>
    Você irá para a tela do estabelecimento.
    <br><br>
    Certeza que deseja voltar?<br><br>
    <a href="<?=URL_PUBLIC?>/estabelecimento/<?=$response['loja']['id']?>"
        class="btn btn-danger mr-3">VOLTAR</a>
    <a id="cancelarVoltar" class="btn btn-warning">CANCELAR</a>
</div>
</div>

</div>
     </section>       
</div>
<!--End PageContent-->
<?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('form').parsley();
        <?= ($_SESSION['pedido']['tipo'] == 'Retirar Balcão' ? '$("input").attr("type","hidden");' : '') ?>
    })

    $('#cancelarVoltar').click(function () {
        $('#voltar').hide('fast');
    })

//    history.pushState(null, null, location.href);
//    window.onpopstate = function () {
//        $('#voltar').show('fast');
//        history.go(1);
//    };
</script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script type="text/javascript">
            $(function() {
                $.mask.definitions['~'] = "[+-]";
                $("#telefone").mask("(99) 9999?9-9999");
                $("#telefone").on("blur", function() {
                    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

                    if( last.length == 3 ) {
                        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
                        var lastfour = move + last;

                        var first = $(this).val().substr( 0, 9 );

                        $(this).val( first + '-' + lastfour );
                    }
                });
                $("#cep").mask("99.999-999");
            });
        </script>
</body>
</html>