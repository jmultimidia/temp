<?php
use AgendaLabs\Libs\Helper;

$title = 'Estabelecimento';
$css   = [      
    URL_PUBLIC . '/assets/app/css/framework' . MINIFY . '.css?202004021911',
];
$script = [    
];
$page = "estabelecimentos";
require APP . 'view/app/_templates/initFile.php';
?>

<body class="theme-light" data-highlight="blue2">
    <?php require APP . 'view/app/_templates/preloader.php';?>
    <div id="page-transitions">
        <!--header-->
        <div class="page-hider"></div>

        <!--End header-->
        <!--Page Content-->
        <div class="page-content header-clear-large animated fadeIn faster">
            <!--
<div class="discover-gradient">
<svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"></polygon></svg>
</div>
-->
            <section id="estabelecimento">
                <div class="heading-style pb-0 mb-0">
                    <h5 class="bolder">Você está em <a href="<?=URL_PUBLIC.DS.$page?>"
                            class="button button-small button-round button-fill"><?=$title?></a></h5>
                </div>
                <div class="content itens-details ver-radio">

                    <div class="row mt-0 pt-0 pb-0 mb-1">
                        <div class="col col-12">
                            <h3><?=$response['nome']?></h3>
                            <?php if ($response['preco'] > '0.00'):?>
                            <h4 class="uppercase ultrabold">R$ <?=Helper::valor($response['preco'])?></h4>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="descricao">
                        <h2 class="uppercase ultrabold">Descrição</h2>
                        <p><?=$response['descricao']?></p>
                    </div>
                </div>

        </div>
        <!--End PageContent-->
        <?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
</body>

</html>