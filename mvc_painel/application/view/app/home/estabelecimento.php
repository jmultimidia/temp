<?php

use AgendaLabs\Libs\Helper;

$title = 'Estabelecimento';
$css   = [
    URL_PUBLIC . '/assets/app/css/framework' . MINIFY . '.css?202004021911',
    URL_PUBLIC . '/assets/app/css/framework-store' . MINIFY . '.css?201912071637',
    URL_PUBLIC . '/album/PhotoSwipe/dist/photoswipe.css',
    URL_PUBLIC . '/album/PhotoSwipe/dist/default-skin/default-skin.css',
    URL_PUBLIC . '/assets/app/css/gallery'.MINIFY.'.css?202003281914',    
    URL_PUBLIC . '/assets/app/css/cart'.MINIFY.'.css',    
];
$script = [
    URL_PUBLIC . '/assets/app/scripts/plugins' . MINIFY . '.js',
];
$page = "estabelecimento";

require APP . 'view/app/_templates/initFile.php';
?>

<body class="theme-light" data-highlight="blue2">
    <?php require APP . 'view/app/_templates/preloader.php';?>
    <div id="page-transitions">
        <!--header-->
        <div class="page-hider"></div>
        <!--End header-->
        <!--Page Content-->
        <div class="page-content header-clear-larger animated fadeIn faster">
            <!--
<div class="discover-gradient">
<svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"></polygon></svg>
</div>
-->
            <section id="estabelecimento">
                <?php
if ($response['loja']):
    if ($response['loja']['capa']): ?>
                <img src="<?=$response['loja']['capa']?>" data-src="<?=$response['loja']['capa']?>"
                    class="preload-image responsive-image bottom-5">
                <?php endif;?>
                <div class="chips chips-small mt-2">
                    <?php if ($response['loja']['aberto']): ?>
                    <a href="#" class="color-black"><i class="chips-icon fa fa-check bg-green-dark"></i>Aberto</a>
                    <?php else:
    if ($response['loja']['target_fechado'] == 1):
    ?>
                    <a href="#" class="color-black"><i class="chips-icon fas fa-times bg-red-dark"></i>Fechado</a>
                    <?php endif;
endif;
if ($response['loja']['aberto']):
    if ($response['loja']['delivery']):
    ?>
                    <a href="#" class="color-black"><i class="chips-icon fas fa-motorcycle bg-blue2-dark"></i>Delivery <?php if ($response['loja']['taxa_entrega'] > 0) {
        ?> | R$ <?=Helper::valor($response['loja']['taxa_entrega'])?><?php
    }?></a>
                    <?php if ($response['loja']['tempo_entrega']): ?>
                    <a href="#" class="color-black"><i
                            class="chips-icon far fa-clock bg-default-dark"></i><?=$response['loja']['tempo_entrega']?>
                        min</a>
                    <?php endif; //taxa entrega
endif; //delivery
endif; //aberto
if (in_array($response['loja']['id'], $_SESSION['favoritos'] ?? [])):
?>
                    <span class="favorite color-black"><i
                            class="favorite-btn chips-icon fas fa-heart bg-red-dark pr-005"
                            data-id="<?=$response['loja']['id']?>"></i><span class="favTxt">Remover
                            favorito</span></span>
                    <?php else: ?>
                    <span class="favorite color-black"><i
                            class="favorite-btn chips-icon fas fa-heart bg-night-dark pr-005"
                            data-id="<?=$response['loja']['id']?>"></i><span class="favTxt"
                            data-id="<?=$response['loja']['id']?>">Favoritar</span></span>
                    <?php endif;?>
                </div>
                <div class="clear"></div>
                <div class="store-slide-title mt-2 mb-2">
                    <h4 class="mt-2 mb-2 center-text"><?=$response['loja']['nome']?></h4>
                    <em class="color-darkgray-dark small-text center-text"><?=$response['loja']['descricao']?></em>
                </div>
                <!--Sociais-->
                <?php if ($response['loja']['instagram'] || $response['loja']['facebook'] || $response['loja']['website']): ?>
                <div class="footer-socials mt-2 mb-2">
                    <?php if ($response['loja']['instagram']): ?>
                    <a onclick="javascript:window.open('<?=$response['loja']['instagram']?>', '_system')"
                        class="scale-hover no-border"><i class="bg-instagram fab fa-instagram font-16"></i></a>
                    <?php endif;
                    if ($response['loja']['facebook']): ?>
                    <a onclick="javascript:window.open('<?=$response['loja']['facebook']?>', '_system')"
                        class="scale-hover no-border "><i class="bg-facebook fab fa-facebook-f font-16"></i></a>
                    <?php endif;
                    if ($response['loja']['website']): ?>
                    <a href="<?=$response['loja']['website']?>" target="_blank"
                        class="scale-hover no-border back-to-top"><i class="bg-highlight fab fa-chrome font-16"></i></a>
                    <?php endif;?>
                </div>
                <?php endif;?>
                <!--End Sociais-->
                <div class="content">
                    <!--tabs-->
                    <div class="tabs">
                        <div class="tab-pill-titles" data-active-tab-pill-background="bg-default-dark">
                            <?php if ($response['loja']['aberto'] && $response['loja']['online']): ?>
                            <a href="#" class="active-tab-pill-button bg-default-dark" data-tab-pill="tab-pill-1a">Faça
                                sua escolha</a>
                            <?php else:?>
                            <a href="#" class="active-tab-pill-button bg-default-dark" data-tab-pill="tab-pill-1a">Entre
                                em contato</a>
                            <?php endif;?>
                            <a href="#" class="color-dark" data-tab-pill="tab-pill-2a">Localização</a>
                            <a href="#" class="color-dark" data-tab-pill="tab-pill-3a">Mais Informações</a>
                        </div>
                        <div class="tab-pill-content">
                            <div class="tab-item active-tab" id="tab-pill-1a">
                                <div class="accordion accordion-style-1">
                                    <?php
        if ($response['loja']['aberto'] && $response['loja']['online']):

                                    switch ($response['loja']['type']) :
                                        case 0:
                                            include APP . 'view/app/home/modules/lista_produtos.php';
                                            break;
                                        case 1:
                                            include APP . 'view/app/home/modules/lista_produtos.php';
                                            break;
                                        case 2:
                                            include APP . 'view/app/home/modules/type2.php';
                                            break;
                                        case 3:
                                            include APP . 'view/app/home/modules/type3.php';
                                            break;
                                    endswitch;
        else:?>
                                    <!--Contatos e Reserva-->
                                    <?php include APP . 'view/app/home/modules/contatos.php';?>
                                    <!-- End Contatos e Reservas-->
                                    <?php endif;
                                    ?>

                                </div>
                            </div>
                            <!--Localização-->
                            <div class="tab-item pt-1" id="tab-pill-2a">
                                <div class="heading-style bottom">
                                    <div class="line">
                                        <i class="fas fa-map-marker-alt font-30 color-secondary-light pl-3"></i>
                                        <div class="decoration decoration-line-fade opacity-20"></div>
                                    </div>
                                    <div class="address">
                                        <h5 class="mb-2">
                                        </h5>
                                        <h5>
                                            <?=$response['loja']['complemento'] . ($response['loja']['complemento'] && $response['loja']['bairro'] ? ', ' : '') . $response['loja']['bairro']?>
                                            <?php if (!empty($response['loja']['cep'])): echo " - CEP: " . $response['loja']['cep'] . "";endif;?>
                                        </h5>

                                        <a target="_blank"
                                            onclick="abrirMapa('<?=$response['loja']['lat']?>','<?=$response['loja']['lng']?>','<?=$response['loja']['nome']?>')"
                                            class="store-button button bg-secondary-dark button-icon button-full button-sm top-10 button-rounded uppercase ultrabold">
                                            <i class="far fa-map no-bold"></i>Abrir no mapa
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--End Localização-->
                            <!--Informações-->
                            <div class="tab-item pt-1" id="tab-pill-3a">
                                <?php
                            if ($response['horario']): ?>
                                <div class="heading-style">
                                    <div class="line">
                                        <i
                                            class="fas fa-user-clock heading-icon font-30 color-secondary-light pl-3"></i>
                                        <div class="decoration"></div>
                                    </div>
                                    <h3 class="heading-title">Horário de funcionamento</h3>
                                    <div class="card-body">
                                        <ul class="list-group m-t">
                                            <?php
                                    foreach ($response['horario'] as $item) :
                                ?>
                                            <li class="list-group-item"><span
                                                    class="pull-left"><?=$item['dia']?></span><i class="far fa-clock"
                                                    aria-hidden="true"></i><span
                                                    class="horario"><?=Helper::hora($item['hora_inicio'])?> -
                                                    <?=Helper::hora($item['hora_fim'])?></span>
                                            </li>
                                            <?php endforeach;?>
                                        </ul>
                                    </div>
                                </div>
                                <?php endif;
                            if ($response['loja']['type'] < 2):
                                if ($response['formapagamento']): ?>
                                <div class="heading-style">
                                    <div class="line">
                                        <i
                                            class="fas fa-money-check-alt heading-icon font-30 color-secondary-light"></i>
                                        <div class="decoration"></div>
                                    </div>
                                    <h3 class="heading-title">Formas de pagamento</h3>
                                    <div class="card-body formas-de-pagamento">
                                        <ul class="list-group m-t">
                                            <?php
                                    foreach ($response['formapagamento'] as $item):
                                ?>
                                            <li class="list-group-item"><?php
    if ($item['imagem']) :
            ?><figure><img src="<?=$item['imagem']?>" height="30"></figure><?php
    endif;?><span><?=$item['formapagamento']?></span>
                                            </li>
                                            <?php endforeach;?>
                                        </ul>
                                    </div>
                                </div>
                                <?php endif;
                            endif;     
                            if ($response['facilidade']):?>
                                <div class="heading-style">
                                    <div class="line">
                                        <i class="far fa-grin-alt heading-icon font-30 color-secondary-light pl-3"></i>
                                        <div class="decoration"></div>
                                    </div>
                                    <h3 class="heading-title">Facilidades</h3>
                                    <div class="facilidades chips chips-large my-3">
                                        <?php
                                    foreach ($response['facilidade'] as $item):
                                ?>
                                        <a href="#" class="color-black"><i
                                                class="chips-icon fa fa-check bg-green-dark"></i><?=$item['facilidade']?></a>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <?php endif;
                            if ($response['loja']['video']) :?>
                                <div class="content pt-0">
                                    <a href="<?=$response['loja']['video']?>" target="_blank"
                                        class="icon-column-center">
                                        <i class="fas fa-play color-danger-dark font-30 mt-0 pt-0 mb-2"></i>
                                        <h5 class="color-theme center-text bolder">Veja um vídeo deste estabelecimento
                                        </h5>
                                    </a>
                                </div>
                                <?php
                            endif;
  $imgs_og = $response['loja']['json_estabelecimento'];
    $imgs    = json_decode($imgs_og);
if ($imgs && $imgs_og != '{}'):
?>
                                <div class="heading-style">
                                    <div class="line">
                                        <i class="far fa-camera heading-icon font-30 color-secondary-light pl-3"></i>
                                        <div class="decoration"></div>
                                    </div>
                                    <h3 class="heading-title">Fotos do Estabelecimento</h3>
                                    <div class="my-gallery galeria" itemscope itemtype="http://schema.org/ImageGallery">
                                        <?php
foreach ($imgs as $key => $img) {
    $img  = Helper::urlHttps($img);
    $size = getimagesize($img);
    $w    = $size[0];
    $h    = $size[1];
    echo '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                                            <a href="' . $img . '" itemprop="contentUrl" data-size="' . $w . 'x' . $h . '">
                                                <img src="' . $img . '" class="thumbers" itemprop="thumbnail" alt=""/>
                                            </a>
                                            <figcaption itemprop="caption description"></figcaption>
                                        </figure>
                                      ';
}
?>
                                    </div>
                                </div>
                                <?php endif;?>
                            </div>
                            <!--End Informações-->
                        </div>
                    </div>
                    <!--endtabs-->
                </div>
                <?php else: ?>
                <div class="heading-style mt-5">
                    <div class="notification-large notification-has-icon notification-gray notification-green bottom-0">
                        <div class="notification-icon"><i class="far fa-frown-open notification-icon"></i></div>
                        <h1 class="uppercase ultrabold">Nenhum registro encontrado.</h1>
                    </div>
                    <?php endif;?>
            </section>
        </div>
    </div>
    <!--End PageContent-->
    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>
    <?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
    <script src="<?=URL_PUBLIC?>/album/PhotoSwipe/dist/photoswipe.min.js"></script>
    <script src="<?=URL_PUBLIC?>/album/PhotoSwipe/dist/photoswipe-ui-default.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
    function categButtonToggle(id) {
        //console.log(id);
        //if(attributeValue == 'collapsing')
        //    return;

        var aria = $('#categCollapseTrigger' + id).attr('aria-expanded')

        if (aria == 'true') {
            $('#categCollapseIndicator' + id).removeClass('fa-plus-circle');
            $('#categCollapseIndicator' + id).addClass('fa-minus-circle');
        } else {
            $('#categCollapseIndicator' + id).removeClass('fa-minus-circle');
            $('#categCollapseIndicator' + id).addClass('fa-plus-circle');
        }
    }

    var initPhotoSwipeFromDOM = function(gallerySelector) {

        // parse slide data (url, title, size ...) from DOM elements
        // (children of gallerySelector)
        var parseThumbnailElements = function(el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                figureEl,
                linkEl,
                size,
                item;

            for (var i = 0; i < numNodes; i++) {

                figureEl = thumbElements[i]; // <figure> element

                // include only element nodes
                if (figureEl.nodeType !== 1) {
                    continue;
                }

                linkEl = figureEl.children[0]; // <a> element

                size = linkEl.getAttribute('data-size').split('x');

                // create slide object
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(size[0], 10),
                    h: parseInt(size[1], 10)
                };



                if (figureEl.children.length > 1) {
                    // <figcaption> content
                    item.title = figureEl.children[1].innerHTML;
                }

                if (linkEl.children.length > 0) {
                    // <img> thumbnail element, retrieving thumbnail url
                    item.msrc = linkEl.children[0].getAttribute('src');
                }

                item.el = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);
            }

            return items;
        };

        // find nearest parent element
        var closest = function closest(el, fn) {
            return el && (fn(el) ? el : closest(el.parentNode, fn));
        };

        // triggers when user clicks on thumbnail
        var onThumbnailsClick = function(e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            // find root element of slide
            var clickedListItem = closest(eTarget, function(el) {
                return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
            });

            if (!clickedListItem) {
                return;
            }

            // find index of clicked item by looping through all child nodes
            // alternatively, you may define index via data- attribute
            var clickedGallery = clickedListItem.parentNode,
                childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;

            for (var i = 0; i < numChildNodes; i++) {
                if (childNodes[i].nodeType !== 1) {
                    continue;
                }

                if (childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }



            if (index >= 0) {
                // open PhotoSwipe if valid index found
                openPhotoSwipe(index, clickedGallery);
            }
            return false;
        };

        // parse picture index and gallery index from URL (#&pid=1&gid=2)
        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
                params = {};

            if (hash.length < 5) {
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if (!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');
                if (pair.length < 2) {
                    continue;
                }
                params[pair[0]] = pair[1];
            }

            if (params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;

            items = parseThumbnailElements(galleryElement);

            // define options (if needed)
            options = {

                // define gallery index (for URL)
                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function(index) {
                    // See Options -> getThumbBoundsFn section of documentation for more info
                    var thumbnail = items[index].el.getElementsByTagName('img')[
                            0], // find thumbnail
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect();

                    return {
                        x: rect.left,
                        y: rect.top + pageYScroll,
                        w: rect.width
                    };
                }

            };

            // PhotoSwipe opened from URL
            if (fromURL) {
                if (options.galleryPIDs) {
                    // parse real index when custom PIDs are used
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for (var j = 0; j < items.length; j++) {
                        if (items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    // in URL indexes start from 1
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if (isNaN(options.index)) {
                return;
            }

            if (disableAnimation) {
                options.showAnimationDuration = 0;
            }

            // Pass data to PhotoSwipe and initialize it
            gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        };

        // loop through all gallery elements and bind events
        var galleryElements = document.querySelectorAll(gallerySelector);

        for (var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i + 1);
            galleryElements[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if (hashData.pid && hashData.gid) {
            openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
        }
    };

    // execute above function
    initPhotoSwipeFromDOM('.my-gallery');
    </script>
    <script type="text/javascript">
    function total() {
        'use strict';
        $('.qtde').each(function() {
            if ($(this).val() > 0) {
                var id = $(this).parent().parent().parent().attr('id');
                var qtde = parseInt($(this).val());
                var total = $('.total').text();
                total = parseFloat(total.replace('R$ ', '').replace(',', '.'));
                var valor = $('#' + id + ' .valor').text();
                valor = parseFloat(valor.replace('R$', '').replace(',', '.'));
                var total = parseFloat(total + (valor * qtde)).toFixed(2);
                $('.total').text('R$ ' + total.replace('.', ','));
            }
        });
    }

    total();

    const parent = this
    $(".appVue").each(function(index, el) {
        'use strict';
        const app = new Vue({
            el,
            data: {
                message: 'Hello Vue!'
            },
            methods: {
                add(id) {
                    var qtde = parseInt($('#' + id + ' .qtde').val());
                    $('#' + id + ' .qtde').val(qtde + 1);

                    var total = $('.total').text();
                    total = parseFloat(total.replace('R$ ', '').replace(',', '.'));
                    var valor = $('#' + id + ' .valor').text().trim();
                    valor = parseFloat(valor.replace('R$', '').replace(',', '.'));
                    valor = (isNaN(valor) ? 0 : valor);
                    var qtde = parseInt($('#' + id + ' .qtde').val());
                    var total = parseFloat(total + valor).toFixed(2);

                    $('.total').text('R$ ' + total.replace('.', ','));
                },
                del(id) {
                    var qtde = parseInt($('#' + id + ' .qtde').val());
                    if (qtde > 0) {
                        $('#' + id + ' .qtde').val(qtde - 1);

                        var total = $('.total').text();
                        total = parseFloat(total.replace('R$ ', '').replace(',', '.'));
                        var valor = $('#' + id + ' .valor').text();
                        valor = parseFloat(valor.replace('R$', '').replace(',', '.'));
                        var qtde = parseInt($('#' + id + ' .qtde').val());
                        var total = parseFloat(total - valor).toFixed(2);
                        $('.total').text('R$ ' + total.replace('.', ','));
                    }
                }
            },
            mounted() {}
        })
    });

    $("#checkout").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '<?=URL_PUBLIC?>/ajax',
            dataType: 'text',
            data: {
                controller: 'Site',
                action: 'checkoutSave',
                param: $(this).serialize()
            },
            cache: false,
            success: function(data) {
                if ($('.parsley-required').is(':visible')) {

                } else {
                    window.location.href = '<?=URL_PUBLIC?>/checkout';
                }
            },
        });
    });

    function shadowBoxDisappear() {
        $('#product_shadow_box').css('display', 'none');
    }

    var shadowBoxId;

    function shadowBoxAppear(imgSrc, id) {
        gtag('event', 'Clique Foto Produto', {
            'event_category': 'Tela Estabelecimento',
            'event_label': 'Produto ' + id,
            'value': 0
        });
        $('#shadow-box-name').html($('#' + id + '_name').html());
        $('#shadow-box-description').html($('#' + id + '_description').html());
        $('#shadow-box-img').attr('src', '<?= URL_PUBLIC ?>/' + imgSrc);
        var qtde = parseInt($('#' + id + ' .qtde').val());
        $('#shadow_box_qtde').val(qtde);
        shadowBoxId = id;
        $('#product_shadow_box').css('display', 'block');
    }

    function shadowBoxAdd() {
        var id = shadowBoxId;
        var qtde = parseInt($('#' + id + ' .qtde').val());
        $('#' + id + ' .qtde').val(qtde + 1);
        $('#shadow_box_qtde').val(qtde + 1);
        var total = $('.total').text();
        total = parseFloat(total.replace('R$ ', '').replace(',', '.'));
        var valor = $('#' + id + ' .valor').text();
        valor = parseFloat(valor.replace('R$ ', '').replace(',', '.'));
        var qtde = parseInt($('#' + id + ' .qtde').val());
        var total = parseFloat(total + valor).toFixed(2);
        $('.total').text('R$ ' + total.replace('.', ','));
    }

    function shadowBoxDel() {
        var id = shadowBoxId;
        var qtde = parseInt($('#' + id + ' .qtde').val());
        if (qtde > 0) {
            $('#' + id + ' .qtde').val(qtde - 1);
            $('#shadow_box_qtde').val(qtde - 1);
            var total = $('.total').text();
            total = parseFloat(total.replace('R$ ', '').replace(',', '.'));
            var valor = $('#' + id + ' .valor').text();
            valor = parseFloat(valor.replace('R$ ', '').replace(',', '.'));
            var qtde = parseInt($('#' + id + ' .qtde').val());
            var total = parseFloat(total - valor).toFixed(2);
            $('.total').text('R$ ' + total.replace('.', ','));
        }
    }
    </script>
    <div class="zoom-product animated faster fadeIn" id="product_shadow_box">
        <div class="inner-shadow-box">
            <div class="zoom-inner"><img id="shadow-box-img" src="" />
                <div class="zoom-info">
                    <p id="shadow-box-name"></p>
                    <p id="shadow-box-description"></p>
                    <div class="mt-1">
                        <div class="store-cart-qty mt-5">
                            <a href="#" class="remover" onclick="shadowBoxDel()"><i
                                    class="fa fa-minus color-theme"></i></a>
                            <input id="shadow_box_qtde" type="number" readonly value="" class="qtde">
                            <a href="#" class="adicionar" onclick="shadowBoxAdd()"><i
                                    class="fa fa-plus color-theme"></i></a>
                        </div>
                    </div>
                    <button class="btn btn-outline-secondary" onclick="shadowBoxDisappear()">Fechar</button>
                    <!--<button class="btn" style="background-color: #ec891c; color: #ffffff" onclick="shadowBoxDisappear()">Fechar</button> -->
                </div>
            </div>
        </div>
    </div>
    <!--
    <div id="cart">
        <i class="fa fa-shopping-basket cart-icon3"></i>
        <span class="cart-total cart-total-3">100</span>
    </div>
    -->
    <div id="vue-cart">
        <div class="cart"><span class="cart-size">R$ 345,50</span><i class="fa fa-shopping-cart"></i>
            <div class="cart-items">
                <table class="cartTable">
                    <tbody>
                        <tr class="product">
                            <td class="align-left">
                                <div class="cartImage"
                                    style="background-size: cover; background-position: center center; background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chain.jpg&quot;);">
                                    <i class="close fa fa-times"></i></div>
                            </td>
                            <td class="align-center"><button><i class="close fa fa-minus"></i></button></td>
                            <td class="align-center">5</td>
                            <td class="align-center"><button><i class="close fa fa-plus"></i></button></td>
                            <td class="align-center">Produto 1</td>
                            <td>R$ 6,00</td>
                        </tr>
                        <tr class="product">
                            <td class="align-left">
                                <div class="cartImage"
                                    style="background-size: cover; background-position: center center; background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/orange.jpg&quot;);">
                                    <i class="close fa fa-times"></i></div>
                            </td>
                            <td class="align-center"><button><i class="close fa fa-minus"></i></button></td>
                            <td class="align-center">1</td>
                            <td class="align-center"><button><i class="close fa fa-plus"></i></button></td>
                            <td class="align-center">Poduto 2</td>
                            <td>R$ 1,50</td>
                        </tr>
                    </tbody>
                </table>
                <h4 class="cartSubTotal">R$ 7,50</h4><button class="clearCart">Esvaziar Carrinho
                </button><button class="checkoutCart">Finalizar</button>
                <table></table>
            </div>
        </div>
    </div>
</body>

</html>