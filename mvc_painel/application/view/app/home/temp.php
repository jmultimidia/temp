<?php
$title = 'Temp';
$css   = [  
    URL_PUBLIC . '/assets/app/css/framework' . MINIFY . '.css',    
];
$script = [    
];
$page = "temp";
require APP . 'view/app/_templates/initFile.php';
?>
<body class="theme-light" data-highlight="blue2">
<div id="page-transitions">
<!--header-->
<div class="page-hider"></div>

<!--End header-->
    <!--Page Content-->
<div class="page-content header-clear-large animated fadeIn faster">
<!--
<div class="discover-gradient">
<svg viewBox="0 0 100 100" preserveAspectRatio="none"><polygon fill="white" points="0,100 100,0 100,100"></polygon></svg>
</div>
-->
<div class="heading-style pb-0 mb-0">
    <h5 class="bolder">Você está em <a href="<?=URL_PUBLIC.DS.$page?>" class="button button-small button-round button-fill"><?=$title?></a></h5>
</div>
<div class="content itens-lst">
<form action="<?=URL_PUBLIC?>/pesquisa" method="get">
    <div class="input-group stylish-input-group">
        <input type="text" name="palavra-chave" class="search__input" placeholder="O que você procura?">
    </div>
</form>
</div>
</div>
<!--End PageContent-->
<?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
</body>
</html>
