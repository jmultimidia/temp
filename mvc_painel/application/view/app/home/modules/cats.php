<?php
if (!strcasecmp(basename($_SERVER['SCRIPT_NAME']), basename(__FILE__))) {
    die('Acesso Negado!');
}
if ($response['segmentos']):
?>
<section id="main-delivery">
    <div class="title-medium-container title-with-link pt-2"><h4>Categorias</h4>
    <a href="<?=URL_PUBLIC?>/categorias" class="button button-small button-round button-fill">Ver Mais<i class="left-5 fas fa-angle-double-right"></i></a>
    </div>
    <div id="own-delivery" class="owl-carousel icons-delivery">
    <?php
$i = 0;
foreach ($response['segmentos'] as $segmento):
    $i++;
    $calc = $i % 2;
    if ($calc == 1):
        $div    = "<div class=\"item main-circle\">";
        $divEnd = "";
    else:
        $div    = "";
        $divEnd = "</div>";
    endif;
    echo "$div";
    ?><a href="<?=URL_PUBLIC?>/estabelecimentos/<?=$segmento['id']?>" data-transition="slidefade">
	        <div class="circle"><img data-src="<?=URL_PUBLIC?>/<?=$segmento['imagem']?>" class="lazyload" alt="<?=$segmento['segmento']?>"></div>
	        <span><p><?=$segmento['segmento']?></p></span>
	  </a><?php
    echo "$divEnd";
endforeach ?>
    </div>
</section>
<?php endif;?>