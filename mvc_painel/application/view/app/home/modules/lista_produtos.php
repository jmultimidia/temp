<?php
use AgendaLabs\Libs\Helper;
if ($response['categorias']):
?>
<form action="<?=URL_PUBLIC?>/checkout" id="checkout" method="post">
    <?php
    foreach ($response['categorias'] as $key => $categoria):
    ?>
    <a href="#" class="color-theme font-14" data-accordion="accordion-<?=$key?>"><?=$categoria?><i
            class="fa fa-plus"></i></a>
    <div class="accordion-content" id="accordion-<?=$key?>">
        <?php
    if (count($response['produtos'][$key]) > 0):
        $status = 0;
        $x      = 0;
        foreach ($response['produtos'][$key] as $produto):
            $x     = $x + 1;
            $class = "";
            if ($x > 1):
                $class = "top-10";
            endif;
            if ($produto['status'] == 1):
                $preco = Helper::valor($produto['valor']);
                ?>
        <!--produto-->
        <div class="decoration bottom-10 <?=$class?>"></div>
        <div class="store-cart-1" id="p<?=$produto['id']?>_t<?=$produto['id_tamanho']?>">
            <?php if ($produto['imagem'] != URL_PUBLIC . '/assets/img/avatar.jpg'): ?>
            <div
                onclick="shadowBoxAppear('<?= $produto['imagem'] ?>','p<?=$produto['id']?>_t<?=$produto['id_tamanho']?>')">
                <img class="preload-image" src="<?=$produto['imagem']?>" data-src="<?=$produto['imagem']?>" alt="img">
            </div>
            <?php else: ?><img class="preload-image" src="<?=URL_PUBLIC?>/assets/img/avatar.jpg"
                data-src="<?=URL_PUBLIC?>/assets/img/avatar.jpg" alt="img">
            <?php endif;?>
            <strong id="p<?=$produto['id']?>_t<?=$produto['id_tamanho']?>_name"><?=$produto['nome']?></strong>
            <?php if ($produto['descricao']):
            $class = "";
            if (strlen(strip_tags($produto['descricao'])) > 50):
                $class = "description";
            endif;
            echo "<div class=\"" . $class . "\">
			                            <p id=\"p".$produto['id']."_t".$produto['id_tamanho']."_description\">$produto[descricao]</p>
			                            </div>";endif;
        if ($produto['varios_sabores']):
        ?><span class="obs text-right">* preço estimado, poderá sofrer alteração após selecionar os sabores ao
                finalizar o pedido.</span><?php
    endif;
    ?>
            <!--<span class="color-green-dark"><del class="color-gray-dark">$499</del> 10% Discount</span>-->
            <div class="appVue">
                <em
                    class="color-theme valor"><?=Helper::valor($produto['valor'])?></span><?=($produto['varios_sabores']) ? '<i class="obs-asterisk fas fa-asterisk"></i>' : '';?></em>
                <div class="store-cart-qty">
                    <a href="#" class="remover" v-on:click="del('p<?=$produto['id']?>_t<?=$produto['id_tamanho']?>')"><i
                            class="fa fa-minus color-theme"></i></a>
                    <input name="pedido[produto][<?=$produto['id']?>][<?=$produto['id_tamanho']?>]" type="number"
                        readonly value="<?=$_SESSION['pedido']['produto'][$produto['id'] . '_' . $produto['id_tamanho']] ?? 0
    ?>" class="qtde">
                    <a href="#" class="adicionar"
                        v-on:click="add('p<?=$produto['id']?>_t<?=$produto['id_tamanho']?>')"><i
                            class="fa fa-plus color-theme"></i></a>
                </div>
                <!--<a href="#" class="store-cart-1-remove color-orange-dark ">Remover </a>-->
                <!--end produto-->
            </div>
        </div>
        <ul class="list-group list-group-flush"><?php
    //print_r($response['produtos'][$produto['id']]['adicionais']);
    $prod_adc = $produto['adicional'] ?? [];
    foreach ($prod_adc as $item) {
        if ($adc[$item['id']]['tipo'] == 'label') {
            ?><li class="list-group-item"><?=$adc[$item['id']]['nome']?></li><?php
    foreach ($prod_adc as $it) {
                if ($adc[$it['id']]['pai'] == $item['id']) {
                    ?>
            <li class="list-group-item">
                <label style="width: 100%; margin: 0;">
                    <span
                        class="float-right valorAdicional"><?=$adc[$it['id']]['valor'] > 0 ? 'R$ ' : ''?><?=$adc[$it['id']]['valor'] > 0 ? Helper::valor($adc[$it['id']]['valor']) : ''?></span>
                    <input type="<?=$adc[$it['id']]['tipo']?>" class="inputAdicional"
                        name="<?='produtos[' . $i . '][adicional][]'?>" value="<?=$it['id']?>" data-id="heading<?=$i?>"
                        style="vertical-align: text-top;"><span
                        class="nomeAdicional"><?=$adc[$it['id']]['nome']?></span>
                </label>
            </li>
            <?php
    }
            }
        }
    }?></ul>
        <?php
else:
    $status++;
endif;
endforeach;
if ($status == count($response['produtos'][$key])): ?>
        <p>Lamentamos, mas os produtos desta categoria estão fora de estoque :(</p>
        <?php endif;
else: ?><p>Não há produtos nesta categoria :(</p>
        <?php endif;?>
    </div>
    <?php endforeach;?>
    <button type="submit"
        class="store-button button bg-secondary-dark button-icon button-full button-sm top-10 button-rounded uppercase ultrabold"><i
            class="fa fa-shopping-cart"></i>Avançar
        <span class="total float-right">R$ 0,00</span>
    </button>
    <input type="hidden" name="pedido[id_loja]" value="<?=$response['loja']['id']?>">
</form>
<?php endif; //End Categories ?>