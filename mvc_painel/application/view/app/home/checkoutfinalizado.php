<?php

use AgendaLabs\Libs\Helper;

$title = 'Checkout';
$css   = [  
    URL_PUBLIC.'/assets/app/css/checkout'.MINIFY.'.css', 
];
$script = [   
];
$page = "checkout";
require APP . 'view/app/_templates/initFile.php';
?>
<body class="theme-light" data-highlight="blue2">
<div id="page-transitions">
<!--header-->
<div class="page-hider"></div>

<!--End header-->
    <!--Page Content-->
    <section class="ui-content animated fadeIn faster">
<div class="page-content pages_maincontent header-clear-large">
<?php
include APP . 'view/app/home/modules/search.php';
?>
<div class="heading-style pb-0 mb-0">
                <h2 class="heading-title">Pedido Online</h2>                
                <div class="mt-4">
                <?php if($response['code'] == 'success'):?>
                    <div class="notification color-success-dark text-center mb-4">
                    <?= $response['msg']?>
                    </div>
                    <i class="fas fa-check-square font-30 color-success-dark"></i>
                    <?php else :?>
                        <div class="notification color-red-dark text-center mb-4">
                    <?= $response['msg']?>
                    </div>
                        <i class="fas fa-exclamation-triangle font-30 color-red-dark"></i>
                    <?php endif;?>
                </div>      
</div>
<div class="container">
<div class="aviso-add mb-3">

    <div class="row mt-3 text-center" id="formBox">
        <div class="col-12">
        <?php
            if($response['code'] == 'success'){
                if($response['loja']['tempo_entrega']) {
                   echo '<div class="sucesso">Legal! Seu pedido ficará pronto em breve. <div class="relogio"><i class="fas fa-clock"></i>' . date('H:i',strtotime('+' . $response['loja']['tempo_entrega'] . ' minutes',strtotime(date('H:i')))) . '</div>Previsão</div>';
                }
                ?>
                <a href="<?=URL_PUBLIC?>/meuspedidos" class="btn bg-color-blue-dark button-rounded button-sm uppercase ultrabold btn-block text-center mt-3">Ir para pedidos</a>
                <?php
            } else {
                ?>
                <a href="<?=URL_PUBLIC?>/estabelecimento/<?= $response['loja']['id'] ?>" class="btn bg-red-dark button-full button-rounded button-sm uppercase ultrabold btn-block text-center mt-3"><i class="fas fa-arrow-left"></i> Voltar</a>
                <?php
            }
            ?>
        </div>
    </div>
       
</div>
</div>

</div>
     </section>       
</div>
<!--End PageContent-->
<?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('form').parsley();
        <?= ($_SESSION['pedido']['tipo'] == 'Retirar Balcão' ? '$("input").attr("type","hidden");' : '') ?>
    })

    $('#cancelarVoltar').click(function () {
        $('#voltar').hide('fast');
    })

//    history.pushState(null, null, location.href);
//    window.onpopstate = function () {
//        $('#voltar').show('fast');
//        history.go(1);
//    };
</script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script type="text/javascript">
            $(function() {
                $.mask.definitions['~'] = "[+-]";
                $("#telefone").mask("(99) 9999?9-9999");
                $("#telefone").on("blur", function() {
                    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

                    if( last.length == 3 ) {
                        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
                        var lastfour = move + last;

                        var first = $(this).val().substr( 0, 9 );

                        $(this).val( first + '-' + lastfour );
                    }
                });
                $("#cep").mask("99.999-999");
            });
        </script>
</body>
</html>
