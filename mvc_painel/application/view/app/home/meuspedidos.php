<?php

use AgendaLabs\Libs\Helper;

$title = 'Checkout';
$css   = [  
    URL_PUBLIC . '/assets/app/css/framework' . MINIFY . '.css?202004021911',
];
$script = [   
    
];
$page = "checkout";
require APP . 'view/app/_templates/initFile.php';
?>
<body class="theme-light" data-highlight="blue2">
<?php require APP . 'view/app/_templates/preloader.php';?>
<div id="page-transitions">
<!--header-->
<div class="page-hider"></div>

<!--End header-->
    <!--Page Content-->
    <section class="ui-content animated fadeIn faster">
<div class="page-content pages_maincontent header-clear-large">
<?php
include APP . 'view/app/home/modules/search.php';
?>
<div class="heading-style pb-0 mb-0">
                <h2 class="heading-title">Meus pedidos</h2>                
                <div class="mt-4">
                    <i class="fas fa-list font-30 color-blue-dark"></i>
                </div>      
</div>        
        <div class="container bg-light">
        <div class="row">
        <div class="col-12">
            <div class="accordion" id="accordionExample">

                <?php
                $classBadge = [
                        'Cancelado' => 'badge-danger',
                        'Pendente' => 'badge-warning',
                        'Producao' => 'badge-success',
                        'Saiu' => 'badge-info',
                        'Entregue' => 'badge-secondary',
                        'Incompleto' => 'badge-default'
                ];
                foreach ($response['pedidos'] as $item) {
                ?>
                
                <div class="card">
                    <div class="card-header p-0 pl-1" id="heading<?= $item['id'] ?>">
                        <h3 class="mb-0">
                            <a class="btn btn-default btn-block text-left collapsed" data-toggle="collapse"
                               data-target="#collapse<?= $item['id'] ?>" aria-expanded="false"
                               aria-controls="collapse<?= $item['id'] ?>">
                               <strong>#<?= $item['id'] ?> - <?= $item['loja'] ?></strong><br>
                                <?php
                                  if($item['situacao'] == 'Producao' ) {
                                     echo '<div>Seu pedido ficará pronto por volta das <span class="text-danger">' . date('H:i',strtotime('+' . $item['tempo_entrega'] . ' minutes',strtotime($item['data_cadastro']))) . '</span>.</div>';
                                  }
                                ?>
                                <small style="display: block; float: right;"><?= Helper::dataHora($item['data_cadastro']) ?>
                                    <i class="fas fa-clock"></i></small>
                                <span class="badge <?= $classBadge[$item['situacao']] ?> text-light mr-1">
                                  <?php 
                                    switch ( $item['situacao'] ) {
                                        case 'Saiu':
                                            echo "Saiu para Entrega";
                                            break;
                                        case 'Producao':
                                            echo "Em produção";
                                            break;
                                        default:
                                            echo $item['situacao'];
                                            break;
                                     }
                                  ?>
                                </span>
                                R$ <?= Helper::valor($item['valor_pedido']) ?>
                            </a>
                        </h3>
                    </div>

                    <div id="collapse<?= $item['id'] ?>" class="collapse" aria-labelledby="heading<?= $item['id'] ?>"
                         data-parent="#accordionExample">
                        <div class="card-body">
                        <h4 class="bolder mt-2">Pedidos</h4>
                            <ul class="list-group">
                              <?php
                                if ( $item['nome'] ) { ?>
                                  <li class="list-group-item" style="padding: 5px;">
                                     <?= $item['nome'] ?>
                                     <?= ($item['celular']) ? ' (' . $item['celular'] . ')' : ''; ?>
                                  </li>
                                <?php } ?>
                                <li class="list-group-item" style="padding: 5px;">
                                    Total: R$ <?= Helper::valor($item['valor_pedido']) . ($item['pagamento'] == 'Dinheiro' ? ' (Troco: ' . Helper::valor($item['valor_troco']) . ')' : '') ?>
                                </li>
                                <li class="list-group-item" style="padding: 5px;">
                                    Pagamento: <?= $item['pagamento'] ?>
                                </li>
                                <li class="list-group-item" style="padding: 5px;">
                                    <?= $item['tipo'] ?>
                                </li>
                            </ul>
                            <?php
                            if ($item['tipo'] == 'Entregar') {
                            ?>
                            <h4 class="bolder mt-2">Endereço</h4>
                            <ul class="list-group">
                                <li class="list-group-item" style="padding: 5px;">
                                    Endereço: <?= $item['endereco'] . ', ' . $item['numero'] . ($item['complento'] ? ', ' . $item['complento'] : ''); ?>
                                    <br>
                                </li>
                                <li class="list-group-item" style="padding: 5px;">
                                    Ponto de Referência: <?= $item['ptoreferencia'] ?>
                                </li>
                                <li class="list-group-item" style="padding: 5px;">
                                    Bairro: <?= $item['bairro'] ?>
                                </li>
                            </ul>
                            <?php
                            }
                            ?>
                            <h4 class="bolder mt-2">Produtos</h4>
                            <ul class="list-group">
                                <?php
                                foreach ($item['produtos'] as $produto) {
                                    //var_dump($produto);
                                    //var_dump($item['adc']);
                                    ?>
                                    <li class="list-group-item" style="padding: 5px;">
                                        <!-- <span class="badge <?= $classBadge[$item['situacao']] ?>" style="float: none"><?=$produto['qtde']?></span> -->
                                        <?=$produto['qtde']?> &times;&nbsp;
                                        <?=mb_strtoupper($produto['nome'])?><br>
                                        <?= empty($produto['sabores']) ? '' : $produto['sabores'].'<br/>' ;?>
                                        <?=$produto['descricao']?>
                                        <?php /*
                                          for ( $cont = 1; $cont < count($item['adc']); $cont++ ) {
                                            echo ($cont % 2) ? 'Adicionais: ' . $item['adc'][$cont]['descricao'] . '.<br>' : '';
                                          }
                                          if ( $produto['descricao'] ) {
                                            echo 'Observação: ' . $produto['descricao'];
                                          }*/
                                        ?>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
        </div>        
    </div>

</div>
     </section>       
</div>
<!--End PageContent-->
<?php
include APP . 'view/app/_templates/footer.php';
include APP . 'view/app/_templates/scripts.php';
?>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('form').parsley();
        <?= ($_SESSION['pedido']['tipo'] == 'Retirar Balcão' ? '$("input").attr("type","hidden");' : '') ?>
    })

    $('#cancelarVoltar').click(function () {
        $('#voltar').hide('fast');
    })

//    history.pushState(null, null, location.href);
//    window.onpopstate = function () {
//        $('#voltar').show('fast');
//        history.go(1);
//    };
</script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script type="text/javascript">
            $(function() {
                $.mask.definitions['~'] = "[+-]";
                $("#telefone").mask("(99) 9999?9-9999");
                $("#telefone").on("blur", function() {
                    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

                    if( last.length == 3 ) {
                        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
                        var lastfour = move + last;

                        var first = $(this).val().substr( 0, 9 );

                        $(this).val( first + '-' + lastfour );
                    }
                });
                $("#cep").mask("99.999-999");
            });
        </script>
</body>
</html>