<?php
namespace AgendaLabs\Fragments;
use AgendaLabs\Libs\Helper;

class DeliverysFragment
{
    public static function parseTemplate($loja)
    {        

        /*

        Para referencia:

        <!--Item-->
        <div class="store-slide-2">
        <a href="#" class="store-slide-image">
        <img class="preload-image" src="<?=URL_PUBLIC?>/assets/app/images/avatar.jpg" data-src="<?=URL_PUBLIC.DS.$loja['imagem']?>" alt="img">
        </a>
        <div class="store-slide-title">
        <strong><?=$loja['nome']?></strong>
        <p><?=$loja['descricao']?></p>
        </div>
        <div class="store-slide-button">
        <strong><del class="color-default font-10 bold">
        <?=\AgendaLabs\Libs\Helper::isNew($loja['data_cadastro']) ? 'NOVO' : '<!--<ul><li><i class="fa fa-star"></i></li>
        <li><i class="fa fa-star"></i></li>
        <li><i class="fa fa-star"></i></li>
        <li><i class="fa fa-star"></i></li>
        <li><i class="fa fa-star"></i></li>
        </ul>-->'?>
        </del> <?php if ($loja['aberto']): ?>
        <span class="status status-aberto mr-1">Aberto</span><?php if ($loja['online'] == '1'):?><span class="status status-pedido-online">Vendas Online</span><?php endif; else: if ($loja['target_fechado'] == '1'):?><span class="status status-fechado">Fechado</span><?php endif; endif;?></strong>
        <!--<a href="#"><img src="<?=URL_PUBLIC?>/assets/images/icons/black/phone-blue.png"></a>
        <a href="#"><img src="<?=URL_PUBLIC?>/assets/images/icons/black/whatsapp.png"></a>
        <a href="#"><img src="<?=URL_PUBLIC?>/assets/images/icons/black/sooter.png"></a>
        <a href="#"><img src="<?=URL_PUBLIC?>/assets/images/icons/black/coracao-favoritar.png" alt="Favoritar"></a>
        --><a href="#"><i class="<?=in_array($loja['id'], (array) @$_SESSION['favoritos']) ? 'bg-red-dark' : 'bg-night-dark'?> fa fa-heart"></i></a>
        <?php
        if ($loja['telefone']): ?><a href="#"><i class="bg-orange-light fas fa-phone"></i></a>
        <?php endif;
        if ($loja['celular']): ?><a href="#"><i class="bg-whatsapp fab fa-whatsapp"></i></a>
        <?php endif;
        if (!$loja['delivery']): ?>
        <a href="#"><i class="bg-blue2-dark fas fa-motorcycle"></i></a>
        <?php endif;?>
        </div>
        </div>
        <div class="decoration bottom-0"></div>
        <!--End Item-->
         */
        $template = '<!--Item-->';        
        $template .= '<div class="store-slide-2">';
        $template .= '<a href="'.URL_PUBLIC.'/estabelecimento/'.$loja['id'].'" class="store-slide-image">';
        $template .= '<img class="lazyload" src="' . URL_PUBLIC . '/assets/app/images/avatar.jpg" data-src="' . URL_PUBLIC . DS . $loja['imagem'] . '" alt="img">';
        $template .= '</a>';
        $template .= '</span>';
        $template .= '<div class="store-slide-title">';
        $template .= '<a href="'.URL_PUBLIC.'/estabelecimento/'.$loja['id'].'">';
        $template .= '<strong>' . $loja['nome'] . '</strong>';        
        $template .= '<p>' . $loja['descricao'] . '</p>';
        $template .= '</a>';
        $template .= '</div>';
        $template .= '<div class="store-slide-button">';
        $template .= '<strong><del class="color-default font-10 bold">';
        if (Helper::isNew($loja['data_cadastro'])):
            $template .= 'NOVO';
        //else:
//$template .= $star->getRating('userChoose size-4');
  /*          $template .= '<ul><li><i class="fa fa-star"></i></li>
	        <li><i class="fa fa-star"></i></li>
	        <li><i class="fa fa-star"></i></li>
	        <li><i class="fa fa-star"></i></li>
	        <li><i class="fa fa-star"></i></li>
	    </ul>';*/
        endif;
        $template .= '</del>';
        if ($loja['aberto']):
            $template .= '<span class="status status-aberto mr-1">Aberto</span>';
            if ($loja['online'] == '1'):
                $template .= '<span class="status status-pedido-online">Pedidos Online</span>';
            endif;
        else:
            if ($loja['target_fechado'] == '1'):
                $template .= '<span class="status status-fechado">Fechado</span>';
            endif;
        endif;
        $template .= '</strong>';
        $template .= '<span class="favorite"><i class="';
        if (in_array($loja['id'], (array) @$_SESSION['favoritos'])):
            $template .= 'bg-red-dark favorite-btn';
        else:
            $template .= 'bg-night-dark favorite-btn';
        endif;
        $template .= ' fa fa-heart" data-id="'.$loja['id'].'"></i></span>';
        if ($loja['telefone']):
            $template .= '<a href="tel:0'.str_replace(['(', ')', ' ', '-'], '', $loja['telefone']).'"><i class="bg-orange-light fas fa-phone"></i>';
        endif;
        if ($loja['celular']):
            $numero = str_replace(['(', ')', ' ', '-'], '', $loja['celular']);
            $texto  = urlencode('Olá, estou no aplicativo npop e gostaria de um atendimento. Aguardo contato!');
            $template .= '<a onclick="abrirWhats(\'' . $numero . '\',\'' . $texto . '\')"><i class="bg-whatsapp fab fa-whatsapp"></i></a>';
        endif;
        if ($loja['delivery']):
            $template .= '<a href="#"><i class="bg-blue2-dark fas fa-motorcycle"></i></a>';
        endif;
        $template .= ' </div>
        </div>
        <div class="decoration bottom-0"></div>';
        $template .= '<!--End Item-->';

        return $template;
    }
}
