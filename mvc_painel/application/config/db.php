<?php

$development = __DIR__.'/db-development.php';

if (file_exists($development)) {
     return include $development;
} else {
    return [
        'ENVIRONMENT' => 'Produção',
        'MINIFY' => '.min',
        'DB_TYPE' => 'mysql',
        'DB_HOST' => 'localhost',
        'DB_NAME' => '',
        'DB_USER' => '',
        'DB_PASS' => '',
        'DB_CHARSET' => 'utf8'
    ];
}