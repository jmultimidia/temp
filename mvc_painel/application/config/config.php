<?php
//define('DB_TYPE', 'mysql');
//define('DB_HOST', 'localhost');
//define('DB_NAME', 'guiaedelivery');
//define('DB_USER', 'root');
//define('DB_PASS', '');
//define('DB_CHARSET', 'utf8');

$configDB = include __DIR__ . '/db.php';

define('ENVIRONMENT', $configDB['ENVIRONMENT']);
define('DB_TYPE', $configDB['DB_TYPE']);
define('DB_HOST', $configDB['DB_HOST']);
define('DB_NAME', $configDB['DB_NAME']);
define('DB_USER', $configDB['DB_USER']);
define('DB_PASS', $configDB['DB_PASS']);
define('DB_CHARSET', $configDB['DB_CHARSET']);
define('MINIFY', $configDB['MINIFY']);

$bd     = new \AgendaLabs\Core\Model();
$config = $bd->find('configuracao', 1);

if (ENVIRONMENT === 'Desenvolvimento') {    
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    error_reporting(E_NOTICE);
    ini_set("display_errors", 0); 
} else {
    error_reporting(E_NOTICE);
    ini_set("display_errors", 0);    
}
$ip = $_SERVER['REMOTE_ADDR'];
define('APP_TITLE', $config['app_title']);
define('URL_PROTOCOL', $config['protocol']);
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_PUBLIC', URL_PROTOCOL . URL_DOMAIN);
define('URL_ADMIN', URL_PROTOCOL . URL_DOMAIN . '/system');
define('URL_PAGE', trim(URL_PUBLIC . $_SERVER['REQUEST_URI'], '/'));

//define('MP_CLIENT_ID', '4824604728281188');
//define('MP_CLIENT_SECRET', 'B1lRLZNTkLjdId0ooiFzKS3MZD0WGHGD');
define('RECAPTCHA_SITE_KEY', "6LfqOrEUAAAAAH6rGB3bZQHMSBz4g6XAGKrah4BJ"); //Site key
define('RECAPTCHA_SECRET_KEY', "6LfqOrEUAAAAADtVXTZN_9jreDbZ-y_dYLMveApn"); //Secret key

define('MAIL_HOST', $config['mail_host']);
define('MAIL_AUTH', $config['mail_auth']);
define('MAIL_USER', $config['mail_user']);
define('MAIL_PASS', $config['mail_pass']);
define('MAIL_SECURE', $config['mail_secure']);
define('MAIL_PORT', $config['mail_port']);
define('MAIL_SENDTYPE', $config['mail_sendtype']);