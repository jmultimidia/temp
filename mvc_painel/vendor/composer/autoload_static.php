<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1802fdf4726c184ab8f2724531c070f7
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PagSeguro\\' => 10,
            'PHPMailer\\PHPMailer\\' => 20,
        ),
        'A' => 
        array (
            'AgendaLabs\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PagSeguro\\' => 
        array (
            0 => __DIR__ . '/..' . '/pagseguro/pagseguro-php-sdk/source',
        ),
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
        'AgendaLabs\\' => 
        array (
            0 => __DIR__ . '/../..' . '/application',
        ),
    );

    public static $prefixesPsr0 = array (
        'D' => 
        array (
            'Detection' => 
            array (
                0 => __DIR__ . '/..' . '/mobiledetect/mobiledetectlib/namespaced',
            ),
        ),
    );

    public static $classMap = array (
        'MP' => __DIR__ . '/..' . '/mercadopago/sdk/lib/mercadopago.php',
        'MPRestClient' => __DIR__ . '/..' . '/mercadopago/sdk/lib/mercadopago.php',
        'MercadoPagoException' => __DIR__ . '/..' . '/mercadopago/sdk/lib/mercadopago.php',
        'Mobile_Detect' => __DIR__ . '/..' . '/mobiledetect/mobiledetectlib/Mobile_Detect.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1802fdf4726c184ab8f2724531c070f7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1802fdf4726c184ab8f2724531c070f7::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit1802fdf4726c184ab8f2724531c070f7::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit1802fdf4726c184ab8f2724531c070f7::$classMap;

        }, null, ClassLoader::class);
    }
}
